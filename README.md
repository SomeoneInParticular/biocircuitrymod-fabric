# Biocircuitry

## Setup

For setup instructions please see the [fabric wiki page](https://fabricmc.net/wiki/tutorial:setup) which relates to the IDE that you are using. There are not special steps currently required to build this repository.

## License

This modification is available under the GPLv3 license (See LICENSE.txt). I am fine with you using this code however you see fit (I.E. modpacks), so long as its use remains freely accessible (no pay-to-access shenanigans), and the code remains attributed in some manner to this code base. Any of the code herein (modified or otherwise) must also share the GPLv3 license if it is to be distributed. So long as those conditions are met, feel free to do whatever you want without asking for permission. 
