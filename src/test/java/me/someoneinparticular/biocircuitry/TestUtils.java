/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry;

import org.ojalgo.netio.BasicLogger;

public class TestUtils {
    // Timing variables
    private static long cachedTime;

    // Track the start time of the timing log
    public static void beginTiming() {
        cachedTime = System.nanoTime();
    }

    public static void endTiming() {
        long timeElapsed = System.nanoTime() - cachedTime;
        BasicLogger.debug(String.format("Run time: %.2fms", timeElapsed / 1000000f));
    }
}
