/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.proteomics;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.TestUtils;
import me.someoneinparticular.biocircuitry.data_handlers.WormFoodNutrientHandler;
import me.someoneinparticular.biocircuitry.data_handlers.WormMetaboliteHandler;
import me.someoneinparticular.biocircuitry.proteomics.components.WormMetabolite;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.minecraft.Bootstrap;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Pair;
import net.minecraft.util.registry.Registry;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

public class WormPathwayTest {
    // Test metabolites for use in the system
    private static WormMetabolite A;
    private static WormMetabolite B;
    private static WormMetabolite C;
    private static WormMetabolite D;

    // Simulation test metabolites for distribution assessment
    private static WormMetabolite sugar;
    private static WormMetabolite glucose;
    private static WormMetabolite fructose;
    private static WormMetabolite kitin;

    @BeforeClass
    public static void init() {
        // Initialize the bootstrap so Items/ItemStacks can be worked with
        Bootstrap.initialize();
        // Initialize our metabolites
        A = WormMetaboliteHandler.register(BioCircuitry.id("a"));
        B = WormMetaboliteHandler.register(BioCircuitry.id("b"));
        C = WormMetaboliteHandler.register(BioCircuitry.id("c"));
        D = WormMetaboliteHandler.register(BioCircuitry.id("d"));
        sugar = WormMetaboliteHandler.register(BioCircuitry.id("sugar"));
        glucose = WormMetaboliteHandler.register(BioCircuitry.id("glucose"));
        fructose = WormMetaboliteHandler.register(BioCircuitry.id("fructose"));
        kitin = WormMetaboliteHandler.register(BioCircuitry.id("kitin"));
        // Addition of appropriate entries
        WormFoodNutrientHandler.FoodValue appleNut = WormFoodNutrientHandler.register(Registry.ITEM.getId(Items.APPLE));
        appleNut.addNutrient(WormMetaboliteHandler.getId(A), 1);
        WormFoodNutrientHandler.FoodValue potatoNut = WormFoodNutrientHandler.register(Registry.ITEM.getId(Items.POTATO));
        potatoNut.addNutrient(WormMetaboliteHandler.getId(B), 1);
    }

    @Before
    public void printBoundaryLine() {
        System.out.println("-----------------------------------------------------------------------------------------");
    }

    // -- CONSTRUCTION HELPERS -- //
    // Helper function to speed up the construction of arbitrary reactions
    private static HashMap<WormMetabolite, Integer> buildReaction(WormMetabolite[] metabolites, Integer[] values) {
        // Quick check to make sure that each array is the same size
        if (metabolites.length != values.length) {
            return null;
        }
        // And now build the HashMap proper
        HashMap<WormMetabolite, Integer> reaction = new HashMap<>();
        for (int i = 0; i < metabolites.length; i++) {
            reaction.put(metabolites[i], values[i]);
        }
        return reaction;
    }

    // -- TEST HELPERS -- //
    /**
     * Check a collection of pairs for the presence of a designated range of stack values
     * NOTE: This WILL modify the input collection; Create a copy before using this function if that matters!
     * @param col Collection to test
     * @param type Item type expected
     * @param min Minimum count of said item (inclusive)
     * @param max Maximum count of said item (inclusive)
     * @return true if a stack meeting the criteria exists in the collection, false otherwise
     */
    private static <N extends Number> boolean checkCollectionForPair(Collection<Pair<Item, N>> col, Item type, int min, int max) {
        // Check every stack in the collection for the designated properties
        for (Pair<Item, N> stack : col) {
            if (stack.getLeft().equals(type) && stack.getRight().intValue() <= max && stack.getRight().intValue() >= min) {
                col.remove(stack);
                return true;
            }
        }
        // Post cycle sanity check; if min = 0, its true whether a representative stack is present or not
        return min == 0;
    }

    // Wrapper of the above function for easy error message generation
    private static <N extends Number> String checkPairMissing(Collection<Pair<Item, N>> col, Item type, int min, int max, String sourceName) {
        // If the stack lacked the designated type of itemstack
        String msg = "";
        if (!checkCollectionForPair(col, type, min, max)) {
            // Begin the error message
            // Add the appropriate designator
            msg = "\t" + sourceName + " collection lacks stack (" +
                    // Add the stack specific text
                    min + " <= " +
                    type.getName().getString() + " <= " +
                    max + ")\n";
        }
        return msg;
    }

    // Actual error checking
    private static <N extends Number, M extends Number> void assertNoStackErrors(
            List<Pair<Item, N>> leftoverResults, List<Pair<Item, M>> productResults, String errorMSG, WormPathway refPath
    ) {
        // Confirm no excess stacks were created
        if (!leftoverResults.isEmpty()) {
            errorMSG += "Excess costs:\n\t" + leftoverResults.toString();
        }
        if (!productResults.isEmpty()) {
            errorMSG += "\nExcess products:\n\t" + productResults.toString();
        }
        // If any errors were caught, report them
        boolean noErrors = errorMSG.equals("");
        if (!noErrors) {
            errorMSG += "\n\nPathway dump:" +
                    "\n\tCosts: " + refPath.getCosts() +
                    "\n\tProducts: " + refPath.getProducts() +
                    '\n';
        }
        assertTrue("Missing:\n" + errorMSG, noErrors);
    }

    // -- Tests -- //
    @Test
    public void testConstructor_basic() {
        System.out.println("Basic pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoB = buildReaction(
                new WormMetabolite[] {A, B},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> BtoLife = buildReaction(
                new WormMetabolite[] {B},
                new Integer[] {-1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAB = new WormProtein("AtoB", "", AtoB);
        WormProtein protBLife = new WormProtein("BtoLife", "", BtoLife,
                singletonList(new Pair<>(Items.IRON_INGOT, 1f)), 1, 1);
        // Finally, build the pathway for this setup
        TestUtils.beginTiming();
        WormPathway pathway = new WormPathway(
                singletonList(new ItemStack(Items.APPLE, 10)),
                asList(protAB, protBLife)
        );
        TestUtils.endTiming();
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        List<Pair<Item, Integer>> costResults = pathway.getCosts();
        List<Pair<Item, Float>> productResults = pathway.getProducts();
        if (costResults == null) {
            errorMSG = "No costs incurred by this pathway!";
        }
        if (productResults == null) {
            errorMSG = "No products created by this pathway!";
        }
        if (!errorMSG.equals("")) {
            fail(errorMSG);
        }
        errorMSG += checkPairMissing(costResults, Items.APPLE, 1, 1, "Cost");
        errorMSG += checkPairMissing(productResults, Items.IRON_INGOT, 1, 1, "Products");
        // Confirm no excess stacks were created
        assertNoStackErrors(costResults, productResults, errorMSG, pathway);
    }

    @Test
    public void testConstructor_merge() {
        System.out.println("Merging pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoC = buildReaction(
                new WormMetabolite[] {A, C},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> BtoD = buildReaction(
                new WormMetabolite[] {B, D},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> CDtoLife = buildReaction(
                new WormMetabolite[] {C, D},
                new Integer[] {-1, -1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAC = new WormProtein("AtoC", "", AtoC);
        WormProtein protBD = new WormProtein("BtoD", "", BtoD);
        WormProtein protCDLife = new WormProtein("CDtoLife", "",
                CDtoLife, singletonList(new Pair<>(Items.PAPER, 3f)), 1, 1);
        // Finally, build the pathway for this setup
        TestUtils.beginTiming();
        WormPathway pathway = new WormPathway(
                asList(new ItemStack(Items.APPLE, 10), new ItemStack(Items.POTATO, 10)),
                asList(protAC, protBD, protCDLife)
        );
        TestUtils.endTiming();
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        ArrayList<Pair<Item, Integer>> costResults = pathway.getCosts();
        ArrayList<Pair<Item, Float>> productResults = pathway.getProducts();
        errorMSG += checkPairMissing(costResults, Items.APPLE, 0, 1, "Cost");
        errorMSG += checkPairMissing(costResults, Items.POTATO, 0, 1, "Cost");
        errorMSG += checkPairMissing(productResults, Items.PAPER, 3, 3, "Products");
        // Confirm no excess stacks were created
        assertNoStackErrors(costResults, productResults, errorMSG, pathway);
    }

    @Test
    public void testConstructor_split() {
        System.out.println("Splitting pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoB = buildReaction(
                new WormMetabolite[] {A, B},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> AtoC = buildReaction(
                new WormMetabolite[] {A, C},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> BtoLife = buildReaction(
                new WormMetabolite[] {B},
                new Integer[] {-1}
        );
        HashMap<WormMetabolite, Integer> CtoLife = buildReaction(
                new WormMetabolite[] {C},
                new Integer[] {-1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAB = new WormProtein("AtoB", "", AtoB);
        WormProtein protAC = new WormProtein("AtoC", "", AtoC);
        WormProtein protBLife = new WormProtein("BtoLife", "", BtoLife,
                singletonList(new Pair<>(Items.SALMON, 2f)), 1, 1);
        WormProtein protCLife = new WormProtein("CtoLife", "", CtoLife,
                singletonList(new Pair<>(Items.SUGAR, 2f)), 1, 1);
        // Finally, build the pathway for this setup
        TestUtils.beginTiming();
        WormPathway pathway = new WormPathway(
                singletonList(new ItemStack(Items.APPLE, 10)),
                asList(protAB, protAC, protBLife, protCLife)
        );
        TestUtils.endTiming();
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        List<Pair<Item, Integer>> costProducts = pathway.getCosts();
        ArrayList<Pair<Item, Float>> productResults = pathway.getProducts();
        errorMSG += checkPairMissing(costProducts, Items.APPLE, 2, 2, "Cost");
        errorMSG += checkPairMissing(productResults, Items.SALMON, 0, 2, "Products");
        errorMSG += checkPairMissing(productResults, Items.SUGAR, 0, 2, "Products");
        // Confirm no excess stacks were created
        assertNoStackErrors(costProducts, productResults, errorMSG, pathway);
    }

    @Test
    public void testConstructor_invalid() {
        System.out.println("Invalid pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoB = buildReaction(
                new WormMetabolite[] {A, B},
                new Integer[] {-1, 1}
        );
        HashMap<WormMetabolite, Integer> CtoLife = buildReaction(
                new WormMetabolite[] {C},
                new Integer[] {-1}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAB = new WormProtein("AtoB", "", AtoB);
        WormProtein protC = new WormProtein("CtoLife", "", CtoLife,
                singletonList(new Pair<>(Items.SUGAR, 2f)), 1, 1);
        // Finally, build the pathway for this setup
        TestUtils.beginTiming();
        WormPathway pathway = new WormPathway(
                singletonList(new ItemStack(Items.APPLE, 10)),
                asList(protAB, protC)
        );
        TestUtils.endTiming();
        // Confirm that the pathway was set to a null state
        ArrayList<Pair<Item, Integer>> costResults = pathway.getCosts();
        ArrayList<Pair<Item, Float>> productResults = pathway.getProducts();
        assertNull(costResults);
        assertNull(productResults);
    }

    @Test
    public void testConstructor_cyclic() {
        System.out.println("Cyclic pathway test");
        // Build a set of reactions for use during protein construction
        HashMap<WormMetabolite, Integer> AtoB = buildReaction(
                new WormMetabolite[] {A, B},
                new Integer[] {-2, 2}
        );
        HashMap<WormMetabolite, Integer> BtoC = buildReaction(
                new WormMetabolite[] {B, C},
                new Integer[] {-2, 2}
        );
        HashMap<WormMetabolite, Integer> CtoD = buildReaction(
                new WormMetabolite[] {C, D},
                new Integer[] {-2, 2}
        );
        // Note; this reaction effectively synthesizes extra metabolites ex nihilo
        HashMap<WormMetabolite, Integer> DtoB = buildReaction(
                new WormMetabolite[] {D, B},
                new Integer[] {-1, 2}
        );
        HashMap<WormMetabolite, Integer> DtoLife = buildReaction(
                new WormMetabolite[] {D},
                new Integer[] {-2}
        );
        // Build the proteins that mediate these reactions
        WormProtein protAB = new WormProtein("AtoB", "", AtoB);
        WormProtein protBC = new WormProtein("BtoC", "", BtoC);
        WormProtein protCD = new WormProtein("CtoD", "", CtoD);
        WormProtein protDB = new WormProtein("DtoB", "", DtoB);
        // One sugar, biomass, and motility produced per input of metabolite D
        WormProtein protDtoLife = new WormProtein("DtoLife", "", DtoLife,
                singletonList(new Pair<>(Items.SUGAR, 2f)), 2, 2);
        // Finally, build the pathway for this setup
        TestUtils.beginTiming();
        WormPathway pathway = new WormPathway(
                singletonList(new ItemStack(Items.APPLE, 1)),
                asList(protAB, protBC, protCD, protDB, protDtoLife)
        );
        TestUtils.endTiming();
        // Confirm that the pathway was set to a null state
        ArrayList<Pair<Item, Integer>> costResults = pathway.getCosts();
        ArrayList<Pair<Item, Float>> productResults = pathway.getProducts();
        // Confirm that all expected costs and results are correct
        String errorMSG = "";
        errorMSG += checkPairMissing(costResults, Items.APPLE, 1, 1, "Cost");
        errorMSG += checkPairMissing(productResults, Items.SUGAR, 1, 2, "Products");
        // Confirm no excess stacks were created
        assertNoStackErrors(costResults, productResults, errorMSG, pathway);
    }

    @Test
    public void testDistribution() {
        // Designate nutrition of sugar input
        WormFoodNutrientHandler.FoodValue sugarNut = WormFoodNutrientHandler.register(Registry.ITEM.getId(Items.SUGAR));
        sugarNut.addNutrient(WormMetaboliteHandler.getId(sugar), 1);

        // Build the reactions available to the worm
        HashMap<WormMetabolite, Integer> sugar_glucoA = buildReaction(
                new WormMetabolite[] {sugar, glucose, fructose},
                new Integer[] {-4, 3, 2}
        );
        HashMap<WormMetabolite, Integer> sugar_glucoa = buildReaction(
                new WormMetabolite[] {sugar, glucose},
                new Integer[] {-4, 5}
        );
        HashMap<WormMetabolite, Integer> gluco_kitin = buildReaction(
                new WormMetabolite[] {glucose, kitin},
                new Integer[] {-10, 10}
        );
        HashMap<WormMetabolite, Integer> kitin_leather = buildReaction(
                new WormMetabolite[] {kitin},
                new Integer[] {-5}
        );
        HashMap<WormMetabolite, Integer> kitin_string = buildReaction(
                new WormMetabolite[] {kitin},
                new Integer[] {-3}
        );

        // Build the proteins for these reactions
        WormProtein sugR_A = new WormProtein("sugR_A", "", sugar_glucoA, 2);
        WormProtein sugR_a = new WormProtein("sugR_a", "", sugar_glucoa, 1);
        WormProtein gluK = new WormProtein("gluK", "", gluco_kitin);
        WormProtein defP_E = new WormProtein("defP_E", "", kitin_leather,
                singletonList(new Pair<>(Items.LEATHER, 10f)), 1, 10);
        WormProtein defP_S = new WormProtein("defP_S", "", kitin_string,
                singletonList(new Pair<>(Items.STRING, 10f)), 3, 10);

        // Test the pathways for sugar input counts of 1 to 8
        List<WormProtein> prots = asList(sugR_A, sugR_a, gluK, defP_E, defP_S);
        for (int i = 1; i < 11; i++) {
            WormPathway pathway = new WormPathway(
                    singletonList(new ItemStack(Items.SUGAR, i)), prots
            );
            ArrayList<Pair<Item, Float>> productResults = pathway.getProducts();
            float stringCount = 0;
            float leatherCount = 0;
            if (productResults != null) {
                for (Pair<Item, Float> s : productResults) {
                    if (s.getLeft() == Items.STRING) {
                        stringCount = s.getRight();
                    } else if (s.getLeft() == Items.LEATHER) {
                        leatherCount = s.getRight();
                    }
                }
            }
            System.out.printf("%d: %f, %f%n", i, stringCount, leatherCount);
        }
    }
}