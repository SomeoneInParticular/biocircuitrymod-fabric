/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.proteomics.components;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;

import java.util.*;

public enum AminoAcid {
    A("Alanine", "Ala",
            new String[]{"GCT", "GCC", "GCA", "GCG"}),
    R("Arginine", "Arg",
            new String[]{"CGT", "CGC", "CGA", "CGG", "AGA", "AGG"}),
    N("Asparagine", "Asn",
            new String[]{"AAT", "AAC"}),
    D("Aspartic Acid", "Asp",
            new String[]{"GAT", "GAC"}),
    C("Cysteine", "Cys",
            new String[]{"TGT", "TGC"}),
    Q("Glutamine", "Gln",
            new String[]{"CAA", "CAG"}),
    E("Glutamic Acid", "Glu",
            new String[]{"GAA", "GAG"}),
    G("Glycine", "Gly",
            new String[]{"GGT", "GGC", "GGA", "GGG"}),
    H("Histidine", "His",
            new String[]{"CAT", "CAC"}),
    I("Isoleucine", "Ile",
            new String[]{"ATT", "ATC", "ATA"}),
    L("Leucine", "Leu",
            new String[]{"TTA", "TTG", "CTT", "CTC", "CTA", "CTG"}),
    K("Lysine", "Lys",
            new String[]{"AAA", "AAG"}),
    M("Methionine", "Met",
            new String[]{"ATG"}),
    F("Phenylalanine", "Phe",
            new String[]{"TTT", "TTC"}),
    P("Proline", "Pro",
            new String[]{"CCT", "CCC", "CCA", "CCG"}),
    S("Serine", "Ser",
            new String[]{"TCT", "TCC", "TCA", "TCG", "AGT", "AGC"}),
    T("Threonine", "Thr",
            new String[]{"ACT", "ACC", "ACA", "ACG"}),
    W("Tryptophan", "Trp",
            new String[]{"TGG"}),
    Y("Tyrosine", "Tyr",
            new String[]{"TAT", "TAC"}),
    V("Valine", "Val",
            new String[]{"GTT", "GTC", "GTA", "GTG"}),
    X("TERMINATE", "END",
            new String[]{"TAA", "TAG", "TGA"});

    // -- Codon Translation Functions -- //

    // The map for use during translation
    protected static HashMap<String, AminoAcid> CODON_MAP;


    // Build up the CodonMap once the enum is fully initialized
    static {
        buildCodonMap();
    }

    private static void buildCodonMap() {
        CODON_MAP = new HashMap<>();
        for (AminoAcid acid : AminoAcid.values()) {
            registerToCodonMap(acid);
        }
    }

    // Automated registration of codons to the translation map
    protected static void registerToCodonMap(AminoAcid acid) {
        for (ArrayList<NucleicBase> codon : acid.codons) {
            String codon_str = NucleicBase.getSequenceString(codon);
            CODON_MAP.put(codon_str, acid);
        }
    }

    // Build an amino acid sequence from its string representation
    public static ArrayList<AminoAcid> buildFromString(String baseString) {
        ArrayList<AminoAcid> retList = new ArrayList<>();
        char[] charArr = baseString.toUpperCase().toCharArray();
        for (char c : charArr) {
            retList.add(AminoAcid.valueOf(String.valueOf(c)));
        }
        return retList;
    }

    // Get a generic Codon representation of an AminoAcid
    // Useful for debugging, as it is non-random
    @SuppressWarnings("unused")
    public static ArrayList<NucleicBase> getGenericCodon(AminoAcid acid) {
        return acid.codons.get(0);
    }

    // Translate from a nucleic base sequence (string) to a amino acid sequence
    public static ArrayList<AminoAcid> translate(String nucleicSequence) {
        int pos = 0;
        ArrayList<AminoAcid> aminoSequence = new ArrayList<>();
        while (pos <= nucleicSequence.length()-3) {
            // Get the amino acid corresponding to the current frame
            AminoAcid acid = CODON_MAP.get(nucleicSequence.substring(pos, pos+3));
            // If the acid is a termination codon, end prematurely
            if (acid == X) {
                return aminoSequence;
            }
            // Otherwise, build up the sequence
            aminoSequence.add(acid);
            // Shift the codon reading frame
            pos += 3;
        }
        // If we never found a termination codon, return null
        return null;
    }

    // Reverse translate a sequence of AminoAcids into their representative
    // NucleicBase sequence, using random codons when multiple options exist
    public static ArrayList<NucleicBase> reverseTranslate(Collection<AminoAcid> sequence) {
        ArrayList<NucleicBase> result = new ArrayList<>(sequence.size()*3+2);
        // Add the start codon too the beggining
        result.addAll(M.getRandomCodon());
        // Add the sequenced values
        for (AminoAcid acid: sequence) {
            result.addAll(acid.getRandomCodon());
        }
        // Add the termination codon too the end
        result.addAll(X.getRandomCodon());
        return result;
    }

    // -- Alignment/Search components -- //
    // BLOSSUM62 Scoring Matrix; taken from https://www.ncbi.nlm.nih.gov/Class/BLAST/BLOSUM62.txt
    private static final byte[][] BLOSSUM_MATRIX = new byte[][] {
        { 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -4},
        {-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -4},
        {-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3, -4},
        {-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3, -4},
        { 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -4},
        {-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2, -4},
        {-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2, -4},
        { 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -4},
        {-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3, -4},
        {-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -4},
        {-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4},
        {-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2, -4},
        {-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -4},
        {-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -4},
        {-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -4},
        { 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2, -4},
        { 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -4},
        {-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4},
        {-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -4},
        { 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -4},
        {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1}
    };

    public static final byte BLOSSUM_GAP_PENALTY = -4;

    // Get the Blossum Score for
    public static byte getBlossumScore(AminoAcid a, AminoAcid b) {
        return BLOSSUM_MATRIX[a.ordinal()][b.ordinal()];
    }

    public static byte getBlossumDeviation(AminoAcid a, AminoAcid b) {
        return (byte) (getBlossumScore(a, a) - getBlossumScore(a, b));
    }

    // Get the maximum possible Blossum Score for a AminoAcid sequence
    // Kept around in case we decide to change the scoring system again
    @SuppressWarnings("unused")
    public static int getMaxBlossumScore(List<AminoAcid> sequence) {
        int score = 0;
        for (AminoAcid a : sequence) {
            score += getBlossumScore(a, a);
        }
        return score;
    }

    // -- Acid Registration and Attributes -- //
    private final String longName;
    private final String shortName;
    private final ArrayList<ArrayList<NucleicBase>> codons;

    AminoAcid(String longName, String shortName, String[] codons) {
        this.longName = longName;
        this.shortName = shortName;
        this.codons = new ArrayList<>(codons.length);
        for (String codon : codons) {
            this.codons.add(NucleicBase.getSequenceFromString(codon));
        }
    }

    // Kept for debugging
    @SuppressWarnings("unused")
    public String getLongName() {
        return this.longName;
    }

    // Kept for debugging
    @SuppressWarnings("unused")
    public String getShortName() {
        return this.shortName;
    }

    public List<ArrayList<NucleicBase>> getCodons() {
        return new ArrayList<>(this.codons);
    }

    public ArrayList<NucleicBase> getRandomCodon() {
        return new ArrayList<>(this.codons.get(BioCircuitry.RANDOM.nextInt(this.codons.size())));
    }
}