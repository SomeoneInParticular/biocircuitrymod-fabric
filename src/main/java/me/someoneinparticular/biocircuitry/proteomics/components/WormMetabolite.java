/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.proteomics.components;

import me.someoneinparticular.biocircuitry.api.proteomics.IMetabolite;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;

/**
 * Literally just a type wrapper with a convenience function to catch potential
 * spelling mistakes and make cross-mod interaction less painful
 */
public class WormMetabolite implements IMetabolite {
    protected Identifier id;

    // Generic string-based constructor
    public WormMetabolite(Identifier id) {
        this.id = id;
    }

    @Override
    public Identifier getId() {
        return id;
    }

    @Override
    public TranslatableText getTranslatableText() {
        return new TranslatableText(Util.createTranslationKey("metabolite", id));
    }
}
