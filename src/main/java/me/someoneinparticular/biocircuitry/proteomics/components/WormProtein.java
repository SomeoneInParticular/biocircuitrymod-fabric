/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.proteomics.components;

import com.google.common.collect.ImmutableMap;
import me.someoneinparticular.biocircuitry.api.proteomics.IProtein;
import me.someoneinparticular.biocircuitry.data_handlers.WormMetaboliteHandler;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WormProtein implements IProtein<AminoAcid, WormMetabolite> {
    // The sequence associated with this WormProtein
    private final String label;
    private final List<AminoAcid> sequence;
    private final Map<WormMetabolite, Integer> reaction;
    private final int biomass;
    private final int motility;
    private final List<Pair<Item, Float>> products;

    // Full-control constructor
    public WormProtein(String label, List<AminoAcid> sequence,
                       Map<WormMetabolite, Integer> reaction,
                       List<Pair<Item, Float>> products,
                       int biomass, int motility) {
        this.label = label;
        this.sequence = sequence;
        this.reaction = reaction;
        this.biomass = biomass;
        this.motility = motility;
        this.products = products;
    }

    // String-derived sequence constructor
    public WormProtein(String label, String sequence,
                       Map<WormMetabolite, Integer> reaction,
                       List<Pair<Item, Float>> products,
                       int biomass, int motility) {
        this.label = label;
        this.sequence = AminoAcid.buildFromString(sequence);
        this.reaction = reaction;
        this.biomass = biomass;
        this.motility = motility;
        this.products = products;
    }

    // Shortcut for simple reaction-mediating protein
    public WormProtein(String label, String sequence,
                       Map<WormMetabolite, Integer> reaction) {
        this(label, sequence, reaction, null, 0, 0);
    }

    // Shortcut for simple reaction-mediating protein with non-0 dominance
    public WormProtein(String label, String sequence,
                       Map<WormMetabolite, Integer> reaction, int biomass) {
        this(label, sequence, reaction, null, biomass, 0);
    }

    // Shortcut for simple reaction-mediating protein with products
    public WormProtein(String label, String sequence,
                       Map<WormMetabolite, Integer> reaction,
                       List<Pair<Item, Float>> products) {
        // Requires minimal "dominance", otherwise the worm would only eat enough to move around
        this(label, sequence, reaction, products, 1, 0);
    }

    // Network sync constructor
    public static WormProtein build(String label, List<AminoAcid> sequence,
                             Map<Identifier, Integer> reactIds,
                             List<Pair<Item, Float>> products,
                             int biomass, int motility) {
        HashMap<WormMetabolite, Integer> reaction = new HashMap<>();
        for (Map.Entry<Identifier, Integer> entry : reactIds.entrySet()) {
            reaction.put(WormMetaboliteHandler.get(entry.getKey()), entry.getValue());
        }
        return new WormProtein(label, sequence, reaction, products, biomass, motility);
    }

    public String getLabel() {
        return label;
    }

    @Override
    public List<AminoAcid> getSequence() {
        return this.sequence;
    }

    @Override
    public ImmutableMap<WormMetabolite, Integer> getReaction() {
        return ImmutableMap.copyOf(this.reaction);
    }

    @Override
    public Integer getBiomass() {
        return this.biomass;
    }

    @Override
    public int getMotility() {
        return motility;
    }

    @Override
    public List<Pair<Item, Float>> getProducts() {
        return this.products;
    }

    @Override
    public String toString() {
        return label;
    }
}
