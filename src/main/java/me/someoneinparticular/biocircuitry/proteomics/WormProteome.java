/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.proteomics;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.api.proteomics.IProteome;
import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.genomics.WormGenome;
import me.someoneinparticular.biocircuitry.items.ModItems;
import me.someoneinparticular.biocircuitry.items.SequencerItem;
import me.someoneinparticular.biocircuitry.proteomics.components.AminoAcid;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Container which stores and manages proteins for a given worm item. Tracks
 * color coding applied to the proteins contained within itself as well. Can
 * be constructed based off of a genome to automatically identify and store
 * proteins based on their coding sequence.
 */
public class WormProteome implements IProteome<ItemStack, WormProtein> {
    // Data about the actual proteome
    private ArrayList<ArrayList<WormProtein>> proteins;
    private ArrayList<ArrayList<Integer>> spacers;
    private HashMap<WormProtein, Integer> colorMap;

    /**
     * Build a proteome via translating a genome
     * @param genome The genome to use as its basis
     */
    public WormProteome(WormGenome genome) {
        // Initialize required tracking lists
        this.proteins = new ArrayList<>();
        this.spacers = new ArrayList<>();
        // Prepare to find potential sequences
        List<String> chroms = genome.getChromosomeStrings();
        // Prepare our regex pattern
        String startRegex = AminoAcid.M.getCodons().stream()
                .map(c -> c.stream().map(Enum::toString).collect(
                        Collectors.joining("")))
                .collect(Collectors.joining("|", "(", ")"));
        Pattern startPattern = Pattern.compile(startRegex);
        // For each chromosome, find potential proteins from start codons
        for (String chrom : chroms) {
            // Variable tracking the last-seen STOP codon's end position
            int lastPos = 0;
            // Initialize tracking lists
            ArrayList<WormProtein> chromProts = new ArrayList<>();
            ArrayList<Integer> chromSpacers = new ArrayList<>();
            // For each start codon in this chromosome, try and find a valid protein
            Matcher startMatches = startPattern.matcher(chrom);
            while (startMatches.find()) {
                // Determine the best end codon, if any
                int startPos = startMatches.end();
                // See if a valid protein exists for this start codon
                ArrayList<AminoAcid> seq = AminoAcid.translate(chrom.substring(startPos));
                // Terminate if the sequence is just a start codon
                if (seq == null) continue;
                WormProtein prot = WormProteinHandler.search(seq);
                if (prot != null) {
                    // Add the protein found with default color, if one is found
                    chromProts.add(prot);
                    // Determine the spacer between this protein and the prior one
                    // Negative spacer size implies an overlap exists!
                    int posDelta = startPos - lastPos;
                    chromSpacers.add(posDelta);
                    // Shift lastPos to the end of this protein sequence
                    lastPos = startPos + seq.size()*3;
                }
            }
            // Determine the trailing spacer (is always >= 0 due to terminating codons)
            int finalSpacer = chrom.length() - lastPos;
            chromSpacers.add(finalSpacer);
            // Update our proteome's values for this chromosome
            this.proteins.add(chromProts);
            this.spacers.add(chromSpacers);
        }
    }

    /**
     * Rebuild a proteome based on a given worm ItemStack
     * @param stack Stack to pull information from
     */
    public WormProteome(ItemStack stack) {
        this.fromEntityNBT(stack);
    }

    @Override
    public void fromEntityNBT(ItemStack target) {
        // Warn the user if they're pulling the genome from a non-Worm item
        if (target.getItem() != ModItems.WORM) {
            BioCircuitry.LOGGER.warn("Pulling a 'Worm' Proteome from a '" +
                    target.getItem().getTranslationKey() +
                    "' ItemStack. Are you sure you intended to do this?");
        }
        // Fetch the entities tag
        CompoundTag tag = target.getOrCreateTag();
        // Rebuild the protein list
        this.proteinsFromTag(tag);
        // Rebuild the spacer list
        this.spacersFromTag(tag);
        // Rebuild the color map, if any
        this.colorMapFromTag(tag);
    }

    @Override
    public void toEntityNBT(ItemStack target) {
        // Warn the user if the targeted ItemStack is not a worm
        if (target.getItem() != ModItems.WORM) {
            BioCircuitry.LOGGER.warn("Saving a 'Worm' Proteome to a '" +
                    target.getItem().getTranslationKey() +
                    "' ItemStack. Are you sure you intended to do this?");
        }
        // Initialize our tag for this item, getting or creating it as needed
        CompoundTag targetTag = target.getOrCreateTag();
        // Write this proteome's data to the NBT tag
        proteinsToTag(targetTag);
        // Add the list of spacers to our chromosome
        spacersToTag(targetTag);
        // Save the color map of this proteome, if any
        colorMapToTag(targetTag);
    }

    @Override
    public List<WormProtein> getAllProteins() {
        // Flattens the protein list into a list independent of owning chromosome
        List<WormProtein> result = new ArrayList<>();
        proteins.forEach(result::addAll);
        return result;
    }

    public ArrayList<ArrayList<WormProtein>> getProteins() {
        return proteins;
    }

    public ArrayList<ArrayList<Integer>> getSpacers() {
        return spacers;
    }

    public HashMap<WormProtein, Integer> getColorMap() {
        return colorMap;
    }

    public void applyColorMap(ItemStack seqStack) {
        // Confirm the stacks are valid for this process
        if (seqStack == null || seqStack == ItemStack.EMPTY) return;
        CompoundTag seqTag = seqStack.getSubTag(SequencerItem.COLOR_MAP_KEY);
        if (seqTag == null) return;

        // Reset the color map for this proteome if the sequencer has a new one too adopt
        colorMap = new HashMap<>();
        for (String key : seqTag.getKeys()) {
            WormProtein prot = WormProteinHandler.get(new Identifier(key));
            if (prot != null) {
                colorMap.put(prot, seqTag.getInt(key));
            }
        }
    }

    // -- NBT helper functions -- //
    public static final String PROTEINS_KEY = "proteome_proteins";
    public static final String SPACERS_KEY = "proteome_spacers";
    public static final String COLOR_MAP_KEY = "proteome_color_map";
    public static final String SEQUENCED_KEY = "proteome_sequenced";

    /**
     * Write this proteome's proteins (by chromosome) to the target NBT tag
     */
    private void proteinsToTag(CompoundTag tag) {
        // Initialize the tag
        ListTag listTag = new ListTag();
        // Begin writing our proteome data to the tag
        for (ArrayList<WormProtein> subList : this.proteins) {
            ListTag subTag = new ListTag();
            for (WormProtein prot : subList) {
                Identifier id = WormProteinHandler.getId(prot);
                assert id != null;
                subTag.add(StringTag.of(id.toString()));
            }
            listTag.add(subTag);
        }
        tag.put(PROTEINS_KEY, listTag);
    }

    /**
     * Rebuild our proteins from the information contained within an NBT tag
     */
    private void proteinsFromTag(CompoundTag tag) {
        // Reset the protein list
        this.proteins = new ArrayList<>();
        // Confirm this tag contains an appropriate subtag, ending early if not
        if (!tag.contains(PROTEINS_KEY)) {
            return;
        }
        // Get the list that needs to be decompiled
        ListTag listTag = tag.getList(PROTEINS_KEY, 9);
        for (Tag subTag : listTag) {
            // Rebuild the proteins required for this chromosome
            ArrayList<WormProtein> protList = new ArrayList<>();
            for (Tag protTag : ((ListTag) subTag)) {
                String val = protTag.asString();
                Identifier id = new Identifier(val);
                WormProtein prot = WormProteinHandler.get(id);
                if (prot != null) {
                    protList.add(prot);
                }
            }
            this.proteins.add(protList);
        }
    }

    /**
     * Write this proteome's spacers (by chromosome) to the target NBT tag
     */
    private void spacersToTag(CompoundTag tag) {
        ListTag spaceListTag = new ListTag();
        for (ArrayList<Integer> subList : this.spacers) {
            IntArrayTag subTag = new IntArrayTag(subList);
            spaceListTag.add(subTag);
        }
        tag.put(SPACERS_KEY, spaceListTag);
    }

    /**
     * Rebuild our spacers from the information contained within an NBT tag
     */
    private void spacersFromTag(CompoundTag tag) {
        // Reset the spacer list
        this.spacers = new ArrayList<>();
        // Confirm this tag contains an appropriate subtag, ending early if not
        if (!tag.contains(SPACERS_KEY)) {
            return;
        }
        // Get the list that needs to be decompiled
        ListTag listTag = tag.getList(SPACERS_KEY, 11);
        for (Tag subTag : listTag) {
            int[] vals = ((IntArrayTag)subTag).getIntArray();
            ArrayList<Integer> spacerList = new ArrayList<>();
            for (int i : vals) {
                spacerList.add(i);
            }
            this.spacers.add(spacerList);
        }
    }

    /**
     * Write this proteome's color map to the target NBT tag
     */
    private void colorMapToTag(CompoundTag tag) {
        // Exit early if no color map has been created yet
        if (colorMap == null) {
            return;
        }
        // Serialize the full map into NBT for later retrieval
        CompoundTag colorTag = new CompoundTag();
        for (Map.Entry<WormProtein, Integer> mapping : colorMap.entrySet()) {
            Identifier id = WormProteinHandler.getId(mapping.getKey());
            if (id != null) {
                colorTag.putInt(id.toString(), mapping.getValue());
            }
        }
        tag.put(COLOR_MAP_KEY, colorTag);
    }

    /**
     * Rebuild our color map from the information contained within an NBT tag
     */
    private void colorMapFromTag(CompoundTag tag) {
        // Exit early if the tag lacks a color map
        if (!tag.contains(COLOR_MAP_KEY)) {
            return;
        }
        // Otherwise, unpack the data for later usage
        this.colorMap = new HashMap<>();
        CompoundTag colorTag = tag.getCompound(COLOR_MAP_KEY);
        for (String key : colorTag.getKeys()) {
            Identifier id = new Identifier(key);
            WormProtein prot = WormProteinHandler.get(id);
            if (prot != null) {
                colorMap.put(prot, colorTag.getInt(key));
            }
        }
    }

    /**
     * Mark this proteome as having been sequenced, changing how the tooltip is displayed
     */
    public void markSequenced(CompoundTag tag) {
        ByteTag tooltipTag = ByteTag.of(true);
        tag.put(SEQUENCED_KEY, tooltipTag);
    }

    // -- NBT checking functions -- //
    public static boolean hasProteome(CompoundTag tag) {
        // The spacers tag is used because worms can (in theory) exist without a single protein.
        return (tag != null && tag.contains(SPACERS_KEY));
    }

    // Kept around for use by other mods, and debugging
    @SuppressWarnings("unused")
    public static boolean isSequenced(CompoundTag tag) {
        return (tag != null && tag.contains(SEQUENCED_KEY) && tag.getBoolean(SEQUENCED_KEY));
    }
}
