/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.proteomics;

import me.someoneinparticular.biocircuitry.data_handlers.WormFoodNutrientHandler;
import me.someoneinparticular.biocircuitry.proteomics.components.WormMetabolite;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.registry.Registry;
import org.ojalgo.optimisation.Expression;
import org.ojalgo.optimisation.ExpressionsBasedModel;
import org.ojalgo.optimisation.Optimisation;
import org.ojalgo.optimisation.Variable;
import org.ojalgo.structure.Structure1D;

import java.util.*;

/**
 * Manages metabolic pathways within worms, determining the food they will
 * consume and items they will produce to maximize the worm's biomass in its
 * current environment.
 *
 * These item costs and products are tracked, and can be retrieved to
 * mediate item transformation processes within Minecraft.
 */
public class WormPathway {
    // Constants for controlling the effectiveness of this pathway
    private static final int ITEM_MAX = 64;
    private static final int MIN_MOTILITY = 1;
    private static final double WASTE_COST = -0.1;

    // Costs and Products of this pathway while active
    private ArrayList<Pair<Item, Integer>> costs;
    private ArrayList<Pair<Item, Float>> products;
    private double motility;

    // NBT Tag based constructor
    public WormPathway(CompoundTag tag) {
        this.fromNBT(tag);
    }

    // Build-from-resources constructor
    public WormPathway(List<ItemStack> foods, List<WormProtein> proteins) {
        this.rebuildPathway(foods, proteins);
    }

    /**
     * Rebuild this pathway from scratch, given a new set of foods and metabolic
     * proteins
     * @param foods Food available for this pathway to consume
     * @param proteins Proteins available to this pathway to work with
     */
    public void rebuildPathway(List<ItemStack> foods, List<WormProtein> proteins) {
        // Model initialization
        ExpressionsBasedModel model = new ExpressionsBasedModel();
        Map<String, Variable> foodVars = new HashMap<>();
        Map<String, Expression> productExps = new HashMap<>();
        // Create the limiter, used to prevent the worms consuming the earth in one bite
        Expression limiter = model.addExpression("limiter");
        limiter.lower(0).upper(ITEM_MAX);
        // Create and track the "motility" variable, used to determine rate of use
        Expression motility = model.addExpression("motility");
        motility.lower(MIN_MOTILITY);
        // Add the input (food) expressions
        this.addFoodVars(foods, model, limiter, foodVars);
        // Add the transforming (protein) expressions
        this.addProteinVariables(proteins, model, motility, productExps);
        // Add "sink" (waste excretion) variables to prevent locking-up
        this.addSinkVariables(model);
        // Finally, determine the optimal metabolic pathway
        Optimisation.Result result = model.maximise();
        // Reset this pathway to a "not-useful" state if no valid solution exists
        Optimisation.State state = result.getState();
        if (state.isFailure()) {
            this.costs = null;
            this.products = null;
            this.motility = 0;
            return;
        }
        // Get the motility determined by the optimized model
        this.motility = getTotalAmount(model, motility);
        // Determine the foods required to be consumed for one cycle of this pathway
        this.determineCosts(foodVars.values());
        // Determine the items produced (if any) by a cycle of this pathway
        this.determineProducts(model, productExps.values());
    }

    /**
     * Helper function which adds food-item based variables to the model
     * @param foods Items available to eat
     * @param model Model to update
     * @param limiter To-be-updated consumption limiting Expression
     */
    private void addFoodVars(List<ItemStack> foods, ExpressionsBasedModel model,
                             Expression limiter, Map<String, Variable> cachedFoodVars) {
        for (ItemStack foodStack : foods) {
            Item foodItem = foodStack.getItem();
            // Identify the food item's metabolites, if any
            WormFoodNutrientHandler.FoodValue nutVals = WormFoodNutrientHandler.getFoodValue(foodItem);
            // If the food item completely lacks known nutrients, skip to the next food
            if (nutVals == null) continue;
            // Initialize a representative model variable
            String id = Registry.ITEM.getId(foodItem).toString();
            Variable var = cachedFoodVars.getOrDefault(id, null);
            if (var == null) {
                var = model.addVariable(id);
                var.lower(0).upper(foodStack.getCount());
            } else {
                var.upper(var.getUpperLimit().intValue() + foodStack.getCount());
            }
            var.integer(true);
            // Tell the model that this food counts towards the limiter cap
            limiter.set(var, 1);
            // Track the variable for later interpretation
            cachedFoodVars.put(id, var);
            // Update expressions that this variable will effect
            for (Map.Entry<Identifier, Float> entry : nutVals.getNutrients().entrySet()) {
                createOrUpdateExpression(model, var, entry.getKey().toString(), entry.getValue(), false);
            }
        }
    }

    /**
     * Helper function which adds protein based variables to the model
     * @param proteins Items available to eat
     * @param model Model to update
     * @param motility Variable representing the motility this pathway allows for
     * @param cachedProds List of cached expression to be update as needed
     */
    private void addProteinVariables(List<WormProtein> proteins,
                                     ExpressionsBasedModel model,
                                     Expression motility,
                                     Map<String, Expression> cachedProds) {
        // Update the model iteratively for each available item
        for (WormProtein protein : proteins) {
            // Fetch the metabolite map associated with this protein
            Map<WormMetabolite, Integer> reaction = protein.getReaction();
            // Create a new variable representing this protein
            Variable protVar = model.addVariable(protein.getLabel());
            protVar.lower(0).upper(1);
            // Convert the mapped reaction values into an expression for use in the model
            for (Map.Entry<WormMetabolite, Integer> metaPair : reaction.entrySet()) {
                createOrUpdateExpression(model, protVar, metaPair.getKey().getId().toString(), metaPair.getValue(), false);
            }
            // Add the protein's "dominance" (biomass contribution)
            int biomass = protein.getBiomass();
            // Weight is used, as this is what we're actually maximizing
            protVar.weight(biomass);
            // Track the protein's "motility" contribution
            int motVal = protein.getMotility();
            motility.set(protVar, motVal);
            // Check if this protein produces any items during its activity
            List<Pair<Item, Float>> protProds = protein.getProducts();
            if (protProds == null) continue;
            // Add products produced by this protein to the model as well
            for (Pair<Item, Float> product : protein.getProducts()) {
                String id = Registry.ITEM.getId(product.getLeft()).toString();
                createOrUpdateExpression(model, protVar, id, product.getRight(), true);
                // Add this product expression to our cached set for later reference
                cachedProds.computeIfAbsent(id, k -> model.getExpression(id));
            }
        }
    }

    /**
     * Helper with automatically adds "sinks" for excess metabolites,
     * representing waste product excretion at expense to the worm
     * @param model Model to add the sinks too
     */
    private void addSinkVariables(ExpressionsBasedModel model) {
        for (Expression expr : model.getExpressions()) {
            // If the expression is allowed to exist passively (its a product
            // or unique in some way), no sink is needed
            if (expr.getUpperLimit() == null || expr.getUpperLimit().intValue() > 0) {
                continue;
            }
            // Create the "sink" variable for this expression
            Variable var = model.addVariable(expr.getName() + "_sink");
            expr.set(var, -1);
            var.lower(0);
            // Confer the biomass cost incurred by needing to filter out this waste
            var.weight(WASTE_COST);
        }
    }

    /**
     * Updates our costs based on an optimized model's values
     * @param foodVars Variables in the model representing food consumption
     */
    private void determineCosts(Collection<Variable> foodVars) {
        ArrayList<Pair<Item, Integer>> costs = new ArrayList<>();
        // Check every food variable we have tracked for potential usage
        for (Variable food : foodVars) {
            int amount = food.getValue().intValue();
            // Only account for foods which were designated as consumed by the optimization
            if (amount > 0) {
                // Otherwise, find the amount consumed per ingestion cycle
                Item type = Registry.ITEM.get(new Identifier(food.getName()));
                ItemStack cost = new ItemStack(type, amount);
                costs.add(new Pair<>(cost.getItem(), cost.getCount()));
            }
        }
        this.costs = costs.isEmpty() ? null : costs;
    }

    /**
     * Updates our products based on an optimized model's values
     * @param prodExpr Product expressions representing an output item
     */
    private void determineProducts(ExpressionsBasedModel model, Collection<Expression> prodExpr) {
        ArrayList<Pair<Item, Float>> products = new ArrayList<>();
        for (Expression expr : prodExpr) {
            // Get the *sum* of all protein's created amounts for this product
            double amount = getTotalAmount(model, expr);
            if (amount > 0) {
                Item type = Registry.ITEM.get(new Identifier(expr.getName()));
                Pair<Item, Float> product = new Pair<>(type, (float) amount);
                products.add(product);
            }
        }
        this.products = products.isEmpty() ? null : products;
    }

    /**
     * Gets the summed total amount of an expression created in a cycle of
     * the (optimized) model
     * @param model Optimized model to base calculations on
     * @param expr Expression to evaluate for
     * @return The summed total of the expression's value per model cycle
     */
    private double getTotalAmount(ExpressionsBasedModel model, Expression expr) {
        // The collector of summed amounts
        double amount = 0;
        // Get the total motility enabled by the (optimized) model
        for (Structure1D.IntIndex index : expr.getLinearKeySet()) {
            // Fetch the variable associated with this expression index
            Variable var = model.getVariable(index);
            // Determine the usage of this variable
            double usage = var.getValue().doubleValue();
            // Find the per-full-usage amount for this index in this variable
            double perUse = expr.get(index).doubleValue();
            // Add the resulting created amount to our collector
            amount += usage * perUse;
        }
        return amount;
    }

    /**
     * Shortcut to create or update an expression for a variable in our model
     * @param model Model to update
     * @param var Variable to create/update
     * @param label Label to give said variable within the model, if need be
     * @param value Value this expression should associate with the variable
     */
    private void createOrUpdateExpression(ExpressionsBasedModel model, Variable var,
                                          String label, float value, boolean isItem) {
        Expression expr = model.getExpression(label);
        if (expr == null) {
            expr = model.addExpression(label);
            expr.lower(0);
            if (isItem) expr.upper(ITEM_MAX);
            else expr.upper(0);
        }
        expr.set(var, value);
    }

    // -- Getters/Setters -- //
    public ArrayList<Pair<Item, Integer>> getCosts() {
        return costs;
    }

    public ArrayList<Pair<Item, Float>> getProducts() {
        return products;
    }

    public double getMotility() {
        return motility;
    }

    // -- NBT Serialization -- //
    private static final String PRIMARY_KEY = "pathway";
    private static final String COST_KEY = "costs";
    private static final String PROD_KEY = "products";
    private static final String MOT_KEY = "motility";

    /**
     * Reload this WormPathway from data stored within a tag
     */
    public void fromNBT(CompoundTag targetTag) {
        // Get the data in the tag pertaining to a pathway
        CompoundTag tag = (CompoundTag) targetTag.get(PRIMARY_KEY);
        // Set everything as default, not-useable state if no tag data exists
        if (tag == null) {
            this.costs = null;
            this.products = null;
            // Motility <= 0 implies a non-active pathway
            this.motility = 0;
            return;
        }
        // Unpack the cost components
        ListTag costsTag = tag.getList(COST_KEY, 10);
        this.costs = new ArrayList<>(costsTag.size());
        costsTag.forEach(t -> {
            CompoundTag compTag = (CompoundTag) t;
            Item item = Registry.ITEM.get(new Identifier(compTag.getString("item")));
            int amount = compTag.getInt("amount");
            this.costs.add(new Pair<>(item, amount));
        });
        // Unpack the product components
        ListTag prodTag = tag.getList("products", 10);
        this.products = new ArrayList<>(prodTag.size());
        prodTag.forEach(t -> {
            CompoundTag compTag = (CompoundTag) t;
            Item item = Registry.ITEM.get(new Identifier(compTag.getString("item")));
            float amount = compTag.getFloat("amount");
            this.products.add(new Pair<>(item, amount));
        });
        // Unpack the motility value for this pathway
        this.motility = tag.getDouble(MOT_KEY);
    }

    /**
     * Save this WormPathway's data into a target tag
     */
    public void toNBT(CompoundTag targetTag) {
        // Exit if this pathway is in a unusable state (no motility)
        if (this.motility <= 0) return;
        // Initialize the to-be-saved tag
        CompoundTag tag = new CompoundTag();
        // Save our costs
        ListTag costTag = new ListTag();
        this.costs.forEach(s -> {
            CompoundTag pairTag = new CompoundTag();
            pairTag.putString("item", Registry.ITEM.getId(s.getLeft()).toString());
            pairTag.putInt("amount", s.getRight());
            costTag.add(pairTag);
        });
        tag.put(COST_KEY, costTag);
        // Save our products
        ListTag prodTag = new ListTag();
        this.products.forEach(s -> {
            CompoundTag pairTag = new CompoundTag();
            pairTag.putString("item", Registry.ITEM.getId(s.getLeft()).toString());
            pairTag.putFloat("amount", s.getRight());
            prodTag.add(pairTag);
        });
        tag.put(PROD_KEY, prodTag);
        // Save our motility
        tag.putDouble(MOT_KEY, this.motility);
        // Finally, save all of this information to the targeted tag
        targetTag.put(PRIMARY_KEY, tag);
    }
}
