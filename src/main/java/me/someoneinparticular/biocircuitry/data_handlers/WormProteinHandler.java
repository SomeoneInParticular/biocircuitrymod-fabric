/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.data_handlers;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.lib.search_trie.ProteinTrie;
import me.someoneinparticular.biocircuitry.networking.WormProteinSyncPacketManager;
import me.someoneinparticular.biocircuitry.proteomics.components.AminoAcid;
import me.someoneinparticular.biocircuitry.proteomics.components.WormMetabolite;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.item.Item;
import net.minecraft.resource.ResourceManager;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.registry.Registry;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class WormProteinHandler {

    // TODO; Make this a configuration option
    protected static float SEARCH_THRESHOLD = 0.01f;
    protected static final ProteinTrie SEARCH_TRIE = new ProteinTrie();
    protected static final BiMap<Identifier, WormProtein> REGISTRY = HashBiMap.create();

    public static void register(Identifier id, WormProtein protein) {
        REGISTRY.put(id, protein);
        SEARCH_TRIE.insert(protein);
    }

    public static WormProtein get(Identifier id) {
        return REGISTRY.get(id);
    }

    public static Identifier getId(WormProtein prot) {
        return REGISTRY.inverse().get(prot);
    }

    public static Set<WormProtein> getAll() {
        return REGISTRY.values();
    }

    public static WormProtein search(List<AminoAcid> seq) {
        return SEARCH_TRIE.search(seq, (int) (AminoAcid.getMaxBlossumScore(seq)*SEARCH_THRESHOLD));
    }

    public static void reset() {
        REGISTRY.clear();
        SEARCH_TRIE.clear();
    }

    // A useful, non-registered protein for debugging uses
    public static WormProtein dummy = new WormProtein("dummy", "ATGC", new HashMap<>());

    protected static class WormProteinResourceReloadListener implements SimpleSynchronousResourceReloadListener {
        @Override
        public Identifier getFabricId() {
            return BioCircuitry.id("proteins", "loader");
        }

        @Override
        public void apply(ResourceManager manager) {
            // Reset the registry to blank
            reset();

            // Find all resources associated with worm proteins
            Collection<Identifier> resources = manager.findResources("biocircuitry/proteins", (s -> s.endsWith(".json")));

            // Begin loading the contents of the resources found
            JsonParser parser = new JsonParser();
            for (Identifier resource : resources) {
                try (InputStreamReader reader = new InputStreamReader(manager.getResource(resource).getInputStream())) {
                    // Pull the protein elements
                    JsonObject proteinMap = parser.parse(reader).getAsJsonObject().getAsJsonObject("proteins");
                    for (Map.Entry<String, JsonElement> proteinData : proteinMap.entrySet()) {
                        Identifier id = new Identifier(resource.getNamespace(), proteinData.getKey());
                        WormProtein protein = unpackJson(proteinData.getValue().getAsJsonObject());
                        register(id, protein);
                    }
                }
                // Probably unneeded, but theoretically the file could be deleted pre-run post-check
                catch (IOException e) {
                    BioCircuitry.LOGGER.error(String.format("Failed to load %s, file was deleted.", resource.toString()));
                    e.printStackTrace();
                }
            }
        }

        /**
         * Unpack a JSON object to create a WormProtein
         * @param json The JSON object to unpack
         * @return The worm protein created from the unpacked data
         */
        protected static WormProtein unpackJson(JsonObject json) {
            // Pull out required elements of the protein
            String id = json.getAsJsonPrimitive("id").getAsString();
            String sequence = json.getAsJsonPrimitive("sequence").getAsString();
            Map<WormMetabolite, Integer> reaction = unpackReaction(json.getAsJsonObject("reaction"));
            // Pull out optional elements, with defaults
            int biomass = 0;
            if (json.has("biomass")) {
                biomass = json.getAsJsonPrimitive("biomass").getAsInt();
            }
            int motility = 0;
            if (json.has("motility")) {
                motility = json.getAsJsonPrimitive("motility").getAsInt();
            }
            List<Pair<Item, Float>> products = null;
            if (json.has("products")) {
                products = unpackProducts(json.getAsJsonObject("products"));
            }
            return new WormProtein(id, sequence, reaction, products, biomass, motility);
        }

        /**
         * Unpack a JSON element into a reaction map for use in WormProtein creation
         * @param json The JSON object to unpack
         * @return The unpacked map (can be empty!)
         */
        protected static Map<WormMetabolite, Integer> unpackReaction(JsonObject json) {
            // Initiate the metabolite map
            Map<WormMetabolite, Integer> reaction = new HashMap<>();
            // Unpack the JSON
            for (Map.Entry<String, JsonElement> elem : json.entrySet()) {
                Identifier id = new Identifier(elem.getKey());
                int value = elem.getValue().getAsInt();
                reaction.put(WormMetaboliteHandler.get(id), value);
            }
            return reaction;
        }

        /**
         * Unpack a JSON element into a list of products for use in WormProtein creation
         * @param json The JSON object to unpack
         * @return The unpacked map (can be null)
         */
        protected static List<Pair<Item, Float>> unpackProducts(JsonObject json) {
            // Check if the json object is empty, terminate early with null
            if (json.entrySet().isEmpty()) {
                return null;
            }
            // Initialize the product list
            List<Pair<Item, Float>> products = new ArrayList<>();
            // Unpack the JSON
            for (Map.Entry<String, JsonElement> elem : json.entrySet()) {
                Identifier id = new Identifier(elem.getKey());
                Item product = Registry.ITEM.get(id);
                float value = elem.getValue().getAsFloat();
                products.add(new Pair<>(product, value));
            }
            return products;
        }
    }

    public static void init() {
        // Prepare the resource reload listener
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(new WormProteinResourceReloadListener());

        // Tell Minecraft to send synchronization packets when new players log in
        ServerPlayConnectionEvents.JOIN.register((handler, sender, server) -> WormProteinSyncPacketManager.synchronizeProteins(handler.player));
    }
}
