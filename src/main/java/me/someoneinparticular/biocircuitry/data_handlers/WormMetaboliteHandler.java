/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.data_handlers;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.networking.WormMetaboliteSyncPacketManager;
import me.someoneinparticular.biocircuitry.proteomics.components.WormMetabolite;
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.resource.ResourceManager;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Set;

public class WormMetaboliteHandler {
    // Metabolites acts as a simple registry type
    protected static BiMap<Identifier, WormMetabolite> REGISTRY = HashBiMap.create();

    // Register a new metabolite from a label alone
    public static WormMetabolite register(Identifier id) {
        WormMetabolite metab = new WormMetabolite(id);
        REGISTRY.put(id, metab);
        return metab;
    }

    public static WormMetabolite get(Identifier id) {
        return REGISTRY.get(id);
    }

    public static Identifier getId(WormMetabolite metab) {
        return REGISTRY.inverse().get(metab);
    }

    public static Set<WormMetabolite> getAll() {
        return REGISTRY.values();
    }

    public static void reset() {
        REGISTRY.clear();
    }

    protected static class WormMetaboliteResourceReloadListener implements SimpleSynchronousResourceReloadListener {
        @Override
        public Identifier getFabricId() {
            return BioCircuitry.id("metabolites", "loader");
        }

        @Override
        public void apply(ResourceManager manager) {
            // Reset the registry to blank
            reset();

            // Find all resources associated with the metabolite type
            Collection<Identifier> resources = manager.findResources("biocircuitry/metabolites", (s -> s.endsWith(".json")));

            // Begin loading the contents of the resources found
            JsonParser parser = new JsonParser();
            for (Identifier resource : resources) {
                try (InputStreamReader reader = new InputStreamReader(manager.getResource(resource).getInputStream())) {
                    JsonElement element = parser.parse(reader);
                    for (JsonElement comp : element.getAsJsonArray()) {
                        Identifier id = new Identifier(
                                resource.getNamespace(),
                                comp.getAsString()
                        );
                        register(id);
                    }
                }
                // Probably unneeded, but theoretically the file could be deleted pre-run post-check
                catch (IOException e) {
                    BioCircuitry.LOGGER.error(String.format("Failed to load %s, file was deleted.", resource.toString()));
                    e.printStackTrace();
                }
                // Catches cases where the file is improperly formatted (not in JsonArray format)
                catch (IllegalStateException e) {
                    BioCircuitry.LOGGER.warn(String.format("Failed to load %s, file was incorrectly formatted.", resource.toString()));
                }
            }
        }
    }

    public static void init() {
        // Prepare our resource reload listener
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(new WormMetaboliteResourceReloadListener());

        // Tell Minecraft to send synchronization packets when new players log in
        ServerPlayConnectionEvents.JOIN.register((handler, sender, server) -> WormMetaboliteSyncPacketManager.synchronizeMetabolites(handler.player));
    }
}
