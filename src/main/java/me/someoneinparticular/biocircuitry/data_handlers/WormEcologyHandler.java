/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.data_handlers;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.io.Files;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.ecology.WormEcology;
import me.someoneinparticular.biocircuitry.ecology.sequence_generators.*;
import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.resource.ResourceManager;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class WormEcologyHandler {
    // Keys for the search table's predicates
    protected static final String TEMP_LOWER_KEY = "temp_lower";
    protected static final String TEMP_UPPER_KEY = "temp_upper";
    protected static final String BIOME_CATEGORY_KEY = "biome_category";
    protected static final String BIOME_LIST_KEY = "biome_list";
    protected static final String HEIGHT_LOWER_KEY = "height_lower";
    protected static final String HEIGHT_UPPER_KEY = "height_upper";
    protected static final String BAIT_REQUIRED_KEY = "bait_required";
    protected static final String BAIT_ITEMS_KEY = "bait_items";
    protected static final String SPAWN_BLOCKS_KEY = "spawn_blocks";
    protected static final String ECOLOGY_KEY = "ecology";
    // The list of predicates that can be searched in the search table
    protected static final String[] SEARCH_COLUMNS = new String[] {
            TEMP_LOWER_KEY,
            TEMP_UPPER_KEY,
            BIOME_CATEGORY_KEY,
            BIOME_LIST_KEY,
            HEIGHT_LOWER_KEY,
            HEIGHT_UPPER_KEY,
            BAIT_REQUIRED_KEY,
            BAIT_ITEMS_KEY,
            SPAWN_BLOCKS_KEY,
            ECOLOGY_KEY
    };

    // The search table for the ecologies
    protected static HashBasedTable<Identifier, String, Object> searchTable = HashBasedTable.create(256, SEARCH_COLUMNS.length);

    protected static class WormEcologyResourceReloadListener implements SimpleSynchronousResourceReloadListener {

        @Override
        public Identifier getFabricId() {
            return BioCircuitry.id("ecologies", "loader");
        }

        @Override
        public void apply(ResourceManager manager) {
            // Reset the search table
            searchTable.clear();

            // Find all resources associated with worm ecology
            Collection<Identifier> resources = manager.findResources("biocircuitry/ecologies", (s -> s.endsWith(".json")));

            // Initialize a map to keep track of useful elements
            //noinspection UnstableApiUsage
            Multimap<Identifier, Pair<Identifier, JsonObject>> orphanChildren = MultimapBuilder.hashKeys().arrayListValues().build();
            HashMap<Identifier, HashMap<Identifier, AbstractSequenceGenerator<NucleicBase>>> constantMap = new HashMap<>();

            // Parse through the resource
            JsonParser parser = new JsonParser();
            for (Identifier resource : resources) {
                //noinspection UnstableApiUsage
                Identifier id = new Identifier(
                        resource.getNamespace(),
                        Files.getNameWithoutExtension(resource.getPath())
                );
                try (InputStreamReader reader = new InputStreamReader(manager.getResource(resource).getInputStream())) {
                    JsonObject element = parser.parse(reader).getAsJsonObject();
                    // Test to see if the element is currently an orphan
                    if (element.has("parent")) {
                        Identifier parentId = new Identifier(element.get("parent").getAsString());
                        if (!constantMap.containsKey(parentId)) {
                            // If so, add it to the list for later adoption
                            orphanChildren.put(parentId, new Pair<>(id, element));
                            continue;
                        }
                    }
                    // Unpack the contents of this JSON
                    unpackEcologyJson(id, element, orphanChildren, constantMap);
                }
                // Probably unneeded, but theoretically the file could be deleted pre-run post-check
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        protected static void unpackEcologyJson(Identifier id, JsonObject json, Multimap<Identifier, Pair<Identifier, JsonObject>> orphanChildren, HashMap<Identifier, HashMap<Identifier, AbstractSequenceGenerator<NucleicBase>>> constants) {
            // Check that an entry for this ID doesn't already exist
            if (searchTable.containsRow(id)) {
                // TODO: Replace with more nuanced replacement/deletion logic
                BioCircuitry.LOGGER.warn("Ecology with the ID " + id.toString() + " already exists; skipping");
                return;
            }
            // Initialize the constants for this Ecology description
            HashMap<Identifier, AbstractSequenceGenerator<NucleicBase>> localConstants = new HashMap<>();
            constants.put(id, localConstants);
            // Inherit all constants from the requested parent, if any
            if (json.has("parent")) {
                Identifier parentId = new Identifier(json.get("parent").getAsString());
                HashMap<Identifier, AbstractSequenceGenerator<NucleicBase>> priorConstants = constants.get(parentId);
                priorConstants.forEach(localConstants::put);
            }
            // Initialize and store all new constants in this map, if any
            if (json.has("constants")) {
                JsonObject localConstantDefinitions = json.get("constants").getAsJsonObject();
                for (Map.Entry<String, JsonElement> c : localConstantDefinitions.entrySet()) {
                    AbstractSequenceGenerator<NucleicBase> generator = unpackGenerator(c.getValue().getAsJsonObject(), constants.get(id));
                    assert generator != null;
                    Identifier constantId = new Identifier(id.getNamespace(), c.getKey());
                    localConstants.put(constantId, generator);
                }
            }
            // Initialize the genome generator and its predicates, if this resource represents one
            if (json.has("generator")) {
                // Check to make sure a predicate element exists (fail-faster)
                assert json.has("predicates");
                // Initiate the generator first, before its predicates (fail-faster)
                ArrayList<AbstractSequenceGenerator<NucleicBase>> chromGeneratorList = new ArrayList<>();
                JsonArray chromSpecArray = json.getAsJsonArray("generator");
                for (JsonElement chromGeneratorArray : chromSpecArray) {
                    ArrayList<AbstractSequenceGenerator<NucleicBase>> generators = new ArrayList<>();
                    for (JsonElement generatorSpec : chromGeneratorArray.getAsJsonArray()) {
                        AbstractSequenceGenerator<NucleicBase> generator = unpackGenerator(generatorSpec.getAsJsonObject(), localConstants);
                        generators.add(generator);
                    }
                    chromGeneratorList.add(new MultiSequenceGenerator(generators));
                }
                WormEcology ecology = new WormEcology(chromGeneratorList, 2);
                searchTable.put(id, ECOLOGY_KEY, ecology);
                // Register the predicates for the ecology
                unpackPredicates(json.getAsJsonObject("predicates"), id);
            }
            // 'Adopt' any orphaned ecologies which designated this ecology as its parent
            Collection<Pair<Identifier, JsonObject>> orphans = orphanChildren.get(id);
            for (Pair<Identifier, JsonObject> o : orphans) {
                unpackEcologyJson(o.getLeft(), o.getRight(), orphanChildren, constants);
            }
        }

        protected static AbstractSequenceGenerator<NucleicBase> unpackGenerator(JsonObject json, HashMap<Identifier, AbstractSequenceGenerator<NucleicBase>> constants) {
            String type = json.get("type").getAsString();
            switch (type) {
                case "C":
                    // Constant reference
                    Identifier constantId = new Identifier(json.get("id").getAsString());
                    AbstractSequenceGenerator<NucleicBase> generator = constants.get(constantId);
                    if (generator == null) {
                        BioCircuitry.LOGGER.warn("Constant does not exist: " + constantId);
                    }
                    return generator;
                case "M":
                    // Chained list of other Multiple other generators
                    JsonArray multiValueContents = json.get("value").getAsJsonArray();
                    List<AbstractSequenceGenerator<NucleicBase>> sequenceGenerators = new ArrayList<>(multiValueContents.size());
                    for (JsonElement x : multiValueContents) {
                        sequenceGenerators.add(unpackGenerator(x.getAsJsonObject(), constants));
                    }
                    return new MultiSequenceGenerator(sequenceGenerators);
                case "P":
                    // Reverse translated Protein sequence
                    Identifier proteinId = new Identifier(json.get("id").getAsString());
                    return new ProteinBasedSequenceGenerator(proteinId);
                case "R":
                    // Random variant of other generator product
                    JsonObject value = json.get("value").getAsJsonObject();
                    float rate = json.get("rate").getAsFloat();
                    return new RandomVariantGenerator(unpackGenerator(value, constants), rate);
                case "S":
                    // Explicit sequence
                    String sequenceValue = json.get("value").getAsString();
                    return new SimpleSequenceGenerator(NucleicBase.getSequenceFromString(sequenceValue));
                case "W":
                    // Weighted random choice of set of sequence generators
                    JsonArray generatorOptionContents = json.get("value").getAsJsonArray();
                    JsonArray optionWeightContents = json.get("weights").getAsJsonArray();
                    List<AbstractSequenceGenerator<NucleicBase>> sequenceChoices = new ArrayList<>(generatorOptionContents.size());
                    List<Integer> optionWeights = new ArrayList<>(optionWeightContents.size());
                    for (JsonElement x : generatorOptionContents) {
                        sequenceChoices.add(unpackGenerator(x.getAsJsonObject(), constants));
                    }
                    for (JsonElement x : optionWeightContents) {
                        optionWeights.add(x.getAsInt());
                    }
                    return new WeightedOptionSequenceGenerator(sequenceChoices, optionWeights);
                default:
                    BioCircuitry.LOGGER.error("INVALID SEQUENCE GENERATOR TYPE '" + type + "'");
                    return null;
            }
        }

        protected static void unpackPredicates(JsonObject json, Identifier index) {
            // Pull out the row that should be modified
            Map<String, Object> row = searchTable.row(index);

            // Add temperature predicates
            if (json.has(TEMP_LOWER_KEY)) {
                row.put(TEMP_LOWER_KEY, json.get(TEMP_LOWER_KEY).getAsFloat());
            }
            if (json.has(TEMP_UPPER_KEY)) {
                row.put(TEMP_UPPER_KEY, json.get(TEMP_UPPER_KEY).getAsFloat());
            }

            // Add biome predicates
            if (json.has(BIOME_CATEGORY_KEY)) {
                row.put(BIOME_CATEGORY_KEY, Biome.Category.valueOf(json.get(BIOME_CATEGORY_KEY).getAsString().toUpperCase()));
            }
            if (json.has(BIOME_LIST_KEY)) {
                JsonArray biomeIdRefs = json.getAsJsonArray(BIOME_LIST_KEY);
                // Stored as IDs because data-packs can load biomes after they're requested
                Collection<Identifier> biomes = new ArrayList<>(biomeIdRefs.size());
                for (JsonElement e : json.getAsJsonArray(BIOME_LIST_KEY)) {
                    biomes.add(new Identifier(e.getAsString()));
                }
                row.put(BIOME_LIST_KEY, biomes);
            }

            // Add height predicates
            if (json.has(HEIGHT_LOWER_KEY)) {
                row.put(HEIGHT_LOWER_KEY, json.get(HEIGHT_LOWER_KEY).getAsInt());
            }
            if (json.has(HEIGHT_UPPER_KEY)) {
                row.put(HEIGHT_UPPER_KEY, json.get(HEIGHT_UPPER_KEY).getAsInt());
            }

            // Bait predicates
            if (json.has(BAIT_REQUIRED_KEY)) {
                row.put(BAIT_REQUIRED_KEY, json.get(BAIT_REQUIRED_KEY).getAsBoolean());
            }
            if (json.has(BAIT_ITEMS_KEY)) {
                JsonArray baitIdemIds = json.getAsJsonArray(BAIT_ITEMS_KEY);
                Set<Item> baitItemSet = new HashSet<>(baitIdemIds.size());
                for (JsonElement e : json.getAsJsonArray(BAIT_ITEMS_KEY)) {
                    baitItemSet.add(Registry.ITEM.get(new Identifier(e.getAsString())));
                }
                row.put(BAIT_ITEMS_KEY, baitItemSet);
            }

            // Spawn block predicate
            if (json.has(SPAWN_BLOCKS_KEY)) {
                JsonArray spawnBlockIds = json.getAsJsonArray(SPAWN_BLOCKS_KEY);
                Set<Block> spawnBlockSet = new HashSet<>(spawnBlockIds.size());
                for (JsonElement e : json.getAsJsonArray(SPAWN_BLOCKS_KEY)) {
                    spawnBlockSet.add(Registry.BLOCK.get(new Identifier(e.getAsString())));
                }
                row.put(BAIT_ITEMS_KEY, spawnBlockSet);
            }
        }
    }

    public static void init() {
        // Prepare our resource reload listener
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(new WormEcologyResourceReloadListener());
    }

    public static Collection<WormEcology> query(Float temp, Integer height, Biome biome, Item bait, Block spawnBlock) {
        // Predicate for temperature query
        Predicate<Map<String, Object>> tempPredicate = row -> (Float) row.get(TEMP_LOWER_KEY) <= temp && (Float) row.get(TEMP_UPPER_KEY) >= temp;
        // Predicate for height query
        Predicate<Map<String, Object>> heightPredicate = row -> (Integer) row.get(HEIGHT_LOWER_KEY) <= height && (Integer) row.get(HEIGHT_UPPER_KEY) >= height;
        // Predicate for biome query
        Predicate<Map<String, Object>> biomePredicate = row -> {
            //noinspection unchecked
            Set<Biome> biomeList = (Set<Biome>) row.get(BIOME_LIST_KEY);
            if (biomeList == null) {
                return row.get(BIOME_CATEGORY_KEY) == null || row.get(BIOME_CATEGORY_KEY) == biome.getCategory();
            }
            return biomeList.contains(biome) || row.get(BIOME_CATEGORY_KEY) == biome.getCategory();

        };
        // Predicate for item query
        Predicate<Map<String, Object>> baitItemPredicate = row -> {
            boolean baitRequired = row.get(BAIT_REQUIRED_KEY) != null && (Boolean) row.get(BAIT_REQUIRED_KEY);
            if (baitRequired) {
                //noinspection unchecked
                Set<Item> baitSet = (Set<Item>) row.get(BAIT_ITEMS_KEY);
                return baitSet == null || baitSet.contains(bait);
            }
            return true;
        };
        // Predicate for spawn block
        Predicate<Map<String, Object>> spawnBlockPredicate = row -> {
            //noinspection unchecked
            Set<Block> blockSet = (Set<Block>) row.get(SPAWN_BLOCKS_KEY);
            return blockSet == null || blockSet.contains(spawnBlock);
        };

        // Filter and reduce the search tables content to values matching the predicate
        Predicate<Map<String, Object>> searchPredicate = tempPredicate.and(heightPredicate).and(biomePredicate).and(baitItemPredicate).and(spawnBlockPredicate);
        List<Map<String, Object>> validRows = searchTable.rowMap().values().stream().filter(searchPredicate).collect(Collectors.toList());

        // Return the ecologies contained within these rows
        return validRows.stream().map(row -> (WormEcology) row.get(ECOLOGY_KEY)).collect(Collectors.toList());
    }
}
