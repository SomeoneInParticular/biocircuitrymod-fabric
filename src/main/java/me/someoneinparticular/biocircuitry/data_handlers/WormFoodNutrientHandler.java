/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.data_handlers;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.item.Item;
import net.minecraft.resource.ResourceManager;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class WormFoodNutrientHandler {

    protected static final BiMap<Identifier, FoodValue> REGISTRY = HashBiMap.create();

    protected static class WormFoodNutrientResourceReloadListener implements SimpleSynchronousResourceReloadListener {
        @Override
        public Identifier getFabricId() {
            return BioCircuitry.id("nutrients", "loader");
        }

        @Override
        public void apply(ResourceManager manager) {
            // Reset the registry to blank
            REGISTRY.clear();

            // Find all resources associated with the metabolite type
            Collection<Identifier> resources = manager.findResources("biocircuitry/nutrients", (s -> s.endsWith(".json")));

            // Load the data pack contents into our registries
            JsonParser parser = new JsonParser();
            for (Identifier resource : resources) {
                try (InputStreamReader reader = new InputStreamReader(manager.getResource(resource).getInputStream())) {
                    JsonElement element = parser.parse(reader);
                    interpretJson(element.getAsJsonObject());
                }
                // Probably unneeded, but theoretically the file could be deleted pre-run post-check
                catch (IOException e) {
                    BioCircuitry.LOGGER.error(String.format("Failed to load %s, file was deleted.", resource.toString()));
                    e.printStackTrace();
                }
                // Catches cases where the file is improperly formatted (not in JsonArray format)
                catch (IllegalStateException e) {
                    BioCircuitry.LOGGER.warn(String.format("Failed to load %s, file was incorrectly formatted.", resource.toString()));
                }
            }
        }

        protected static void interpretJson(JsonObject json) {
            // Iterate through the components
            for (Map.Entry<String, JsonElement> jsonElem : json.entrySet()) {
                Identifier itemId = new Identifier(jsonElem.getKey());
                FoodValue foodValue = register(itemId);
                // Begin the updating process
                JsonObject elements = jsonElem.getValue().getAsJsonObject();
                for (Map.Entry<String, JsonElement> nutrientElem : elements.entrySet()) {
                    Identifier metabId = new Identifier(nutrientElem.getKey());
                    float nutValue = nutrientElem.getValue().getAsFloat();
                    foodValue.addNutrient(metabId, nutValue);
                }
            }
        }
    }

    // Data wrapper class to make nutrient values for an item easier to work with
    public static class FoodValue {
        private final HashMap<Identifier, Float> nutrients;

        // Generate a new FoodValue for the item with the given id
        public FoodValue() {
            this.nutrients = new HashMap<>();
        }

        // Add a new nutrient to the FoodValue mapping via the Metabolite's id
        // Returns the updated value so it can be chained for debug purposes
        @SuppressWarnings("UnusedReturnValue")
        public FoodValue addNutrient(Identifier metab_id, float val) {
            nutrients.put(metab_id, val);
            return this;
        }

        // Reset the nutrient values contained, in preparation to replace it
        public void resetNutrients() {
            nutrients.clear();
        }

        // Get the list of nutrients associated with this mapping
        public HashMap<Identifier, Float> getNutrients() {
            return nutrients;
        }
    }


    public static FoodValue register(Identifier itemId) {
        FoodValue value = new FoodValue();
        REGISTRY.put(itemId, value);
        return value;
    }

    public static FoodValue get(Identifier itemId) {
        return REGISTRY.get(itemId);
    }

    public static FoodValue getFoodValue(Item item) {
        return REGISTRY.get(Registry.ITEM.getId(item));
    }

    public static void init() {
        // Prepare the resource reload listener
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(new WormFoodNutrientResourceReloadListener());
    }
}
