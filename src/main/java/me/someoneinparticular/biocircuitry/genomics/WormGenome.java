/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.genomics;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.api.genomics.IGenome;
import me.someoneinparticular.biocircuitry.genomics.components.NeedlemanWunschAlignment;
import me.someoneinparticular.biocircuitry.genomics.components.WormChromosome;
import me.someoneinparticular.biocircuitry.items.ModItems;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.ByteArrayTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WormGenome implements IGenome<ItemStack> {
    // TODO: Make these linked to a configuration variable
    // Configuration to limit the maximum chromosome count of the list
    protected static final int MAX_CHROM_COUNT = 10;
    // Minimum alignment score required for chromosomes to form a "recombinant"
    protected final static int MIN_SCORE = 0;

    // List of Chromosomes this Genome contains
    private ArrayList<WormChromosome> chromosomes = null;
    // The chromosomal sequence alignments, based on chromosome alignment scores
    private ArrayList<NeedlemanWunschAlignment> alignments = null;
    // Chromosomes which, either due to poor pairing relation or being edged
    // out, must undergo NonDisjunction
    private ArrayList<WormChromosome> orphans = null;

    /**
     * Build a WormGenome from a raw list of worm chromosomes.
     * @param chromosomes The chromosomes to store within the genome
     */
    public WormGenome(List<WormChromosome> chromosomes) {
        this.chromosomes = new ArrayList<>(chromosomes);
    }

    /**
     * Build a genome which consists of a number of composite chromosomes
     * @param chromosomes The chromosomes to be placed within the genome
     */
    public WormGenome(WormChromosome... chromosomes) {
        this(Arrays.asList(chromosomes));
    }

    /**
     * Build a WormGenome by pulling from an ItemStack's NBT data
     * @param target Target ItemStack to read from
     */
    public WormGenome(ItemStack target) {
        this.fromEntityNBT(target);
    }

    /**
     * Generate a new WormGenome from (presumably half size) genomes
     * @param gamete1 The first genome to be added
     * @param gamete2 The second genome to be added
     */
    public WormGenome(WormGenome gamete1, WormGenome gamete2) {
        this.chromosomes = new ArrayList<>();
        this.chromosomes.addAll(gamete1.chromosomes);
        this.chromosomes.addAll(gamete2.chromosomes);
    }

    // -- Getters/Setters -- //
    public ArrayList<WormChromosome> getChromosomes() {
        return chromosomes;
    }

    // -- NBT Helpers -- //
    @Override
    public void fromEntityNBT(ItemStack target) {
        // Pull the tag describing the genome, if it exists
        if (!target.getItem().equals(ModItems.WORM)) {
            BioCircuitry.LOGGER.warn("Pulled a 'Worm' Genome from an '" +
                    target.getItem().getTranslationKey() +
                    "' ItemStack. Are you sure you intended to do this?");
        }
        CompoundTag target_tag = target.getTag();
        // If the tag does not exist, initialize am empty Genome
        if (target_tag == null) {
            BioCircuitry.LOGGER.warn("You tried to pull a Genome from a Worm without any NBT!");
            this.chromosomes = null;
            return;
        }
        CompoundTag genome_tag = target_tag.getCompound("genome");
        if (genome_tag == null) {
            BioCircuitry.LOGGER.warn("You tried to pull a Genome from a Worm without one!");
            this.chromosomes = null;
            return;
        }
        // 7 = ByteArrayTag type index
        ListTag chromosome_tag = genome_tag.getList("chromosomes", 7);
        if (chromosome_tag == null) {
            BioCircuitry.LOGGER.warn("You have a genome with no chromosomes!");
            return;
        }
        this.chromosomes = new ArrayList<>(chromosome_tag.size());
        // Rebuild the chromosomes and add them to our Genome
        for (Tag chrom_tag : chromosome_tag) {
            WormChromosome chrom = new WormChromosome();
            chrom.fromNBT((ByteArrayTag) chrom_tag);
            this.chromosomes.add(chrom);
        }
    }

    @Override
    public void toEntityNBT(ItemStack target) {
        if (target.getItem() != ModItems.WORM) {
            BioCircuitry.LOGGER.warn("Saving a 'Worm' Genome to a '" +
                    target.getItem().getTranslationKey() +
                    "' ItemStack. Are you sure you intended to do this?");
        }
        ListTag chrom_list_nbt = new ListTag();
        for (WormChromosome chrom : this.chromosomes) {
            ByteArrayTag chrom_nbt = chrom.toNBT();
            chrom_list_nbt.add(chrom_nbt);
        }
        CompoundTag gen_tag = new CompoundTag();
        gen_tag.put("chromosomes", chrom_list_nbt);
        target.putSubTag("genome", gen_tag);
    }

    // -- Modifying/Producing Functions -- //
    @Override
    public WormGenome buildGamete() {
        // Initialization
        ArrayList<WormChromosome> chromList = new ArrayList<>();
        // If this genome has not already had alignments generated, generate them
        if (this.alignments == null) {
            this.rebuildAlignments();
        }
        // For each alignment, generate a recombinant
        for (NeedlemanWunschAlignment a : this.alignments) {
            chromList.add(a.makeRecombinant());
        }
        // For each orphan, determine if non-disjunction occurred
        for (WormChromosome c : this.orphans) {
            if (BioCircuitry.RANDOM.nextBoolean()) {
                chromList.add(c);
            }
        }
        return new WormGenome(chromList);
    }

    /**
     * Build the optimal alignment pairings for this genome based on
     * the chromosomes of this Genome's similarity to one another
     */
    private void rebuildAlignments() {
        // List of chromosomes left to be tested for optimal alignments
        ArrayList<WormChromosome> todoList = new ArrayList<>(this.chromosomes);
        // Reset the alignment and orphan list to avoid infinite growth
        this.alignments = new ArrayList<>();
        this.orphans = new ArrayList<>();
        // Find the best, valid alignment for each chromosome
        while (todoList.size() > 1) {
            // Currently targeted chromosome
            WormChromosome targetChrom = todoList.get(0);
            // Current best score, its associated chromosome, and its alignment (minimum score set above)
            NeedlemanWunschAlignment alignment = null;
            int currentScore = MIN_SCORE;
            int currentIndex = 0;
            WormChromosome current_chrom;
            // For each chromosome not tested
            for (int i = 1; i < todoList.size(); i++) {
                // Build the alignment for the chromosome
                current_chrom = todoList.get(i);
                NeedlemanWunschAlignment tempAlignment =
                        new NeedlemanWunschAlignment(targetChrom, current_chrom);
                // If the new alignment is better than the previous, update the values
                if (tempAlignment.getScore() > currentScore) {
                    alignment = tempAlignment;
                    currentScore = alignment.getScore();
                    currentIndex = i;
                }
            }
            // If this chromosome formed a valid alignment, save it for later use
            if (alignment != null) {
                this.alignments.add(alignment);
                todoList.remove(currentIndex);
            }
            // Otherwise, add the chromosome to the orphan list
            else {
                this.orphans.add(targetChrom);
            }
            // Remove the chromosome from the queue
            todoList.remove(0);
        }
        // Add the last chromosome left in the list, if applicable
        if (!todoList.isEmpty()) {
            this.orphans.add(todoList.get(0));
        }
    }

    // -- Data Reporting -- //
    public List<String> getChromosomeStrings() {
        if (this.chromosomes != null) {
            ArrayList<String> retList = new ArrayList<>(this.chromosomes.size());
            for (WormChromosome chrom : this.chromosomes) {
                retList.add(chrom.toString());
            }
            return retList;
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public String toString() {
        return "WormGenome{" +
                "chromosomes=" + chromosomes +
                ", alignments=" + alignments +
                ", orphans=" + orphans +
                '}';
    }
}
