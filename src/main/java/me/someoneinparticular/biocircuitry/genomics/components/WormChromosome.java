/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.genomics.components;

import net.minecraft.nbt.ByteArrayTag;

import java.util.ArrayList;
import java.util.List;

/**
 * A worm Chromosome, representing a sequence of NucleicBases
 * <br>
 * Mediates the identification of genes and the cross-over process between it
 * and another WormChromosome, as well as how its saved and read from
 * NBT format.
 */
public class WormChromosome {
    // The sequence representing this chromosome
    private ArrayList<NucleicBase> sequence;

    // Default, blank chromosome creator
    public WormChromosome() {
        this.sequence = new ArrayList<>();
    }

    // Chromosome with an explicitly set sequence
    public WormChromosome(List<NucleicBase> sequence) {
        this.sequence = new ArrayList<>(sequence);
    }

    // Ditto above, but from a String for easier creation
    public WormChromosome(String sequence) {
        this.sequence = NucleicBase.getSequenceFromString(sequence);
    }

    /**
     * Generate the NBT tag representing this Chromosome
     * @return The NBT tag representing this Chromosome's sequence
     */
    public ByteArrayTag toNBT() {
        byte[] byte_arr = new byte[this.sequence.size()];
        for (int i = 0; i < this.sequence.size(); i++) {
            byte_arr[i] = (byte) this.sequence.get(i).ordinal();
        }
        return new ByteArrayTag(byte_arr);
    }

    /**
     * Rebuild this chromosome from an NBT tag
     * @param nbt The NBT tag representing this Chromosome's sequence
     */
    public void fromNBT(ByteArrayTag nbt) {
        // Fetch the data from the NBT tag
        byte[] byte_arr = nbt.getByteArray();
        // Reset this Chromosome's sequence
        this.sequence = new ArrayList<>(byte_arr.length);
        // Interpret the NBT tag
        for (byte b : byte_arr) {
            NucleicBase base = NucleicBase.values()[b];
            this.sequence.add(base);
        }
    }

    /**
     * Clone this chromosome
     * @return The newly cloned Chromosome
     */
    public WormChromosome duplicate() {
        WormChromosome clone = new WormChromosome();
        clone.sequence = new ArrayList<>(this.sequence);
        return clone;
    }

    // -- Getters/Setters -- //
    public ArrayList<NucleicBase> getSequence() {
        return this.sequence;
    }

    // -- Built-in Overrides -- //
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (NucleicBase base : this.sequence) {
            str.append(base.toString());
        }
        return str.toString();
    }
}
