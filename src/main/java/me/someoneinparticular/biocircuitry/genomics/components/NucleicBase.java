/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.genomics.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum NucleicBase {
    A, T, C, G;

    /**
     * Generate a random nucleic acid sequence
     * @param length The length the sequence should be
     * @param random The {@link Random} instance that should be used to generate the sequence
     * @return The sequence of nucleic acids of the desired length
     */
    public static ArrayList<NucleicBase> randomSequence(int length, Random random) {
        ArrayList<NucleicBase> sequence = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            sequence.add(NucleicBase.values()[random.nextInt(4)]);
        }
        return sequence;
    }

    /**
     * Randomly mutate a sequence. Currently only supports SNP mutations
     * @param sequence The original "wild-type" sequence
     * @param mutationRate The mutations rate (in mutations per nucleic acid position)
     * @param rand The {@link Random} instance that should be used to mutate the sequence
     * @return The now-mutated sequence
     */
    public static ArrayList<NucleicBase> mutateSequence(List<NucleicBase> sequence, double mutationRate, Random rand) {
        ArrayList<NucleicBase> mutated = new ArrayList<>(sequence);
        int changes = (int) (mutated.size() * mutationRate);
        for (int i = 0; i < changes; i++) {
            int newVal = rand.nextInt(4);
            int newPos = rand.nextInt(sequence.size());
            mutated.set(newPos, NucleicBase.values()[newVal]);
        }
        return mutated;
    }

    /**
     * Convert the nucleic sequence string into raw text
     * @param seq The sequence to convert into string form
     * @return The string form of the sequence
     */
    public static String getSequenceString(ArrayList<NucleicBase> seq) {
        StringBuilder seq_string = new StringBuilder();
        for (NucleicBase b : seq) {
            seq_string.append(b.name());
        }
        return seq_string.toString();
    }

    /**
     * Convert a raw text string into a nucleic acid sequence
     * @param string The string to convert into sequence form
     * @return The sequence corresponding to the input string
     */
    public static ArrayList<NucleicBase> getSequenceFromString(String string) {
        String str = string.toUpperCase();
        ArrayList<NucleicBase> list = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            String c = str.substring(i, i+1);
            list.add(NucleicBase.valueOf(c));
        }
        return list;
    }
}
