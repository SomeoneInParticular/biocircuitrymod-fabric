/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.genomics.components;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import com.mojang.datafixers.util.Pair;
import org.ojalgo.random.Gamma;

import java.util.*;

/**
 * Alignment generated via the Needleman-Wunsch algorithm.
 * Very slow with large sequences, but extremely accurate.
 * Thankfully worm genomes are small, so this won't be an issue.
 */
// TODO: Find a way to save this to NBT in a compact form
public class NeedlemanWunschAlignment {
    // Tracked Chromosomes
    private final WormChromosome chromA;
    private final WormChromosome chromB;

    // Default Scoring Matrix
    // TODO Make this a configurable set of values
    private final static int MATCH = 2;
    private final static int MISMATCH = -2;
    private final static int INDEL = -3;

    // Score for the current alignment of this pair of Chromosomes
    private int score = 0;

    // Preserved alignment schema values
    private ArrayList<Pair<Integer, Integer>> alignmentSchema;

    // -- Constructors -- //
    // Generic constructor for generating the alignment between two Chromosomes
    public NeedlemanWunschAlignment(WormChromosome a, WormChromosome b) {
        this.chromA = a;
        this.chromB = b;
        this.align();
    }

    // -- Getters/Setters -- //
    // Not used currently, but kept around for debugging purposes
    @SuppressWarnings("unused")
    public Collection<WormChromosome> getTrackedChromosomes() {
        ArrayList<WormChromosome> list = new ArrayList<>(2);
        list.add(this.chromA);
        list.add(this.chromB);
        return list;
    }

    public int getScore() {
        return this.score;
    }

    // -- Internal Methods -- //
    /**
     * Build a score matrix for the two chromosomes this alignment corresponds too
     * @return The score matrix build (a 2d integer array)
     */
    private int[][] buildScoreMatrix() {
        // Initialization
        ArrayList<NucleicBase> seqA = chromA.getSequence();
        ArrayList<NucleicBase> seqB = chromB.getSequence();
        int aSize = seqA.size();
        int bSize = seqB.size();
        int[][] val_matrix = new int[aSize+1][bSize+1];
        // Initial matrix edge value setup
        for (int a = 0; a <= aSize; a++) {
            val_matrix[a][0] = a*INDEL;
        }
        for (int b = 0; b <= bSize; b++) {
            val_matrix[0][b] = (b*INDEL);
        }
        // Matrix scoring
        for (int a = 1; a <= aSize; a++) {
            for (int b = 1; b <= bSize; b++) {
                int aShift = val_matrix[a-1][b] + INDEL;
                int bShift = val_matrix[a][b-1] + INDEL;
                int ab_shift;
                NucleicBase baseA = seqA.get(a-1);
                NucleicBase baseB = seqB.get(b-1);
                if (baseA.equals(baseB)) {
                    ab_shift = val_matrix[a-1][b-1] + MATCH;
                } else {
                    ab_shift = val_matrix[a-1][b-1] + MISMATCH;
                }
                val_matrix[a][b] = Math.max(ab_shift, Math.max(aShift, bShift));
            }
        }
        // Return the matrix for further processing
        return val_matrix;
    }

    /**
     * Take a score matrix and generate our alignment (match-tracking) schema
     * @param scoreMatrix The score matrix guiding the schema construction
     */
    private void buildSchema(int[][] scoreMatrix) {
        // Setup
        int a = scoreMatrix.length - 1;
        int b = scoreMatrix[0].length - 1;
        this.alignmentSchema = new ArrayList<>();
        // Schema construction
        while (a > 0 && b > 0) {
            // If the score indicates a shift along sequence A
            if (scoreMatrix[a][b] == scoreMatrix[a-1][b] + INDEL) {
                a--;
                // If the score indicates a shift along sequence B
            } else if (scoreMatrix[a][b] == scoreMatrix[a][b-1] + INDEL) {
                b--;
            } // If the score indicates a match or mismatch
            else {
                if (scoreMatrix[a][b] == scoreMatrix[a-1][b-1] + MATCH) {
                    alignmentSchema.add(new Pair<>(a, b));
                }
                a--;
                b--;
            }
        }
        // Reverse the schema, as it is built from the tail up
        Collections.reverse(this.alignmentSchema);
    }

    /**
     * Generate an alignment, given two chromosomes, updating the schema and score
     * Simply a shortcut for a common chain of functions defined above
     */
    private void align() {
        // Build the alignment schema
        int[][] scoreMatrix = buildScoreMatrix();
        // Save the alignment score for later use
        this.score = scoreMatrix[scoreMatrix.length-1][scoreMatrix[0].length-1];
        // Build the alignment schema for use within the program
        this.buildSchema(scoreMatrix);
    }

    /**
     * Generates a random recombinant chromosome.
     * Uses a simplified version of Voorrips and Maliepaard's meiosis simulation
     * (2012) using double bivalent crossover mechanics (our worms aren't
     * tetradic). Their function, in turn, uses Kosambi's mapping function,
     * which we use as well.
     * @return The resulting recombinant chromosome
     */
    public WormChromosome makeRecombinant() {
        // Set up the gamma, with the mean crossover position being centered along the alignment
        double shape = 2.63;
        double rate = 2.63 / alignmentSchema.size();
        Gamma distribution = new Gamma(shape, rate);
        // Initialization
        Random rand = BioCircuitry.RANDOM;
        boolean save_first = rand.nextBoolean();
        ArrayDeque<Integer> cross_points = new ArrayDeque<>();
        double nextDelta;
        // Initial 'virtual' cross-over, to avoid "late sequence" preference
        int currentPos = -alignmentSchema.size()*3 + (int) (Math.log(1-rand.nextDouble()) * -(alignmentSchema.size()));
        // Further 'virtual' generation until we reach a valid cross-over position
        while (currentPos < 0) {
            currentPos += distribution.intValue();
        }
        // Generate crossover points
        while (currentPos < alignmentSchema.size()) {
            // Add the cross-over point to our list
            cross_points.add(currentPos);
            // Calculate the next potential cross-over point
            nextDelta = distribution.intValue();
            // Special case; if the sequence would roll the same position, return an unaltered chromosome
            if (nextDelta < 1) {
                return BioCircuitry.RANDOM.nextBoolean() ? this.chromA : this.chromB;
            }
            // Otherwise, update the cross-over point
            else {
                currentPos += nextDelta;
            }
        }
        return this.generateRecombinant(cross_points, save_first);
    }

    /**
     * Generate a new recombinant chromosome from the two chromosomes tracked by
     * the alignment. Allows for debugging/testing with explicit crossover
     * positions (plus potential API hooks, if desired)
     * @param crossPoints Matching position indices to cross-over at
     * @param saveFirst Whether the chromosome starting at chromA or chromB should be saved
     * @return The resulting recombinant from the crossovers (if any)
     */
    private WormChromosome generateRecombinant(Queue<Integer> crossPoints, boolean saveFirst) {
        // Initialization
        ArrayList<NucleicBase> seqA = this.chromA.getSequence();
        ArrayList<NucleicBase> seqB = this.chromB.getSequence();
        int current_pos = 0;
        ArrayList<NucleicBase> newSeq = new ArrayList<>();
        // Loop variables
        Pair<Integer, Integer> matchIndices;
        // Sequence construction
        while (crossPoints.peek() != null) {
            // Fetch the most recent crossover region
            int currentCrossPoint = crossPoints.remove();
            matchIndices = this.alignmentSchema.get(currentCrossPoint);
            // If we're saving the first Chromosome's sequence (ChromA)...
            if (saveFirst) {
                // Add ChromA's subsequence between the last crossover and this crossover point
                newSeq.addAll(new ArrayList<>(seqA.subList(current_pos, matchIndices.getFirst())));
                // Prepare for the next crossover, setting the initial position on ChromB
                current_pos = matchIndices.getSecond();
                // Set ChromB to be saved next
                saveFirst = false;
            }
            // Otherwise, we're saving the second Chromosome's sequence (ChromB)
            else {
                // Add ChromB's subsequence between the last crossover and this crossover point
                newSeq.addAll(new ArrayList<>(seqB.subList(current_pos, matchIndices.getFirst())));
                // Prepare for the next crossover, setting the initial position on ChromA
                current_pos = matchIndices.getSecond();
                // Set ChromA to be saved next
                saveFirst = true;
            }
        }
        // Fill in the final sequence of the chromosome, after the last crossover
        if (saveFirst) {
            newSeq.addAll(new ArrayList<>(seqA.subList(current_pos, seqA.size())));
        } else {
            newSeq.addAll(new ArrayList<>(seqB.subList(current_pos, seqB.size())));
        }
        // Return the new sequence
        return new WormChromosome(newSeq);
    }

    /**
     * Simple helper function which provides a string representation of the
     * recorded alignments
     * @param crossPoints The cross-over points for the chromosome
     * @return The string representation of this alignment
     */
    public String generateAlignmentStrings(List<Integer> crossPoints) {
        ArrayList<NucleicBase> seqA = chromA.getSequence();
        ArrayList<NucleicBase> seqB = chromB.getSequence();
        int aPos = 0;
        int bPos = 0;
        int crossIdx = -1;
        int crossPnt = -1;
        if (crossPoints != null && crossPoints.size() > 0) {
            crossPnt = crossPoints.get(0);
            crossIdx = 1;
        }
        StringBuilder builderA = new StringBuilder();
        StringBuilder builderB = new StringBuilder();
        StringBuilder builderC = new StringBuilder();
        for (int i = 0; i < alignmentSchema.size(); i++) {
            Pair<Integer, Integer> element = alignmentSchema.get(i);
            // Unpack the element into its components
            int aVal = element.getFirst();
            int bVal = element.getSecond();
            // Add the a->b insertions (b->a deletions)
            while (aPos < aVal-1) {
                builderA.append(seqA.get(aPos).toString());
                builderB.append("-");
                builderC.append("~");
                aPos++;
            }
            while (bPos < bVal-1) {
                builderB.append(seqB.get(bPos).toString());
                builderA.append("-");
                builderC.append("~");
                bPos++;
            }
            builderA.append(seqA.get(aPos).toString());
            aPos++;
            builderB.append(seqB.get(bPos).toString());
            bPos++;
            // If a crossover list was provided indicating this position, mark it
            if (crossPnt == i) {
                builderC.append("X");
                if (crossIdx != crossPoints.size()) {
                    crossPnt = crossPoints.get(crossIdx);
                    crossIdx++;
                }
            } else {
                builderC.append("~");
            }
        }
        String strA = builderA.toString();
        String strB = builderB.toString();
        String strC = builderC.toString();
        return strA + "\n" + strC + "\n" +  strB;
    }

    @Override
    public String toString() {
        return generateAlignmentStrings(null) + "\nscore: " + score;
    }
}
