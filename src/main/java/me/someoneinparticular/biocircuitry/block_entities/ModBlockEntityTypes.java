/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.block_entities;

import me.someoneinparticular.biocircuitry.blocks.ModBlocks;
import me.someoneinparticular.biocircuitry.lib.BioCircBlockEntityType;

public class ModBlockEntityTypes {
    public static final BioCircBlockEntityType<BroodBinEntity> BROOD_BIN_TYPE =
            new BioCircBlockEntityType<>(BroodBinEntity::new, null, "brood_bin", ModBlocks.BROOD_BIN);
    public static final BioCircBlockEntityType<FeedBinEntity> FEED_BIN_TYPE =
            new BioCircBlockEntityType<>(FeedBinEntity::new, null, "feed_bin", ModBlocks.FEED_BIN);

    public static void registerEntityTypes() {
        BROOD_BIN_TYPE.register();
        FEED_BIN_TYPE.register();
    }
}
