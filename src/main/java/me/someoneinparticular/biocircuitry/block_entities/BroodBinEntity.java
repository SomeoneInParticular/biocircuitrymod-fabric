/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.block_entities;

import me.someoneinparticular.biocircuitry.genomics.WormGenome;
import me.someoneinparticular.biocircuitry.gui.descriptions.BroodBinGuiDescription;
import me.someoneinparticular.biocircuitry.items.ModItems;
import me.someoneinparticular.biocircuitry.lib.inventory.WhitelistInventory;
import me.someoneinparticular.biocircuitry.proteomics.WormProteome;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;

public class BroodBinEntity extends LootableContainerBlockEntity implements ExtendedScreenHandlerFactory, Tickable {

    private static final TranslatableText CONTAINER_NAME = new TranslatableText("container.brood_bin");

    // Sub-inventories
    private final WhitelistInventory feedInv;
    private final WhitelistInventory maternalWormInv;
    private final WhitelistInventory paternalWormInv;
    private final WhitelistInventory babyWormInv;
    private final WhitelistInventory sequencerInv;

    // Breeding progress
    private int progress = 0;
    public static final int MAX_PROGRESS = 30;

    public BroodBinEntity() {
        super(ModBlockEntityTypes.BROOD_BIN_TYPE);
        this.feedInv = new WhitelistInventory(1, Items.SUGAR) {
            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                BroodBinEntity.this.markDirty();
            }
        };
        this.maternalWormInv = new WhitelistInventory(1, ModItems.WORM) {
            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                BroodBinEntity.this.markDirty();
            }
        };
        this.paternalWormInv = new WhitelistInventory(1, ModItems.WORM) {
            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                BroodBinEntity.this.markDirty();
            }
        };
        // No whitelist = take only
        this.babyWormInv = new WhitelistInventory(1) {
            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                BroodBinEntity.this.markDirty();
            }
        };
        this.sequencerInv = new WhitelistInventory(1, ModItems.SEQUENCER) {
            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                BroodBinEntity.this.markDirty();
            }
        };
    }

    public WhitelistInventory getFeedInv() {
        return this.feedInv;
    }

    public WhitelistInventory getMaternalInv() {
        return this.maternalWormInv;
    }

    public WhitelistInventory getPaternalInv() {
        return this.paternalWormInv;
    }

    public WhitelistInventory getBabyInv() {
        return this.babyWormInv;
    }

    public WhitelistInventory getSequencerInv() {
        return this.sequencerInv;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    /**
     * Attempt to generate a new worm from the worms contained within, if they
     * exist and have food.
     * @return If a new worm is born (currently unused, but kept for API purposes)
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean attemptWormSpawn() {
        // Only the server should attempt to spawn worms; otherwise phantom worms could appear
        if (this.world != null && this.world.isClient()) {
            return false;
        }
        // Confirm two parents are present alongside their food
        if (!feedInv.isEmpty() && !maternalWormInv.isEmpty() && !paternalWormInv.isEmpty()) {
            // Make sure a baby isn't taking up the output slot already
            if (babyWormInv.isEmpty()) {
                // Rebuild the genomes
                WormGenome maternalGenome = new WormGenome(maternalWormInv.getStack(0));
                WormGenome paternalGenome = new WormGenome(paternalWormInv.getStack(0));
                // Determine the gametes associated with these genomes
                WormGenome maternalGamete = maternalGenome.buildGamete();
                WormGenome paternalGamete = paternalGenome.buildGamete();
                // Initialize a new baby worm ItemStack
                ItemStack babyWorm = new ItemStack(ModItems.WORM);
                // Build a new genome from the gametes
                WormGenome babyGenome = new WormGenome(maternalGamete, paternalGamete);
                // Build the proteome from the genome
                WormProteome babyProteome = new WormProteome(babyGenome);
                // Sequence the baby if a sequencer has been provided
                if (!sequencerInv.isEmpty()) {
                    ItemStack seqStack = sequencerInv.getStack(0);
                    babyProteome.applyColorMap(seqStack);
                    babyProteome.markSequenced(babyWorm.getOrCreateTag());
                }
                // Attach the data to a newborn worm
                babyGenome.toEntityNBT(babyWorm);
                babyProteome.toEntityNBT(babyWorm);
                // Place the baby in the output
                babyWormInv.setStack(0, babyWorm);
                // Have the parents eat
                feedInv.getStack(0).decrement(1);
                // Baby born, return success!
                return true;
            }
        }
        // No baby :(
        return false;
    }

    @Override
    protected Text getContainerName() {
        return CONTAINER_NAME;
    }

    @Override
    protected ScreenHandler createScreenHandler(int syncId, PlayerInventory playerInventory) {
        return new BroodBinGuiDescription(syncId, playerInventory, this);
    }

    @Override
    protected DefaultedList<ItemStack> getInvStackList() {
        DefaultedList<ItemStack> stackList = DefaultedList.of();
        stackList.addAll(feedInv.getFullInv());
        stackList.addAll(maternalWormInv.getFullInv());
        stackList.addAll(paternalWormInv.getFullInv());
        stackList.addAll(babyWormInv.getFullInv());
        stackList.addAll(sequencerInv.getFullInv());
        return stackList;
    }

    @Override
    protected void setInvStackList(DefaultedList<ItemStack> list) {
        // Assume that the person setting this stack puts stuff where they want to
        this.feedInv.setStack(0, list.get(0));
        this.maternalWormInv.setStack(0, list.get(1));
        this.paternalWormInv.setStack(0, list.get(2));
        this.babyWormInv.setStack(0, list.get(3));
    }

    @Override
    public int size() {
        return feedInv.size() + maternalWormInv.size() +
                paternalWormInv.size() + babyWormInv.size() +
                sequencerInv.size();
    }

    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        super.fromTag(state, tag);
        this.feedInv.fromTag("food", tag);
        this.maternalWormInv.fromTag("maternal", tag);
        this.paternalWormInv.fromTag("paternal", tag);
        this.babyWormInv.fromTag("baby", tag);
        this.sequencerInv.fromTag("sequencer", tag);
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        // Generate the initial tag
        CompoundTag targetTag = super.toTag(tag);
        // Save our components to the tag
        targetTag = feedInv.toTag("food", targetTag);
        targetTag = maternalWormInv.toTag("maternal", targetTag);
        targetTag = paternalWormInv.toTag("paternal", targetTag);
        targetTag = babyWormInv.toTag("baby", targetTag);
        targetTag = sequencerInv.toTag("sequencer", targetTag);
        // Return the now-updated tag
        return targetTag;
    }

    @Override
    public String toString() {
        return "BroodBinEntity{" +
                "feedInv=" + feedInv +
                ", maternalWormInv=" + maternalWormInv +
                ", paternalWormInv=" + paternalWormInv +
                ", babyWormInv=" + babyWormInv +
                '}';
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
        buf.writeBlockPos(pos);
    }

    @Override
    public void tick() {
        // Return if the entity should not tick (it exists in an invalid/non-existent world)
        if (world == null || world.isClient) return;
        // Otherwise, run the ticking process if the system is in a running state
        if (babyWormInv.isEmpty() &&
            !maternalWormInv.isEmpty() &&
            !paternalWormInv.isEmpty() &&
            !feedInv.isEmpty()) {
            // If the progress has completed, spawn a new baby worm
            if (progress == MAX_PROGRESS) {
                attemptWormSpawn();
            }
            // Increment the progress
            progress++;
        } else {
            progress = 0;
        }
    }
}
