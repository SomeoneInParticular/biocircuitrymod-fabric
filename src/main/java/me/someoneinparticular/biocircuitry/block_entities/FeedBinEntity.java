/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.block_entities;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.gui.descriptions.FeedBinGuiDescription;
import me.someoneinparticular.biocircuitry.items.ModItems;
import me.someoneinparticular.biocircuitry.lib.inventory.GenericInventory;
import me.someoneinparticular.biocircuitry.lib.inventory.WhitelistInventory;
import me.someoneinparticular.biocircuitry.proteomics.WormPathway;
import me.someoneinparticular.biocircuitry.proteomics.WormProteome;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Pair;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;

import java.util.List;

public class FeedBinEntity extends LootableContainerBlockEntity implements Tickable, ExtendedScreenHandlerFactory {
    // Container name
    public static final TranslatableText CONTAINER_NAME = new TranslatableText("container.feed_bin");

    // Sub-inventories
    private final GenericInventory inputInv;
    private final WhitelistInventory wormInv;
    private final WhitelistInventory outputInv;

    // Properties
    private int cooldown = 0;
    private float progress = 0;
    private double perTick = 0;
    // The state of the block; positive = running, negative = disabled, 0 = querying
    private int state = 0;

    public static final byte MAX_COOLDOWN = 20; // 1 second @ standard speed
    public static final byte MAX_PROGRESS = 100; // 100 % = complete

    // Worm Pathway being managed
    private WormPathway pathway = null;

    public FeedBinEntity() {
        // Initialize basic block-entity requirements
        super(ModBlockEntityTypes.FEED_BIN_TYPE);
        // Inventories
        this.inputInv = new GenericInventory(4) {
            @Override
            public void markDirty() {
                super.markDirty();
                FeedBinEntity.this.markDirty();
            }

            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                FeedBinEntity.this.checkPathway();
            }
        };
        this.wormInv = new WhitelistInventory(1, ModItems.WORM) {
            @Override
            public void markDirty() {
                super.markDirty();
                FeedBinEntity.this.markDirty();
            }

            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                FeedBinEntity.this.checkPathway();
            }
        };
        this.outputInv = new WhitelistInventory(4) {
            @Override
            public void markDirty() {
                super.markDirty();
                FeedBinEntity.this.markDirty();
            }

            @Override
            public void setStack(int slot, ItemStack stack) {
                super.setStack(slot, stack);
                FeedBinEntity.this.checkPathway();
            }
        };
    }

    // -- Inventory getters -- //
    public GenericInventory getInputInv() {
        return inputInv;
    }

    public WhitelistInventory getWormInv() {
        return wormInv;
    }

    public WhitelistInventory getOutputInv() {
        return outputInv;
    }

    // -- Property getters/setters -- //
    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    // -- Overrides -- //
    @Override
    protected Text getContainerName() {
        return CONTAINER_NAME;
    }

    @Override
    protected ScreenHandler createScreenHandler(int i, PlayerInventory playerInventory) {
        return new FeedBinGuiDescription(i, playerInventory, this);
    }

    @Override
    protected DefaultedList<ItemStack> getInvStackList() {
        DefaultedList<ItemStack> stackList = DefaultedList.of();
        stackList.addAll(inputInv.getFullInv());
        stackList.addAll(wormInv.getFullInv());
        stackList.addAll(outputInv.getFullInv());
        return stackList;
    }

    @Override
    protected void setInvStackList(DefaultedList<ItemStack> list) {
        int currentIdx = 0;
        // Assume the person setting this list knows what they're doing
        for (int i = 0; i < this.inputInv.size(); i++) {
            this.inputInv.setStack(i, list.get(currentIdx));
            currentIdx++;
        }
        this.wormInv.setStack(0, list.get(currentIdx++));
        for (int i = 0; i < this.outputInv.size(); i++) {
            this.inputInv.setStack(i, list.get(currentIdx));
            currentIdx++;
        }
    }

    @Override
    public int size() {
        return this.inputInv.size() +
                this.wormInv.size() +
                this.outputInv.size();
    }

    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        super.fromTag(state, tag);

        // Inventories and Complex Structures
        this.inputInv.fromTag("input", tag);
        this.wormInv.fromTag("worm", tag);
        this.outputInv.fromTag("output", tag);
        this.pathway = new WormPathway(tag);
        if (this.pathway.getCosts() == null) this.pathway = null;

        // Properties
        this.cooldown = tag.getInt("cooldown");
        this.progress = tag.getFloat("progress");
        this.perTick = tag.getFloat("per_tick");
        this.state = tag.getInt("state");
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        CompoundTag targetTag = super.toTag(tag);

        // Inventories and Complex Structures
        this.inputInv.toTag("input", targetTag);
        this.wormInv.toTag("worm", targetTag);
        this.outputInv.toTag("output", targetTag);
        if (pathway != null) {
            this.pathway.toNBT(targetTag);
        }

        // Properties
        tag.putInt("cooldown", this.cooldown);
        tag.putFloat("progress", this.progress);
        tag.putDouble("per_tick", this.perTick);
        tag.putInt("state", this.state);

        return targetTag;
    }

    @Override
    public void tick() {
        // Only run ticks on the server side
        if (this.world == null || this.world.isClient()) return;
        // Cooldown for pathway reconstruction, if a change has been observed
        if (this.state == 0) {
            // If no possible pathway could exist, simply reset everything
            if (!readyForPathway()) {
                this.disable();
            }
            // If the cooldown hasn't passed, continue its progress
            else if (this.cooldown < MAX_COOLDOWN) {
                this.cooldown++;
            }
            // Finally, if neither of the above occurred, try and rebuild the pathway
            else {
                WormProteome proteome = new WormProteome(this.wormInv.getStack(0));
                List<ItemStack> foods = this.inputInv.getFullInv();
                WormPathway pathway = new WormPathway(foods, proteome.getAllProteins());
                // If the pathway is invalid/useless, disable this until something changes
                if (pathway.getCosts() == null || pathway.getCosts().isEmpty()) {
                    this.disable();
                }
                // Otherwise save the resulting pathway
                else {
                    this.pathway = pathway;
                    this.perTick = pathway.getMotility();
                    this.state = 1;
                    this.checkPathway();
                }
                // Set the cooldown back to 0, indicating a completed check
                this.cooldown = 0;
            }
        }
        // Progress the feeding cycle if a pathway exist and we have room for it
        else if (this.pathway != null && this.state > 0) {
            // If the progress cycle is ongoing, increment its progress
            if (this.progress < MAX_PROGRESS) {
                this.progress += this.perTick;
            }
            // Barring that, try to run the pathway's cost cycle
            else {
                // Create the products into the output list
                if (pathway.getProducts() != null) {
                    this.placeInOutput(pathway.getProducts(), false);
                }
                // Take the costs required
                if (this.takeFromInput(pathway.getCosts(), false)) {
                    // If the costs somehow failed, end here and mark to be rebuild
                    this.progress = 0;
                    this.cooldown = 1;
                }
                // Mark this as dirty, needing to be saved
                this.markDirty();
                this.checkPathway();
                // Reset the progress
                this.progress = 0;
            }
        }
        // Otherwise do nothing until a one of the conditions are met
    }

    // Helper functions
    /**
     * Shortcut which forces this entity to enter the pathway-checking state
     */
    protected void markPathwayInvalid() {
        this.cooldown = 1;
        this.state = 0;
    }

    /**
     * Shortcut which forces the entity to enter a disabled, minimal-memory state
     */
    protected void disable() {
        this.cooldown = 0;
        this.progress = 0;
        this.perTick = 0;
        this.pathway = null;
        this.state = -2;
    }

    /**
     * Shortcut function which checks if the entity can support a worm pathway
     * @return Whether a worm pathway can be supported in the current state
     */
    protected boolean readyForPathway() {
        return (!this.inputInv.isEmpty() && !this.wormInv.isEmpty());
    }

    /**
     * Take (or simulate the taking) of a list of cost items from our input
     * @param costs Costs to take
     * @param simulate Whether this action should be simulated or done outright
     * @return Whether the actions was successful (all costs accounted for)
     */
    protected boolean takeFromInput(List<Pair<Item, Integer>> costs, boolean simulate) {
        // For each item we need to take the cost from
        for (Pair<Item, Integer> cost : costs) {
            // Track whether the cost has been paid
            int amount = cost.getRight();
            // Iterate through the slots and try to take wherever possible
            for (int i = 0; i < this.inputInv.size(); i++) {
                // Get the stack potentially being reduced
                ItemStack inputStack = this.inputInv.getStack(i);
                // If they are the same type as required, try to take the cost from it
                if (cost.getLeft() == inputStack.getItem()) {
                    int available = inputStack.getCount();
                    // If the amount available to take is enough to satisfy the cost, finish here
                    if (amount <= available) {
                        if (!simulate) {
                            inputStack.decrement(amount);
                            if (inputStack.getCount() == 0) this.inputInv.setStack(i, ItemStack.EMPTY);
                        }
                        amount = 0;
                        break;
                    }
                    // Otherwise, take what we can
                    else {
                        amount -= inputStack.getCount();
                        if (!simulate) {
                            this.inputInv.setStack(i, ItemStack.EMPTY);
                        }
                    }
                }
                // Edge case; amount left = 0 but the input stack = 0 too
                if (amount == 0) {
                    break;
                }
            }
            // If we were unable to completely satisfy the cost, finish and report this
            if (amount > 0) {
                return false;
            }
        }
        // If we didn't return previously, just finish here
        return true;
    }

    /**
     * Produce (or simulate the production of) a list of product items in our output
     * @param products Products to produce
     * @param simulate Whether this action should be simulated or done outright
     * @return Whether the actions was successful (all products placed)
     */
    protected boolean placeInOutput(List<Pair<Item, Float>> products, boolean simulate) {
        // For each item we need to insert
        for (Pair<Item, Float> prod : products) {
            // Track whats left to place in the output
            int amount;
            // If its a simulation, assuming the most is produced
            if (simulate) {
                amount = (int) Math.ceil(prod.getRight());
            }
            // Otherwise, roll for the chances of the last item being created
            else {
                amount = (int) Math.floor(prod.getRight());
                float chance = prod.getRight() - amount;
                if (Math.random() < chance) {
                    ++amount;
                }
            }
            // Iterate through the slots and try to take wherever possible
            for (int i = 0; i < this.outputInv.size(); i++) {
                // Get the stack potentially being reduced
                ItemStack outputStack = this.outputInv.getStack(i);
                // If the stack is empty, just place what remains here and finish
                if (outputStack.isEmpty()) {
                    if (!simulate) {
                        this.outputInv.setStack(i, new ItemStack(prod.getLeft(), amount));
                    }
                    amount = 0;
                    break;
                }
                // If the stacks match item types, place what you can here
                if (outputStack.getItem() == prod.getLeft()) {
                    int spareRoom = outputStack.getMaxCount() - outputStack.getCount();
                    // If we can place what remains here, do so and finish
                    if (spareRoom >= amount) {
                        if (!simulate) {
                            outputStack.increment(amount);
                        }
                        amount = 0;
                        break;
                    }
                    // Otherwise, place what we can and continue
                    else {
                        if (!simulate) {
                            outputStack.increment(spareRoom);
                        }
                        amount -= spareRoom;
                    }
                }
                // Edge case; amount left = 0 but the output stack also had 0 room
                if (amount == 0) {
                    break;
                }
            }
            // If the full amount could not be placed
            if (amount > 0) {
                BioCircuitry.LOGGER.error("FAILED TO PLACE " + prod.getLeft().getTranslationKey());
                BioCircuitry.LOGGER.error("AMOUNT: " + prod.getRight());
                return false;
            }
        }
        return true;
    }

    /**
     * Force a validation of the current pathway, potentially marking the entity
     * to check for a better one if it is not valid
     */
    public void checkPathway() {
        // Only run these checks server-side
        if (this.world == null || this.world.isClient) {
            return;
        }

        // If we lack a worm or any food, just disable this until later
        if (this.wormInv.isEmpty() || this.inputInv.isEmpty()) {
            this.disable();
        }

        // Null pathways are always invalid when we have a worm and food;
        // we should try to build a new one
        else if (this.pathway == null) {
            this.markPathwayInvalid();
        }

        // If our foods have just changed, we should try and build a new pathway
        else if (!this.takeFromInput(this.pathway.getCosts(), true)) {
            this.markPathwayInvalid();
        }

        // Finally, see if we just have a backlog products that need to be cleared
        else if (this.pathway.getProducts() != null) {
            boolean b = !this.placeInOutput(this.pathway.getProducts(), true);
            if (b) this.state = -1;
        }
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
        buf.writeBlockPos(pos);
    }
}
