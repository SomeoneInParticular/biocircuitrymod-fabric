/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry;

import me.someoneinparticular.biocircuitry.gui.ModScreens;
import me.someoneinparticular.biocircuitry.networking.ModNetworking;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;


@Environment(EnvType.CLIENT)
public class BioCircClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        BioCircuitry.LOGGER.info("Initializing Client Side components");
        // Initialize GUI (screen) instances required for the player to interact
        ModScreens.initialize();
        // Initialize the networking components required client-side
        ModNetworking.initClient();
    }
}
