/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.screens;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import me.someoneinparticular.biocircuitry.gui.descriptions.ChromatographGuiDescription;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;

public class ChromatographScreen extends CottonInventoryScreen<ChromatographGuiDescription> {

    public ChromatographScreen(ChromatographGuiDescription gui, PlayerInventory inv, Text name) {
        super(gui, inv.player, name);
    }
}
