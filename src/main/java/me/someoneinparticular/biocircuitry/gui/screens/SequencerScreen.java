/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.screens;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import io.github.cottonmc.cotton.gui.widget.WPanel;
import me.someoneinparticular.biocircuitry.gui.descriptions.SequencerGuiDescription;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;

public class SequencerScreen extends CottonInventoryScreen<SequencerGuiDescription> {
    @SuppressWarnings("unused")
    public SequencerScreen(SequencerGuiDescription gui, PlayerInventory inv, Text title) {
        super(gui, inv.player);
        gui.runOnSetRoot(this::rescale);
    }

    @Override
    protected void reposition(int screenWidth, int screenHeight) {
        this.rescale();
    }

    /**
     * A more generic version of "reposition", which also conveniently makes
     * it a "Runnable" function for easy use elsewhere
     */
    protected void rescale() {
        WPanel basePanel = description.getRootPanel();
        boolean isNull = (basePanel == null);
        if (!isNull) {
            this.description.slots.clear();
            basePanel.validate(description);

            backgroundWidth = basePanel.getWidth();
            backgroundHeight = basePanel.getHeight();

            //DEBUG
            if (backgroundWidth<16) backgroundWidth=300;
            if (backgroundHeight<16) backgroundHeight=300;
        }

        if (!description.isFullscreen()) {
            x = (width / 2) - (backgroundWidth / 2);
            y = (height / 2) - (backgroundHeight / 2);
            titleX = 0;
            titleY = 0;
        } else {
            x = 0;
            y = 0;

            // Offset the title coordinates a little from the edge
            titleX = 10;
            titleY = 10;
        }

        // Redraw the backgrounds, because LibGUI decided single-use functions
        // were the way to go for fuck knows why
        if (!isNull) {
            this.description.addPainters();
        }
    }
}
