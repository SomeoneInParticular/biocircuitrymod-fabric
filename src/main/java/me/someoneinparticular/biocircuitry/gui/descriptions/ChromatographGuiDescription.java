/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.descriptions;

import io.github.cottonmc.cotton.gui.networking.NetworkSide;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.WPlayerInvPanel;
import io.github.cottonmc.cotton.gui.widget.WText;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import io.github.cottonmc.cotton.gui.widget.data.VerticalAlignment;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormMetaboliteHandler;
import me.someoneinparticular.biocircuitry.lib.rendering.BackgroundRendering;
import me.someoneinparticular.biocircuitry.networking.ChromatographPacketManager;
import me.someoneinparticular.biocircuitry.proteomics.components.WormMetabolite;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

import java.util.Map;

import static me.someoneinparticular.biocircuitry.gui.ModScreenHandlers.CHROMATOGRAPH_SCREEN_HANDLER;

public class ChromatographGuiDescription extends PlayerInvTransferingGuiDescription {

    protected static final int INFO_WIDTH = 134;
    protected static final int INFO_HEIGHT = 60;
    protected static final int INFO_X_OFFSET = 24;
    protected static final int INFO_Y_OFFSET = 14;

    protected final static TranslatableText DEFAULT_TEXT = new TranslatableText(String.format("label.%s.no_item", BioCircuitry.MOD_ID));
    protected final static TranslatableText WAITING_TEXT = new TranslatableText(String.format("label.%s.awaiting_packet", BioCircuitry.MOD_ID));
    protected final static TranslatableText INVALID_TEXT = new TranslatableText(String.format("label.%s.invalid_item", BioCircuitry.MOD_ID));

    protected WText nutrientText;

    public ChromatographGuiDescription(int syncID, PlayerInventory playerInventory) {
        // Create the base GUI
        super(CHROMATOGRAPH_SCREEN_HANDLER, syncID, playerInventory);

        // Initialize the inventory this GUI will manage
        blockInventory = new SimpleInventory(1);

        // Initialize the root panel upon which everything else is build
        WPlainPanel rootPanel = new WPlainPanel();

        // Add the player inventory panel
        WPlayerInvPanel playerPanel = new WPlayerInvPanel(playerInventory);
        rootPanel.add(playerPanel, 0, INFO_HEIGHT+20);

        // Add the info text panel
        this.nutrientText = new WText(DEFAULT_TEXT);
        nutrientText.setHorizontalAlignment(HorizontalAlignment.CENTER);
        nutrientText.setVerticalAlignment(VerticalAlignment.CENTER);
        nutrientText.setColor(0xFFFFFF);

        WPlainPanel infoPanel = new WPlainPanel();
        // Background renderers only function on client-side
        if (getNetworkSide() == NetworkSide.CLIENT) {
            infoPanel.setBackgroundPainter(BackgroundRendering.RETRO_GREEN);
        }
        infoPanel.add(nutrientText, 0, 0, INFO_WIDTH, INFO_HEIGHT);

        rootPanel.add(infoPanel, INFO_X_OFFSET, INFO_Y_OFFSET, INFO_WIDTH, INFO_HEIGHT);

        // Add the slot that the analyzed item should be placed in
        WItemSlot itemSlot = new WItemSlot(blockInventory, 0, 1, 1, false);
        if (world.isClient) {
            itemSlot.addChangeListener((slot, inventory, index, stack) -> onSlotChanged());
        }
        rootPanel.add(itemSlot, 0, 10);

        // Set the root and validate that everything is correct
        setRootPanel(rootPanel);

        rootPanel.validate(this);
    }

    @Environment(EnvType.CLIENT)
    protected void onSlotChanged() {
        ItemStack stack = blockInventory.getStack(0);
        if (stack == null || stack.isEmpty()) {
            setInfoText(DEFAULT_TEXT);
            return;
        }
        Item item = stack.getItem();
        ChromatographPacketManager.sendFoodValueRequestC2SPacket(item, this.syncId);
        setInfoText(WAITING_TEXT);
    }

    @Environment(EnvType.CLIENT)
    protected void setInfoText(TranslatableText text) {
        nutrientText.setHorizontalAlignment(HorizontalAlignment.CENTER);
        nutrientText.setVerticalAlignment(VerticalAlignment.CENTER);
        nutrientText.setText(text);
    }

    @Environment(EnvType.CLIENT)
    public void receiveNutrientInfo(Map<Identifier, Float> nutrients) {
        // If the nutrients are empty, notify the user
        if (nutrients == null || nutrients.size() == 0) {
            setInfoText(INVALID_TEXT);
            return;
        }
        // Otherwise, present the results to the user
        nutrientText.setHorizontalAlignment(HorizontalAlignment.LEFT);
        nutrientText.setVerticalAlignment(VerticalAlignment.TOP);
        // Set the text to be aligned top-left
        nutrientText.setHorizontalAlignment(HorizontalAlignment.LEFT);
        nutrientText.setVerticalAlignment(VerticalAlignment.TOP);
        // Build up the full text required
        TranslatableText newText = new TranslatableText("");
        for (Map.Entry<Identifier, Float> mapping : nutrients.entrySet()) {
            newText.append(new LiteralText(String.format("%2.2f - ", mapping.getValue())));
            WormMetabolite metab = WormMetaboliteHandler.get(mapping.getKey());
            newText.append(metab.getTranslatableText());
            newText.append(new LiteralText("\n"));
        }
        nutrientText.setText(newText);
    }

    /**
     * Just does clean up stuff created for the GUI when the GUI is closed, to
     * prevent pseudo-"memory leaks". In this case, it deletes the inventories so
     * they don't eventually drag the server to a crawl, and drops any worm that
     * may have been left in the inventory by returning it too the player.
     */
    @Override
    public void close(PlayerEntity player) {
        // Give or drop all inventory items for the player
        this.dropInventory(player, world, blockInventory);
        // Close the inventory
        blockInventory.onClose(player);
        // Run anything else that needs to occur
        super.close(player);
    }
}
