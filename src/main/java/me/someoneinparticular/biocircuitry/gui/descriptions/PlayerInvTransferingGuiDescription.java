/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.descriptions;

import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;

/**
 * Bypasses LibGUIs disabled transferSlot behaviour to re-enable it.
 */
public abstract class PlayerInvTransferingGuiDescription extends SyncedGuiDescription {
    public PlayerInvTransferingGuiDescription(ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory) {
        super(type, syncId, playerInventory);
    }

    /**
     * Required Override because LibGUI made everything fucking private for god
     * knows why. They also just ignore the existing "transfer slot" function
     * which exists precisely to manage QUICK_MOVE and replaced it with their
     * own private function, again for fuck knows why.
     */
    @Override
    public ItemStack onSlotClick(int slotNumber, int button, SlotActionType action, PlayerEntity player) {
        if (action == SlotActionType.QUICK_MOVE) {
            return transferSlot(player, slotNumber);
        }
        return super.onSlotClick(slotNumber, button, action, player);
    }

    /**
     * Allows player "shift clicking" functionality
     * @param player The player doing the shift clicking
     * @param index The index of the slot being clicked
     * @return The ItemStack produced from the operation (landing in the cursor slot)
     */
    @Override
    public ItemStack transferSlot(PlayerEntity player, int index) {
        // Initialization
        ItemStack itemStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(index);
        // Confirm the slot is valid and has a stack to transfer
        if (slot != null && slot.hasStack()) {
            // Get the stack occupying the targeted index
            ItemStack itemStack2 = slot.getStack();
            itemStack = itemStack2.copy();
            // If the index indicates a player slot was chosen, try to transfer to the block inventory
            int playerInvSize = playerInventory.main.size();
            if (index < playerInvSize) {
                // Only main slots are accessible to the GUI; no offhand or armor
                if (!this.insertItem(itemStack2, playerInvSize, slots.size(), false)) {
                    return ItemStack.EMPTY;
                }
                // Otherwise, try to transfer from the block inventory to the player
            } else {
                // Only main slots are accessible to the GUI; no offhand or armor
                if (!this.insertItem(itemStack2, 0, playerInvSize, false)) {
                    return ItemStack.EMPTY;
                }
            }

            // If the stack, after being inserted, is empty, set the prior stack to nothing
            if (itemStack2.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            }
            // Otherwise mark that the slot changed so it is saved later
            else {
                slot.markDirty();
            }

            // If the now-placed ItemStack contains the entire original stack, notify that the original stack is completely empty
            if (itemStack2.getCount() == itemStack.getCount()) {
                return ItemStack.EMPTY;
            }

            slot.onTakeItem(player, itemStack2);
        }
        // Default to returning nothing
        return itemStack;
    }
}
