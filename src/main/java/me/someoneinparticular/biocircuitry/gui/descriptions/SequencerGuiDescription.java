/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.descriptions;

import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.ValidatedSlot;
import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.widget.*;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import io.github.cottonmc.cotton.gui.widget.data.VerticalAlignment;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormMetaboliteHandler;
import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.genomics.WormGenome;
import me.someoneinparticular.biocircuitry.gui.widgets.WRollingFrameSprite;
import me.someoneinparticular.biocircuitry.items.ModItems;
import me.someoneinparticular.biocircuitry.items.SequencerItem;
import me.someoneinparticular.biocircuitry.lib.inventory.SequencerInventory;
import me.someoneinparticular.biocircuitry.lib.rendering.BackgroundPainting;
import me.someoneinparticular.biocircuitry.lib.rendering.BackgroundRendering;
import me.someoneinparticular.biocircuitry.networking.SequencerPacketManager;
import me.someoneinparticular.biocircuitry.proteomics.WormProteome;
import me.someoneinparticular.biocircuitry.proteomics.components.AminoAcid;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.util.TriState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.*;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static me.someoneinparticular.biocircuitry.gui.ModScreenHandlers.SEQUENCER_SCREEN_HANDLER;

/**
 * The GUI handler for the Sequencer item, used to sequence worm genomes for
 * more in-depth analysis than would be possible via simply observing the
 * phenotypes of the worm.
 */
public class SequencerGuiDescription extends SyncedGuiDescription {
    // Synchronization map
    public static final HashMap<Integer, SequencerGuiDescription> activeInstances = new HashMap<>();

    // The slot containing the to-be-sequenced item, cached for convenient access
    private final WItemSlot loadedSlot;

    // A list of slots this inventory uses, some of which may be hidden
    protected final HashMap<Integer, ValidatedSlot> trackedSlots = new HashMap<>();

    // Screen-linked call to make when required
    private Runnable runOnRootChange = null;

    // Cached panels for later recycled use
    private BasePanel basePanel = null;
    private ConfigPanel configPanel = null;
    private ResultsPanel resultsPanel = null;

    // Useful universal constants
    private final int GENERIC_BUTTON_WIDTH = 100;

    /**
     * Constructor for the client-side, based on a ItemStack containing packet
     */
    public SequencerGuiDescription(int syncId, PlayerInventory playerInv, PacketByteBuf packet) {
        this(syncId, playerInv, packet.readItemStack());
    }

    /**
     * Generic constructor, which actually does all the work
     * @param syncId The Synchronization ID for this screen handler
     * @param playerInv The player's inventory
     * @param seqStack The ItemStack of the sequencer this GUI should be built around
     */
    public SequencerGuiDescription(int syncId, PlayerInventory playerInv, ItemStack seqStack) {
        // Run anything the GUI may need to run in advance
        super(SEQUENCER_SCREEN_HANDLER, syncId, playerInv);

        // If this a server-side instance, track it for networking usage later
        if (!world.isClient) {
            activeInstances.put(syncId, this);
        }

        // Track our inventory
        SequencerInventory seqInv = new SequencerInventory(seqStack);
        seqInv.onOpen(playerInv.player);
        this.blockInventory = seqInv;

        // Have the sequencer tell us to update the results panel info when it completes a cycle
        if (!world.isClient) {
            seqInv.addSequencingCompletionTask(() -> SequencerPacketManager.sendSequencingCompleteS2CPacket((ServerPlayerEntity) playerInv.player, blockInventory.getStack(SequencerInventory.WORM_IDX)));
        }

        // Create the ItemSlot widget that will manage the entity within the inventory
        loadedSlot = WItemSlot.of(seqInv, SequencerInventory.WORM_IDX);
        loadedSlot.setFilter(stack -> (stack.getItem() == ModItems.WORM));

        // Create the property delegate which will manage the inventories attributes
        propertyDelegate = new PropertyDelegate() {
            @Override
            public int get(int index) {
                if (index == 0) {
                    return seqInv.getProgress();
                }
                return 0;
            }

            @Override
            public void set(int index, int value) {
                if (index == 0) {
                    seqInv.setProgress(value);
                }
            }

            @Override
            public int size() {
                return 1;
            }
        };
        this.addProperties(propertyDelegate);

        // Start by rendering this GUI in its basic form
        buildAsBase();
    }

    public void runOnSetRoot(Runnable run) {
        runOnRootChange = run;
    }

    // Just a hook for external processes to tell the inventory to update itself
    public void updateProteinMappedColor(WormProtein prot, int color) {
        ((SequencerInventory) blockInventory).updateProteinColor(prot, color);
    }

    // Another hook, this time to attempt to update to the results panel
    @Environment(EnvType.CLIENT)
    public void updateResults(ItemStack stack) {
        // If the stack is null, empty, or not sequenced, terminate early
        if (stack == null || stack.isEmpty() || !WormProteome.isSequenced(stack.getTag())) {
            return;
        }
        // Create the results panel if it does not already exist
        if (resultsPanel == null) {
            resultsPanel = new ResultsPanel(stack);
        }
        // Otherwise, just have it update itself
        else {
            resultsPanel.setCachedStack(stack);
        }
    }

    protected WButton sequencingButton() {
        WButton seqButton = new WButton();
        seqButton.setLabel(new TranslatableText(String.format("button.%s.sequencing", BioCircuitry.MOD_ID)));
        seqButton.setOnClick(SequencerGuiDescription.this::buildAsBase);
        return seqButton;
    }

    protected WButton configButton() {
        WButton configButton = new WButton();
        configButton.setLabel(new TranslatableText(String.format("button.%s.config", BioCircuitry.MOD_ID)));
        configButton.setOnClick(SequencerGuiDescription.this::buildAsConfig);
        return configButton;
    }

    protected WButton resultsButton() {
        WButton resultsButton = new WButton() {
            @Override
            @Environment(EnvType.CLIENT)
            public void tick() {
                // Do whatever else this might need to do
                super.tick();
                // You can only go to the results panel when it has been created
                this.setEnabled(resultsPanel != null);
            }
        };
        resultsButton.setOnClick(SequencerGuiDescription.this::buildAsResults);
        resultsButton.setLabel(new TranslatableText(String.format("button.%s.results", BioCircuitry.MOD_ID)));
        return resultsButton;
    }

    /**
     * Set the root panel to a new value, now with the ability to call a function
     * (usually provided the corresponding SequencerScreen) when this occurs.
     */
    @Override
    public SyncedGuiDescription setRootPanel(WPanel newRoot) {
        // Add (with centering) the new root panel we want to build everything up on
        super.setRootPanel(newRoot);

        // Clear focus to avoid "ghost actions" on the prior panel
        WWidget focusedWidget = this.getFocus();
        if (focusedWidget != null) {
            focusedWidget.releaseFocus();
        }

        // If we have a a runnable to run after the root has been reset, run it
        if (runOnRootChange != null) {
            runOnRootChange.run();
        }

        // Validate this panel to avoid any weird quirks from popping up
        newRoot.validate(this);

        // Return this (now modified) description for further modification if need be
        return this;
    }

    /**
     * Due to the client changing the number of slots present at any given time
     * (without synchronizing to server), an update of the slot updating system
     * needs to be made.
     */
    @Override
    public void updateSlotStacks(List<ItemStack> stacks) {
        for (int i = 0; i < stacks.size(); i++) {
            setStackInSlot(i, stacks.get(i));
        }
    }

    /**
     * Due to the client changing the number of slots present at any given time
     * (without synchronizing to server), an update of the slot updating system
     * needs to be made.
     */
    @Override
    public void setStackInSlot(int slot, ItemStack stack) {
        // Use our tracked slot list (which is not cleared when panels change)
        // instead of the slots list to avoid "no slot exists" errors
        trackedSlots.get(slot).setStack(stack);
    }

    // Slot peers need to be sustained across panels, even if they
    @Override
    public void addSlotPeer(ValidatedSlot slot) {
        super.addSlotPeer(slot);
        trackedSlots.put(slot.id, slot);
    }

    // Sets the root to a BasePanel instance, using the cached value if already exists
    private void buildAsBase() {
        // Generate the base panel, if it does not already exist
        if (basePanel == null) {
            basePanel = new BasePanel();
        }

        // Update the location of the loaded slot
        loadedSlot.setLocation(72, 35);

        // Swap to the base panel
        setRootPanel(basePanel);
    }

    // Sets the root to a ConfigPanel instance, using the cached value if already exists
    private void buildAsConfig() {
        // Generate the configuration panel, if it does not already exist
        if (configPanel == null) {
            configPanel = new ConfigPanel();
        }

        // Update the location of the loaded slot
        loadedSlot.setLocation((CONFIG_WIDTH/2)-9, CONFIG_VISIBLE_HEIGHT+10);

        // Swap to the ConfigPanel
        setRootPanel(configPanel);
    }

    // Sets the root to a ResultsPanel instance, using the cached value if already exists
    private void buildAsResults() {
        // If you try to switch the results panel before it exists, terminate early
        if (resultsPanel == null) {
            return;
        }
        // If the above did not fail, swap to the results panel and update the slot location
        loadedSlot.setLocation((RESULTS_WIDTH/2)-9, RESULTS_VISIBLE_HEIGHT+10);
        setRootPanel(resultsPanel);
    }

    /**
     * Overrides LibGUIs asinine "assume the user is an idiot, override their
     * background choice on resize" procedure, allowing custom background
     * renderers to be used if they have been specified. Will likely be
     * removed if/when LibGUI swaps from its garbage "single-use" painters to
     * cache-able, re-usable structures.
     */
    @Override
    @Environment(EnvType.CLIENT)
    public void addPainters() {
        if (rootPanel!=null && !fullscreen) {
            // All panels will probably extend this, but its safer to check
            if (rootPanel instanceof BackgroundPainting) {
                ((BackgroundPainting)rootPanel).initBackgroundPainter();
            }

            else {
                rootPanel.setBackgroundPainter(BackgroundPainter.VANILLA);
            }
        }
    }

    /**
     * Just does clean up stuff created for the GUI when the GUI is closed, to
     * prevent pseudo-"memory leaks". In this case, it deletes the inventories so
     * they don't eventually drag the server to a crawl, and drops any worm that
     * may have been left in the inventory by returning it too the player.
     */
    @Override
    public void close(PlayerEntity player) {
        // Give or drop all inventory items for the player
        this.dropInventory(player, world, blockInventory);
        // Close the inventory
        blockInventory.onClose(player);
        // Stop tracking this handler so it can be safely deleted
        if (!world.isClient) {
            activeInstances.remove(syncId);
        }
        // Run anything else that needs to occur
        super.close(player);
    }

    // -- Internal Utility Classes -- //
    // - Base (on-use) Panel - //
    /**
     * The generic, baseline sequencer panel, displayed when the user opens the
     * GUI for the first time. Simply provides a slot in which to place a worm
     * that the player desires to sequence, and buttons to jump to the more
     * detailed panels for further investigation or querying.
     */
    protected class BasePanel extends WPlainPanel implements BackgroundPainting {
        public BasePanel() {
            // The root panel, with a paint override to force it to render when a GUI switch occurs
            WPlainPanel root = this;

            root.setSize(160, 140);

            // Add the slot for the sequencing item
            root.add(loadedSlot, 72, 35);

            // The animated DNA texture used when sequencing is ongoing
            WRollingFrameSprite dnaSprite = new WRollingFrameSprite(
                    BioCircuitry.id("textures/gui/sprites/sequence.png"),
                    0.00625f, 10, 1/3f, WRollingFrameSprite.Direction.LEFT) {
                @Override
                public boolean isActive() {
                    int progress = propertyDelegate.get(0);
                    return (progress > 0 && progress < SequencerInventory.MAX_PROGRESS);
                }
            };
            dnaSprite.setSize(40, 20);
            root.add(dnaSprite, 61, 10);

            // The config button, allowing the user to color-code certain sequences
            WButton configButton = configButton();
            root.add(configButton, 5, 37, 60, 20);

            // The results button, allowing the user to view color coded sequence information
            WButton resultsButton = resultsButton();
            root.add(resultsButton, 97, 37, 60, 20);

            // Add the player inventory panel
            root.add(new WPlayerInvPanel(playerInventory), 0, 60);
        }

        @Override
        @Environment(EnvType.CLIENT)
        public void initBackgroundPainter() {
            this.setBackgroundPainter(BackgroundPainter.VANILLA);
        }
    }

    // - Configuration Panel - //
    // Convenient constants
    private final static int CONFIG_HEIGHT = 200;
    private final static int CONFIG_VISIBLE_HEIGHT = 170;
    private final static int CONFIG_WIDTH = 250;
    private static final int PROT_LIST_WIDTH = 65;
    // The -5 is to account for nested panel "border" width, which is only accounted for some of the time by LibGUI...
    private static final int INFO_WIDTH = CONFIG_WIDTH-PROT_LIST_WIDTH-8;

    /**
     * The panel which displays protein information data, as well as allowing
     * the player to customize how each protein should be displayed in the
     * tooltip and results post-sequencing
     */
    protected class ConfigPanel extends WPlainPanel implements BackgroundPainting {

        private final WPlainPanel visiblePanel;

        private WButton lastPressed = null;

        public ConfigPanel() {
            // Scale the panel correctly
            this.setSize(CONFIG_WIDTH, CONFIG_HEIGHT);

            // The actual baseplate panel for most of the components in this class
            visiblePanel = new WPlainPanel();
            this.add(visiblePanel, 0, 0, CONFIG_WIDTH, CONFIG_VISIBLE_HEIGHT);

            // The return button, allowing the user to return to the original GUI
            WButton returnButton = sequencingButton();
            this.add(returnButton, 10, CONFIG_VISIBLE_HEIGHT+10, GENERIC_BUTTON_WIDTH, 20);

            // The results button, allowing the user to jump to the results (if any)
            WButton resultsButton = resultsButton();
            this.add(resultsButton, CONFIG_WIDTH-GENERIC_BUTTON_WIDTH-10, CONFIG_VISIBLE_HEIGHT+10, GENERIC_BUTTON_WIDTH, 20);

            // The information panel, displaying information on the selected protein
            ConfigInfoPanel configInfoPanel = new ConfigInfoPanel();
            visiblePanel.add(configInfoPanel, PROT_LIST_WIDTH+5, 0, INFO_WIDTH, CONFIG_VISIBLE_HEIGHT);

            // The list of protein buttons to display to the user
            ArrayList<WormProtein> labels = new ArrayList<>(WormProteinHandler.getAll());
            // Needed to account for LibGUI jank
            labels.add(WormProteinHandler.dummy);
            WListPanel<WormProtein, WButton> listPanel = new WListPanel<>(labels,
                    // This needs to be done because LibGUI is a handholdy fuck
                    // otherwise and assumes you're too stupid to set the size yourself
                    () -> new WButton() {
                        @Override
                        public void setSize(int x, int y) {
                            width = 50;
                            height = 20;
                        }
                    },
                    (prot, button) -> {
                        button.setLabel(new LiteralText(prot.getLabel()));
                        // On press, update the information and disable the button
                        // Also re-enable the prior button pressed, if needed
                        button.setOnClick(() -> {
                            configInfoPanel.setTrackedProtein(prot);
                            if (lastPressed != null) lastPressed.setEnabled(true);
                            lastPressed = button;
                            lastPressed.setEnabled(false);
                        });
                    }
            );
            listPanel.setListItemHeight(20);
            visiblePanel.add(listPanel, 0, 0, PROT_LIST_WIDTH, CONFIG_VISIBLE_HEIGHT);

            // Add the loaded slot for view by the user
            this.add(loadedSlot, (CONFIG_WIDTH/2)-9, CONFIG_VISIBLE_HEIGHT+10);
        }

        @Override
        @Environment(EnvType.CLIENT)
        public void initBackgroundPainter() {
            visiblePanel.setBackgroundPainter(BackgroundRendering.VANILLA_TIGHT);
            this.setBackgroundPainter(BackgroundRendering.TRANSPARENT);
        }
    }

    // The maximum string length the hex code is allowed to be
    private static final int HEX_LENGTH = 6;

    /**
     * A panel nested within the Config panel which displays info for the
     * selected protein.
     */
    protected class ConfigInfoPanel extends WPlainPanel implements BackgroundPainting {
        // Elements kept for ease-of-access
        private WormProtein trackedProtein = null;

        // Panel widgets used during drawing, update as needed
        private final WLabel defaultLabel;
        private final WLabel protName;
        private final WText protDesc;
        private final WLabel motilLabel;
        private final WLabel bmassLabel;
        private final WText reactLabel;
        private final WLabel hexLabel;
        private final WTextField colorHexText;
        private final WButton saveColorButton;

        public ConfigInfoPanel() {
            super();

            // The "no protein selected" label, defaulted to when nothing else is present
            // Only draw this message when the selected protein is null
            // The label which displays any messages to the user center-screen
            defaultLabel = new WLabel(new TranslatableText(String.format("label.%s.none_selected", BioCircuitry.MOD_ID)));
            defaultLabel.setVerticalAlignment(VerticalAlignment.CENTER);
            defaultLabel.setHorizontalAlignment(HorizontalAlignment.CENTER);
            // We start only showing this widget
            this.add(defaultLabel, 0, 0, INFO_WIDTH, CONFIG_VISIBLE_HEIGHT);

            // Initialize the protein describing labels, caching them for later
            LiteralText blankText = new LiteralText("");
            protName = new WLabel(blankText);
            protName.setHorizontalAlignment(HorizontalAlignment.CENTER);
            protDesc = new WText(blankText);
            motilLabel = new WLabel(blankText);
            bmassLabel = new WLabel(blankText);
            reactLabel = new WText(blankText);
            reactLabel.setHorizontalAlignment(HorizontalAlignment.CENTER);
            reactLabel.setVerticalAlignment(VerticalAlignment.CENTER);

            // Color selection labels, for use in configuring the sequence
            hexLabel = new WLabel("0x");
            colorHexText = new WTextField() {
                @Override
                @Environment(EnvType.CLIENT)
                public void onKeyPressed(int ch, int key, int modifiers) {
                    // Do everything else
                    super.onKeyPressed(ch, key, modifiers);
                    // Reset the color to white if the text is not max length
                    if (this.text.length() == this.maxLength) {
                        try {
                            this.setEnabledColor(Integer.parseInt(this.text, 16));
                        } catch (Exception exc) {
                            this.setEnabledColor(0xFF5555);
                        }
                    } else {
                        this.setEnabledColor(0xFFFFFF);
                    }
                    // Re-enable the save button now that changes have occurred
                    saveColorButton.setEnabled(true);
                }

                @Override
                @Environment(EnvType.CLIENT)
                public void onCharTyped(char ch) {
                    // Update the text
                    super.onCharTyped(ch);
                    // Update the text color if we may now have a valid color
                    if (this.text.length() == this.maxLength) {
                        try {
                            this.setEnabledColor(Integer.parseInt(this.text, 16));
                        } catch (Exception exc) {
                            this.setEnabledColor(0xFF5555);
                        }
                    }
                }
            };
            colorHexText.setMaxLength(HEX_LENGTH);
            colorHexText.setHost(SequencerGuiDescription.this); // Needed since apparently "add" is insufficient
            saveColorButton = new WButton();
            saveColorButton.setOnClick(() -> {
                updateProteinColorMap();
                saveColorButton.setEnabled(false);
            });
            // Start disabled until a change occurs
            saveColorButton.setEnabled(false);
            saveColorButton.setLabel(new TranslatableText(String.format("button.%s.save", BioCircuitry.MOD_ID)));
        }

        public void setTrackedProtein(WormProtein newProtein) {
            // Update this GUI to a "blank" state
            this.children.clear();
            // Update the current Protein
            this.trackedProtein = newProtein;
            // Update the display if the new protein is not null
            if (trackedProtein != null) {
                // Clear the children in
                Identifier id = WormProteinHandler.getId(trackedProtein);
                // If the player submitted an invalid protein, just reset to default
                if (id == null) {
                    this.trackedProtein = null;
                    this.add(defaultLabel, 0, 0, INFO_WIDTH, CONFIG_VISIBLE_HEIGHT);
                    return;
                }
                // Otherwise, update our ID and proceed
                String protId = id.getPath();
                // Update the protein's header name
                this.protName.setText(new TranslatableText(String.format(
                        "protein.%s.%s.name",
                        BioCircuitry.MOD_ID,
                        protId)).formatted(Formatting.UNDERLINE)
                );
                this.add(protName, 0, 4, INFO_WIDTH, 10);
                // Update the description of said protein
                this.protDesc.setText(new TranslatableText(String.format(
                        "protein.%s.%s.desc",
                        BioCircuitry.MOD_ID,
                        protId))
                );
                // Allow for 3 lines of text, max, before running into other elements
                this.add(protDesc, 0, 15, INFO_WIDTH, 60);
                // -- Reaction Elements -- //
                // The motility value of the protein
                motilLabel.setText(new TranslatableText(String.format(
                        "label.%s.motility",
                        BioCircuitry.MOD_ID), trackedProtein.getMotility())
                        .formatted(Formatting.BLUE)
                );
                this.add(motilLabel, 0, 75, INFO_WIDTH, 10);
                bmassLabel.setText(new TranslatableText(String.format(
                        "label.%s.biomass",
                        BioCircuitry.MOD_ID), trackedProtein.getBiomass())
                        .formatted(Formatting.BLUE)
                );
                this.add(bmassLabel, 0, 85, INFO_WIDTH, 10);

                // Draw the chemical reaction (with products, if any)
                reactLabel.setText(buildReactionText());
                this.add(reactLabel, 0, 75, INFO_WIDTH, CONFIG_VISIBLE_HEIGHT-75);

                // Draw the sequence color configuration components
                this.add(hexLabel, (INFO_WIDTH/2)-53, CONFIG_VISIBLE_HEIGHT-14, 12, 10);
                // See if theirs already a color mapped to this protein
                CompoundTag seqTag = ((SequencerInventory)blockInventory).sequencerStack.getSubTag(SequencerItem.COLOR_MAP_KEY);
                // Default to the generic tooltip color otherwise
                int color = BioCircuitry.GENERIC_TOOLTIP_COLOR;
                if (seqTag != null) {
                    int newColor = seqTag.getInt(id.toString());
                    color = newColor == 0 ? color : newColor;
                }
                String hexString = Integer.toHexString(color);
                if (hexString.length() < HEX_LENGTH) {
                    String leadString = String.join("", Collections.nCopies(HEX_LENGTH-hexString.length(), "0"));
                    hexString = leadString + hexString;
                }
                colorHexText.setText(hexString);
                colorHexText.setEnabledColor(color);
                this.add(colorHexText, (INFO_WIDTH/2)-40, CONFIG_VISIBLE_HEIGHT-20, 50, 20);
                // Color save button
                this.add(saveColorButton, (INFO_WIDTH/2)+13, CONFIG_VISIBLE_HEIGHT-20, 40, 20);
            }
            // Otherwise, reset the info screen to its default state
            else {
                this.add(defaultLabel, 0, 0, INFO_WIDTH, CONFIG_VISIBLE_HEIGHT);
            }
        }

        private void updateProteinColorMap() {
            // This should only be run client-side
            if (!world.isClient) {
                return;
            }
            // Confirm everything is valid before proceeding
            int color;
            try {
                color = Integer.parseInt(colorHexText.getText(), 16);
            } catch (Exception e) {
                return;
            }
            // Run any update operations the other panels in this GUI might need
            updateProteinMappedColor(trackedProtein, color);
            // Synchronize any changes to the server
            if (world.isClient) {
                SequencerPacketManager.sendColorUpdateC2SPacket(WormProteinHandler.getId(trackedProtein), color, syncId);
            }
        }

        private TranslatableText buildReactionText() {
            // Determine the costs and products for the text
            TranslatableText costText = new TranslatableText("");
            TranslatableText productText = new TranslatableText("");
            trackedProtein.getReaction().forEach((m, i) -> {
                TranslatableText metabText = m.getTranslatableText();
                if (i <= 0) {
                    if (!costText.getSiblings().isEmpty()) {
                        costText.append(" + ");
                    }
                    costText.append(new LiteralText((-i) + " ").formatted(Formatting.DARK_RED));
                    costText.append(metabText);
                } else {
                    if (!productText.getSiblings().isEmpty()) {
                        productText.append(" + ");
                    }
                    productText.append(new LiteralText(i + " ").formatted(Formatting.DARK_GREEN));
                    productText.append(metabText);
                }
            });
            // Add the list of products, if any
            if (trackedProtein.getProducts() != null) {
                trackedProtein.getProducts().forEach(p -> {
                    if (!productText.getSiblings().isEmpty()) {
                        productText.append(" + ");
                    }
                    productText.append(new LiteralText(p.getRight() + " ").formatted(Formatting.DARK_GREEN));
                    productText.append(new TranslatableText(p.getLeft().getTranslationKey()).formatted(Formatting.ITALIC));
                });
            }
            // Build the Text for each side of the reaction
            TranslatableText fullText;
            if (costText.getSiblings().isEmpty()) {
                fullText = new TranslatableText(String.format("label.%s.null", BioCircuitry.MOD_ID));
            } else {
                fullText = costText;
            }
            fullText.append(" -> ");
            if (productText.getSiblings().isEmpty()) {
                fullText.append(new TranslatableText(String.format("label.%s.null", BioCircuitry.MOD_ID)));
            } else {
                fullText.append(productText);
            }

            // Return the resulting string
            return fullText;
        }

        @Override
        @Environment(EnvType.CLIENT)
        public void initBackgroundPainter() {
            this.setBackgroundPainter(BackgroundRendering.TRANSPARENT);
        }
    }

    // - Results Panel - //
    // Convenient constants
    private final static int RESULTS_HEIGHT = 200;
    private final static int RESULTS_VISIBLE_HEIGHT = 170;
    private final static int RESULTS_WIDTH = 250;
    private final static int RESULTS_INFO_WIDTH = 250 - 17; // Width of the scroll bar + some space

    /**
     * The panel which displays the results of sequencing operation, using the
     * color codes set in configuration to display sequencing data clearly for
     * evaluation by the player. Note that it "caches" the last sequence run,
     * but updates when the stack changes (post-sequencing). This means an
     * ItemStack does not be in the sequencer for you to review its information,
     * and can be dropped after the sequencer finishes for other players to use
     * (or to discard it).
     */
    protected class ResultsPanel extends WPlainPanel implements BackgroundPainting {
        // The visible information containing panel, not including "return" buttons
        private final WPlainPanel visiblePanel;
        // The panel on the visible panel displaying the (scrolling) sequence information
        private final WScrollPanel infoPanel;

        // The mode switching button
        private final WButton seqTypeModeButton;

        // The cached (sequenced) ItemStack for which the results should be pulled
        private ItemStack cachedStack;
        // Cached components related to the cached ItemStack, used to determine the
        private WormProteome cachedProteome;
        private WormGenome cachedGenome;

        // Active mode tracking (start in Amino Acid mode)
        private boolean aminoAcidMode = true;

        // The currently displayed information for the ItemStack
        private final WText infoText;

        public ResultsPanel(ItemStack cachedStack) {
            // Initialize the root panel to keep everything sorted nicely
            this.setSize(RESULTS_WIDTH, RESULTS_HEIGHT);

            // The actual baseplate panel for most of the components in this class
            visiblePanel = new WPlainPanel();
            // Force the size for the panel
            this.add(visiblePanel, 0, 0, RESULTS_WIDTH, RESULTS_VISIBLE_HEIGHT);

            // Save our cached stack to pull the results from later
            infoText = new WText(new LiteralText(""));

            // Save the cached stack for later use
            this.setCachedStack(cachedStack);

            // Add the panel which will display the desired information
            infoPanel = new WScrollPanel(infoText);
            infoPanel.setScrollingHorizontally(TriState.FALSE);
            // Provide a 3 pixel buffer + lower panel buffer for the mode swap button
            visiblePanel.add(infoPanel, 4, 4, RESULTS_WIDTH-4*2, RESULTS_VISIBLE_HEIGHT-34);

            // Buttons, added last to LibGUI doesn't ignore them...
            // The mode swap button for changing between Amino and Nucleic Acid view mode
            seqTypeModeButton = new WButton();
            seqTypeModeButton.setLabel(new TranslatableText(String.format("button.%s.sequence_mode.%s", BioCircuitry.MOD_ID, aminoAcidMode ? "amino" : "nucleic")));
            // Allow the button to toggle the sequence mode
            seqTypeModeButton.setOnClick(this::toggleSequenceMode);
            visiblePanel.add(seqTypeModeButton, 0, RESULTS_VISIBLE_HEIGHT-20, RESULTS_WIDTH, 20);

            // The return button, allowing the user to return to the original GUI
            WButton sequencingButton = sequencingButton();
            this.add(sequencingButton, 10, RESULTS_VISIBLE_HEIGHT+10, GENERIC_BUTTON_WIDTH, 20);

            // The config button, allowing the user to update the configuration on the fly
            WButton configButton = configButton();
            this.add(configButton, RESULTS_WIDTH-GENERIC_BUTTON_WIDTH-10, RESULTS_VISIBLE_HEIGHT+10, GENERIC_BUTTON_WIDTH, 20);

            // Add the loaded slot for view by the user
            children.add(loadedSlot);
        }

        protected void toggleSequenceMode() {
            // Flip the mode to the opposite value
            aminoAcidMode = !aminoAcidMode;
            // Update it's text to reflect the change
            seqTypeModeButton.setLabel(new TranslatableText(String.format("button.%s.sequence_mode.%s", BioCircuitry.MOD_ID, aminoAcidMode ? "amino" : "nucleic")));
            // Update the information text to reflect the change
            if (world.isClient) {
                //noinspection MethodCallSideOnly
                updateInfoText();
            }
        }

        @Environment(EnvType.CLIENT)
        protected void setCachedStack(ItemStack newStack) {
            // Refuse to cache a stack without genetic data
            if (!WormProteome.isSequenced(newStack.getTag())) {
                return;
            }
            // Confirm the changes are worth applying
            else if (cachedStack != null && ItemStack.areEqual(cachedStack, newStack)) {
                return;
            }
            // Update the cached stack
            cachedStack = newStack;
            // Update the cached proteome
            cachedProteome = new WormProteome(cachedStack);
            // Update the cached genome
            cachedGenome = new WormGenome(cachedStack);
            // Update the information text to reflect the new stack's info
            updateInfoText();
        }

        @Environment(EnvType.CLIENT)
        protected void updateInfoText() {
            // Determine whether the text should be in Amino Acid or Nucleic Acid form
            Text text;
            if (aminoAcidMode) {
                text = buildTerseAminoAcidInfo();
            } else {
                text = buildTerseNucleicAcidInfo();
            }
            // Update the text with the newly generated text
            infoText.setText(text);
            // Redo what LibGUI does because its obsessed with making everything private for fuck knows why
            TextRenderer font = MinecraftClient.getInstance().textRenderer;
            int lineCount = font.wrapLines(text, RESULTS_INFO_WIDTH).size();
            infoText.setSize(RESULTS_INFO_WIDTH, lineCount*font.fontHeight);
            if (infoPanel != null) {
                infoPanel.layout();
            }
            // Validate the now-changed screen
            validate(SequencerGuiDescription.this);
        }

        /**
         * Generate the Amino Acid (translated) info text to be displayed to the
         * user, chromosome by chromosome, acid by acid. Accommodates for the
         * sequenced worm's genomic color map, NOT the sequencers (this allows
         * non-configured sequencers to act as "passive" results viewers).
         */
        protected MutableText buildTerseAminoAcidInfo() {
            // Initialization
            MutableText text = new LiteralText("");
            text.setStyle(BioCircuitry.GENERIC_TOOLTIP_STYLE);
            ArrayList<ArrayList<WormProtein>> proteins = cachedProteome.getProteins();
            ArrayList<ArrayList<Integer>> spacers = cachedProteome.getSpacers();
            HashMap<WormProtein, Integer> colorMap = cachedProteome.getColorMap();
            List<String> chromStrings = cachedGenome.getChromosomeStrings();

            // Parse through each chromosomes protein values one by one
            for (int i = 0; i < proteins.size(); i++) {
                // Initialize the descriptive text with the header
                LiteralText chromText = new LiteralText(i + ":\n");
                chromText = buildAminoChromText(chromText, proteins.get(i), spacers.get(i), chromStrings.get(i), colorMap);

                // Add a newline character to the end if this is not the last chromosome
                if (i != proteins.size()-1) {
                    chromText.append("\n");
                }
                // Add this text to the children of our current text
                text.append(chromText);
            }
            return text;
        }

        /**
         * Helper function to build a single chromosome's Amino Acid representation
         * @param chromText The "seed" text, on top of which this function will build
         * @param proteins The proteins on the chromosome of choice
         * @param spacers The spacers on the chromosome of choice
         * @param nucleicSequence The nucleic sequence of the chromosome of choice
         * @param colorMap The color map for the worm that owns this chromosome
         * @return The now-updated text, representing the chromosome's amino acid sequence(s)
         */
        protected LiteralText buildAminoChromText(LiteralText chromText, List<WormProtein> proteins, List<Integer> spacers,
                                                  String nucleicSequence, HashMap<WormProtein, Integer> colorMap) {
            // Add the first spacer sequence
            int spacerLen = spacers.get(0);
            float spacerCentiMorgan = (100f * spacerLen)/nucleicSequence.length();
            HoverEvent spacerToolTip = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText(String.format("%2.2fcM", spacerCentiMorgan)));
            Style spacerStyle = Style.EMPTY.withHoverEvent(spacerToolTip);
            chromText.append(new LiteralText(String.join("", Collections.nCopies(spacerLen/3, "-"))).setStyle(spacerStyle));

            // Start building up the text with each protein
            int pos = spacerLen;
            int j;
            for (j = 0; j < proteins.size(); j++) {
                // Pull out the protein to base our color-coding off of
                WormProtein prot = proteins.get(j);

                // Build the string representation for this protein given the genome its taken from
                ArrayList<AminoAcid> aminoSeq = AminoAcid.translate(nucleicSequence.substring(pos));
                LiteralText aminoText;
                if (aminoSeq == null) {
                    BioCircuitry.LOGGER.warn("The Proteome does match the Genome; something has gone very wrong");
                    pos += prot.getSequence().size()*3;
                    aminoText = new LiteralText("(" + prot.getLabel() + "?)");
                } else {
                    pos += aminoSeq.size()*3;
                    aminoText = new LiteralText(aminoSeq.stream().map(Enum::toString).collect(Collectors.joining("")));
                }

                // Determine the color map for the protein
                Style mappedStyle;
                if (colorMap != null) {
                    mappedStyle = Style.EMPTY.withColor(TextColor.fromRgb(colorMap.getOrDefault(prot, BioCircuitry.GENERIC_TOOLTIP_COLOR)));
                } else {
                    mappedStyle = BioCircuitry.GENERIC_TOOLTIP_STYLE;
                }

                // Set the "tooltip" for the protein's text, displayed when moused over
                mappedStyle = mappedStyle.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText(prot.getLabel()).setStyle(mappedStyle)));

                // Set the protein sequence text to use this new style
                aminoText.setStyle(mappedStyle);

                // Append the now-styled protein text
                chromText.append(aminoText);

                // Add the spacer representing text
                spacerLen = spacers.get(j+1);
                if (spacerLen > 0) {
                    spacerCentiMorgan = (100f * spacerLen)/nucleicSequence.length();
                    spacerToolTip = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText(String.format("%2.2fcM", spacerCentiMorgan)));
                    spacerStyle = Style.EMPTY.withHoverEvent(spacerToolTip);
                    chromText.append(new LiteralText(String.join("", Collections.nCopies(spacerLen/3, "-"))).setStyle(spacerStyle));
                }
                // If the spacer implies an overlap, draw an indicator for that instead
                else {
                    chromText.append(String.join("/", Collections.nCopies(spacerLen%3+1, "/")));
                }

                // Update the position along the string with the new spacer info
                pos += spacerLen;
            }

            // Add a trailing newline character to provide space
            chromText.append(new LiteralText("\n"));

            // Return the value
            return chromText;
        }

        /**
         * Generate the Nucleic Acid (non-translated) info text to be displayed
         * to the user, chromosome by chromosome, acid by acid. Accommodates for
         * the sequenced worm's genomic color map, NOT the sequencers (this
         * allows non-configured sequencers to act as "passive" results viewers).
         */
        protected MutableText buildTerseNucleicAcidInfo() {
            // Initialization
            MutableText text = new LiteralText("");
            text.setStyle(BioCircuitry.GENERIC_TOOLTIP_STYLE);
            ArrayList<ArrayList<WormProtein>> proteins = cachedProteome.getProteins();
            ArrayList<ArrayList<Integer>> spacers = cachedProteome.getSpacers();
            HashMap<WormProtein, Integer> colorMap = cachedProteome.getColorMap();
            List<String> chromStrings = cachedGenome.getChromosomeStrings();

            // Parse through each chromosomes protein values one by one
            for (int i = 0; i < proteins.size(); i++) {
                // Initialize the descriptive text
                LiteralText chromText = new LiteralText(i + ":\n");

                // Build up the chrom text
                chromText = buildNucleicChromText(chromText, proteins.get(i), spacers.get(i), chromStrings.get(i), colorMap);

                // Add a newline character to the end if this is not the last chromosome
                if (i != proteins.size()-1) {
                    chromText.append("\n");
                }
                // Add this text to the children of our current text
                text.append(chromText);
            }
            return text;
        }

        /**
         * Helper function to build a single chromosome's Nucleic Acid representation
         * @param chromText The "seed" text, on top of which this function will build
         * @param proteins The proteins on the chromosome of choice
         * @param spacers The spacers on the chromosome of choice
         * @param nucleicSequence The nucleic sequence of the chromosome of choice
         * @param colorMap The color map for the worm that owns this chromosome
         * @return The now-updated text, representing the chromosome's amino acid sequence(s)
         */
        protected LiteralText buildNucleicChromText(LiteralText chromText, List<WormProtein> proteins, List<Integer> spacers,
                                                  String nucleicSequence, HashMap<WormProtein, Integer> colorMap) {
            // Add the leading spacer sequence for the text
            int spacerLen = spacers.get(0);
            float spacerCentiMorgan = (100f * spacerLen)/nucleicSequence.length();
            HoverEvent spacerToolTip = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText(String.format("%2.2fcM", spacerCentiMorgan)));
            Style spacerStyle = Style.EMPTY.withHoverEvent(spacerToolTip);
            chromText.append(new LiteralText(nucleicSequence.substring(0, spacerLen))).setStyle(spacerStyle);

            // Start building up the text with each protein
            int pos = spacerLen;
            for (int j = 0; j < proteins.size(); j++) {
                // Determine the color map for the protein
                WormProtein prot = proteins.get(j);

                // Build the string representation for this protein's nucleic sequence
                ArrayList<AminoAcid> aminoSeq = AminoAcid.translate(nucleicSequence.substring(pos));
                LiteralText nucleicText;
                if (aminoSeq == null) {
                    BioCircuitry.LOGGER.warn("The Proteome does match the Genome; something has gone very wrong");
                    nucleicText = new LiteralText(nucleicSequence.substring(pos, pos+prot.getSequence().size()*3));
                    pos += prot.getSequence().size()*3;
                } else {
                    nucleicText = new LiteralText(nucleicSequence.substring(pos, pos+aminoSeq.size()*3));
                    pos += aminoSeq.size()*3;
                }

                // Determine the style the text should abide by
                Style mappedStyle;
                if (colorMap != null) {
                    mappedStyle = Style.EMPTY.withColor(TextColor.fromRgb(colorMap.getOrDefault(prot, BioCircuitry.GENERIC_TOOLTIP_COLOR)));
                } else {
                    mappedStyle = BioCircuitry.GENERIC_TOOLTIP_STYLE;
                }

                // Set the "tooltip" for the protein's text, displayed when moused over
                mappedStyle = mappedStyle.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText(prot.getLabel()).setStyle(mappedStyle)));

                // Underline coding sequences to make them easier to distinguish from non-coding sequence
                mappedStyle = mappedStyle.withBold(true);

                // Set the protein sequence text to use this new style
                nucleicText.setStyle(mappedStyle);

                // Append the now-styled protein text
                chromText.append(nucleicText);

                // Add the trailing spacer's info
                spacerLen = spacers.get(j+1);
                // Only draw the spacer string if its positive (represents a non-overlapping section)
                if (spacerLen >= 0) {
                    spacerCentiMorgan = (100f * spacerLen)/nucleicSequence.length();
                    spacerToolTip = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText(String.format("%2.2fcM", spacerCentiMorgan)));
                    spacerStyle = Style.EMPTY.withHoverEvent(spacerToolTip);
                    chromText.append(new LiteralText(nucleicSequence.substring(pos, pos+spacerLen)).setStyle(spacerStyle));
                }
                // Otherwise, draw a symbol representing overlapping sections
                else {
                    chromText.append(new LiteralText("/"));
                }

                // Update the position along the sequencer with this spacer's length
                pos += spacerLen;
            }

            // Add the final spacer
            chromText.append(nucleicSequence.substring(pos));

            // Add a trailing newline character to provide space
            chromText.append(new LiteralText("\n"));

            // Return the now-update text
            return chromText;
        }

        @Override
        @Environment(EnvType.CLIENT)
        public void initBackgroundPainter() {
            visiblePanel.setBackgroundPainter(BackgroundRendering.VANILLA_TIGHT);
            infoPanel.setBackgroundPainter(BackgroundRendering.RETRO_GREEN);
            this.setBackgroundPainter(BackgroundRendering.TRANSPARENT);
        }
    }
}
