/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.descriptions;

import io.github.cottonmc.cotton.gui.widget.*;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.block_entities.FeedBinEntity;
import me.someoneinparticular.biocircuitry.gui.ModScreenHandlers;
import me.someoneinparticular.biocircuitry.items.ModItems;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.util.Identifier;

public class FeedBinGuiDescription extends PlayerInvTransferingGuiDescription {
    /**
     * Client-side, buffer-fed constructor. The buffer must contain a block
     * position, use to find the block entity to hook into.
     */
    public FeedBinGuiDescription(int syncId, PlayerInventory playerInv, PacketByteBuf buffer) {
        this(syncId, playerInv, (FeedBinEntity) playerInv.player.world.getBlockEntity(buffer.readBlockPos()));
    }

    /**
     * Side-ignorant constructor, building all components required for both the
     * screen and the screen handler
     * @param syncId The ID used to synchronize the client-side screen and
     *               dual-sided screen handler
     * @param playerInv The player's inventory to be displayed and modified
     *                  within the GUI
     * @param feedBin The feed bin entity this GUI corresponds too
     */
    public FeedBinGuiDescription(int syncId, PlayerInventory playerInv, FeedBinEntity feedBin) {
        // Initialize the base GUI description
        super(ModScreenHandlers.FEED_BIN_SCREEN_HANDLER, syncId, playerInv);

        // Track the bound feed bin for use later
        if (feedBin == null) {
            BioCircuitry.LOGGER.warn("NULL FeedBin GIVEN TO FeedBin GUI INSTANCE");
            feedBin = new FeedBinEntity();
        }
        this.blockInventory = feedBin;

        // Build up the GUI itself
        WPlainPanel root = new WPlainPanel();

        // The "upper bound" for the slots
        int upperBound = 17;

        // Add the player inventory
        WPlayerInvPanel playerPanel = createPlayerInventoryPanel();
        root.add(playerPanel, 0, 57);

        // The worm inventory (declared first so it is prioritized in quick moves)
        WItemSlot wormSlot = WItemSlot.of(feedBin.getWormInv(), 0);
        wormSlot.setFilter(itemStack -> itemStack.getItem() == ModItems.WORM);
        root.add(wormSlot, 72, upperBound);

        // The input inventory
        WItemSlot inputSlots = WItemSlot.of(feedBin.getInputInv(), 0, 2, 2);
        root.add(inputSlots, 25, upperBound);

        // The input inventory
        WItemSlot outputSlots = WItemSlot.of(feedBin.getOutputInv(), 0, 2, 2);
        outputSlots.setInsertingAllowed(false);
        root.add(outputSlots, 101, upperBound);

        // The stencil the worm inventory should use
        WSprite wormStencil = new WSprite(BioCircuitry.id("textures/gui/sprites/stencil_worm.png"));
        root.add(wormStencil, 73, upperBound+1, 16, 16);

        // The progress bar
        WBar progress = new WBar(
                BioCircuitry.id("textures/gui/sprites/arrow_bg.png"),
                BioCircuitry.id("textures/gui/sprites/arrow_fg.png"),
                0, 1, WBar.Direction.RIGHT);
        root.add(progress, 66, 37, 30, 14);

        // The running state sprite
        WSprite stateSprite = new WSprite(BioCircuitry.id("textures/gui/sprites/state_unsure.png"));
        root.add(stateSprite, 73, 0, 16, 16);

        final FeedBinEntity lockedEntity = feedBin;

        // Build the property delegate for the this GUI
        propertyDelegate = new PropertyDelegate() {
            @Override
            public int get(int index) {
                switch (index) {
                    case 0: return (int) lockedEntity.getProgress();
                    case 1: return FeedBinEntity.MAX_PROGRESS;
                    case 2: return lockedEntity.getState();
                    case 3: return FeedBinEntity.MAX_COOLDOWN;
                    default: return 0;
                }
            }

            @Override
            public void set(int index, int value) {
                switch (index) {
                    case 0: lockedEntity.setProgress(value);
                        return;
                    case 2:
                        // On a state change, update the state sprite to reflect such
                        if (value > 0) {
                            stateSprite.setImage(BioCircuitry.id("textures/gui/sprites/state_ok.png"));
                        } else if (value < 0) {
                            stateSprite.setImage(BioCircuitry.id("textures/gui/sprites/state_invalid.png"));
                        } else {
                            stateSprite.setImage(BioCircuitry.id("textures/gui/sprites/state_unsure.png"));
                        }
                        lockedEntity.setState(value);
                }
            }

            @Override
            public int size() {
                return 4;
            }
        };
        this.addProperties(propertyDelegate);

        // Update the root
        setRootPanel(root);

        // Validate all of our changes
        root.validate(this);
    }
}
