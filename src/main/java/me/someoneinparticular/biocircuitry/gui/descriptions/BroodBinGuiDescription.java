/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.descriptions;

import io.github.cottonmc.cotton.gui.widget.*;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.block_entities.BroodBinEntity;
import me.someoneinparticular.biocircuitry.gui.ModScreenHandlers;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.util.Identifier;

public class BroodBinGuiDescription extends PlayerInvTransferingGuiDescription {
    /**
     * Client-side, buffer-fed constructor. The buffer must contain a block
     * position, use to find the block entity to hook into.
     */
    public BroodBinGuiDescription(int syncId, PlayerInventory playerInv, PacketByteBuf buffer) {
        this(syncId, playerInv, (BroodBinEntity) playerInv.player.world.getBlockEntity(buffer.readBlockPos()));
    }

    /**
     * Server-side, entity-fed constructor. Directly binds the GUI to the Brood
     * Bin instance.
     */
    public BroodBinGuiDescription(int syncId, PlayerInventory playerInv, BroodBinEntity broodBin) {
        super(ModScreenHandlers.BROOD_BIN_SCREEN_HANDLER, syncId, playerInv);

        // Track the bound brood bin for use later
        if (broodBin == null) {
            BioCircuitry.LOGGER.warn("NULL BroodBin GIVEN TO BroodBin GUI INSTANCE");
            broodBin = new BroodBinEntity();
        }
        this.blockInventory = broodBin;

        final BroodBinEntity finalEntity = broodBin;

        // Build the property delegate to keep tabs on the progress
        propertyDelegate = new PropertyDelegate() {
            @Override
            public int get(int index) {
                switch (index) {
                    case 0: return finalEntity.getProgress();
                    case 1: return BroodBinEntity.MAX_PROGRESS;
                    default: return 0;
                }
            }

            @Override
            public void set(int index, int value) {
                if (index == 0) {
                    finalEntity.setProgress(value);
                }
            }

            @Override
            public int size() {
                return 2;
            }
        };
        addProperties(propertyDelegate);

        // Set up the root panel, to be build up
        WPlainPanel root = new WPlainPanel();

        // The player inventory panel
        WPlayerInvPanel playerPanel = createPlayerInventoryPanel();

        // The first "paternal" worm slot
        WItemSlot wormSlot1 = WItemSlot.of(broodBin.getPaternalInv(), 0);
        wormSlot1.setFilter(stack -> finalEntity.getPaternalInv().getWhitelist().contains(stack.getItem()));

        WSprite wormSprite1 = new WSprite(BioCircuitry.id("textures/gui/sprites/stencil_worm.png"));

        // The feed slot, where sugar used to breed the worms is place
        WItemSlot feedSlot = WItemSlot.of(broodBin.getFeedInv(), 0);
        feedSlot.setFilter(stack -> finalEntity.getFeedInv().getWhitelist().contains(stack.getItem()));

        WSprite feedSprite = new WSprite(BioCircuitry.id("textures/gui/sprites/stencil_sugar.png"));

        // The second "maternal" worm slot
        WItemSlot wormSlot2 = WItemSlot.of(broodBin.getMaternalInv(), 0);
        wormSlot2.setFilter(stack -> finalEntity.getMaternalInv().getWhitelist().contains(stack.getItem()));

        WSprite wormSprite2 = new WSprite(BioCircuitry.id("textures/gui/sprites/stencil_worm.png"));

        // The baby output slot
        WItemSlot babySlot = WItemSlot.of(broodBin.getBabyInv(), 0);
        babySlot.setFilter(stack -> finalEntity.getBabyInv().getWhitelist().contains(stack.getItem()));

        // The sequencer slot, used to auto-sequence babies if a sequencer is provided
        WItemSlot seqSlot = WItemSlot.of(broodBin.getSequencerInv(), 0);
        seqSlot.setFilter(stack -> finalEntity.getSequencerInv().getWhitelist().contains(stack.getItem()));

        WSprite seqSprite = new WSprite(BioCircuitry.id("textures/gui/sprites/stencil_sequencer.png"));

        // The progress arrow
        WBar progress = new WBar(
                BioCircuitry.id("textures/gui/sprites/long_arrow_bg.png"),
                BioCircuitry.id("textures/gui/sprites/long_arrow_fg.png"),
                0, 1, WBar.Direction.RIGHT);
        progress.setSize(44, 18);

        // Determine the y-based positional markers
        int elementHeight = wormSlot1.getHeight() + wormSlot2.getHeight() + feedSlot.getHeight();
        int infoHeight = 50;
        int yShift = (infoHeight - elementHeight) / 2 + 5; // Offset required to compensate for the title
        double slotOverlap = 0.2;

        // Determine the x-based positional markers
        int elementWidth = 80;
        int fullWidth = playerPanel.getWidth();
        int xShift = (fullWidth - elementWidth)/2;

        // Place all the elements in their correct spot
        root.add(playerPanel, 0, infoHeight);

        int slotYPos = (int) (yShift + (wormSlot1.getHeight()) * slotOverlap);
        int slotXPos = xShift+feedSlot.getWidth();
        root.add(wormSlot1, slotXPos, slotYPos);
        root.add(wormSprite1, slotXPos+1, slotYPos+1, 16, 16);

        slotYPos += wormSlot1.getHeight() * (1-slotOverlap);
        slotXPos = xShift;
        root.add(feedSlot, slotXPos, slotYPos);
        root.add(feedSprite, slotXPos+1, slotYPos+1, 16, 16);

        slotYPos += feedSlot.getHeight() * (1-slotOverlap);
        slotXPos += feedSlot.getWidth();
        root.add(wormSlot2, slotXPos, slotYPos);
        root.add(wormSprite2, slotXPos+1, slotYPos+1, 16, 16);

        slotYPos = feedSlot.getY();
        slotXPos = feedSlot.getX() + elementWidth - feedSlot.getWidth();
        root.add(babySlot, slotXPos, slotYPos);

        slotYPos = 0;
        slotXPos = fullWidth - seqSlot.getWidth();
        root.add(seqSlot, slotXPos, slotYPos);
        root.add(seqSprite, slotXPos+1, slotYPos+1, 16, 16);

        root.add(progress, feedSlot.getX() + feedSlot.getWidth(), feedSlot.getY() + (feedSlot.getHeight() - progress.getHeight())/2,
                progress.getWidth(), progress.getHeight());

        // Update the root and validate it
        setRootPanel(root);
        root.validate(this);
    }
}
