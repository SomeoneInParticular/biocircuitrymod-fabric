/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui;


import me.someoneinparticular.biocircuitry.gui.descriptions.BroodBinGuiDescription;
import me.someoneinparticular.biocircuitry.gui.descriptions.ChromatographGuiDescription;
import me.someoneinparticular.biocircuitry.gui.descriptions.FeedBinGuiDescription;
import me.someoneinparticular.biocircuitry.gui.descriptions.SequencerGuiDescription;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.screen.ScreenHandlerType;

import static me.someoneinparticular.biocircuitry.blocks.ModBlocks.BROOD_BIN;
import static me.someoneinparticular.biocircuitry.blocks.ModBlocks.FEED_BIN;
import static me.someoneinparticular.biocircuitry.items.ModItems.CHROMATOGRAPH;
import static me.someoneinparticular.biocircuitry.items.ModItems.SEQUENCER;

public class ModScreenHandlers {

    public static ScreenHandlerType<BroodBinGuiDescription> BROOD_BIN_SCREEN_HANDLER;
    public static ScreenHandlerType<FeedBinGuiDescription> FEED_BIN_SCREEN_HANDLER;
    public static ScreenHandlerType<SequencerGuiDescription> SEQUENCER_SCREEN_HANDLER;
    public static ScreenHandlerType<ChromatographGuiDescription> CHROMATOGRAPH_SCREEN_HANDLER;

    public static void initialize() {
        BROOD_BIN_SCREEN_HANDLER =
                ScreenHandlerRegistry.registerExtended(BROOD_BIN.getIdentifier(), BroodBinGuiDescription::new);
        FEED_BIN_SCREEN_HANDLER =
                ScreenHandlerRegistry.registerExtended(FEED_BIN.getIdentifier(), FeedBinGuiDescription::new);
        SEQUENCER_SCREEN_HANDLER =
                ScreenHandlerRegistry.registerExtended(SEQUENCER.getIdentifier(), SequencerGuiDescription::new);
        CHROMATOGRAPH_SCREEN_HANDLER =
                ScreenHandlerRegistry.registerSimple(CHROMATOGRAPH.getIdentifier(), ChromatographGuiDescription::new);
    }
}
