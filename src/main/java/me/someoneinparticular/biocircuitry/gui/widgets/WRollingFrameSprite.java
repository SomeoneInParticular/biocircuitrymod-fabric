/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui.widgets;

import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

/**
 * Custom sprite that allows for a "rolling" image, as if the image has been
 * stitched end to end and is being slide past a viewing window infinitely.
 */
public class WRollingFrameSprite extends WWidget {
    // The image to 'roll over'
    Identifier imageId;
    // The shift (in percent) that should be covered per frame 'tick'
    float frameShift;
    // How many milliseconds should exist between each frame 'tick'
    int frameTime;
    // Direction the rolling frame should 'roll'
    Direction direction;
    // The width of the rolling frame
    float frameWidth;

    // The last measure time
    long prior = 0;

    // A functional "hook" which can be overridden for more dynamic functionality
    public boolean isActive() {
        return true;
    }

    /** The left edge of the texture as a fraction. */
    protected float u1 = 0;
    /** The top edge of the texture as a fraction. */
    protected float v1 = 0;
    /** The right edge of the texture as a fraction. */
    protected float u2 = 1;
    /** The bottom edge of the texture as a fraction. */
    protected float v2 = 1;

    public WRollingFrameSprite(Identifier imageId, float frameShift, int frameTime, float frameWidth, Direction direction) {
        this.imageId = imageId;
        this.frameShift = frameShift;
        this.frameTime = frameTime;
        this.frameWidth = frameWidth;
        this.direction = direction;
        // Set our starting point for the image UVs
        float randomOffset = BioCircuitry.RANDOM.nextFloat() * (1 - frameWidth);
        switch (direction) {
            case LEFT:
                u1 = randomOffset;
                u2 = frameWidth + randomOffset;
                break;
            case RIGHT:
                u1 = 1-frameWidth-randomOffset;
                u2 = 1 - randomOffset;
                break;
            case UP:
                v1 = randomOffset;
                v2 = frameWidth+randomOffset;
                break;
            case DOWN:
                v1 = 1-frameWidth-randomOffset;
                v2 = 1-randomOffset;
                break;
        }
    }

    @Override
    public void paint(MatrixStack matrices, int x, int y, int mouseX, int mouseY) {
        // Get the current system time
        long now = System.nanoTime() / 1_000_000L;
        if (this.isActive() && (now - prior) >= frameTime) {
            // Update the UVs
            switch (direction) {
                case LEFT:
                    u1 += frameShift;
                    u2 += frameShift;
                    if (u2 > 1) {
                        u1 = 0;
                        u2 = frameWidth;
                    }
                    break;
                case RIGHT:
                    u1 -= frameShift;
                    u2 -= frameShift;
                    if (u1 < 0) {
                        u1 = 1-frameWidth;
                        u2 = 1;
                    }
                    break;
                case UP:
                    v1 += frameShift;
                    v2 += frameShift;
                    if (v2 > 1) {
                        v1 = 0;
                        v2 = frameWidth;
                    }
                    break;
                case DOWN:
                    v1 -= frameShift;
                    v2 -= frameShift;
                    if (v1 < 0) {
                        v1 = 1-frameWidth;
                        v2 = 1;
                    }
                    break;
            }
            // Update our prior timestamp
            prior = now;
        }

        // Render the current frame
        if (this.isActive()) {
            ScreenDrawing.texturedRect(x, y, width, height, imageId, u1, v1, u2, v2, 0XFFFFFFFF);
        } else {
            ScreenDrawing.texturedRect(x, y, width, height, imageId, u1, v1, u2, v2, 0XFF000000);
        }

    }

    public enum Direction {
        UP,
        RIGHT,
        DOWN,
        LEFT
    }
}
