/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.gui;

import me.someoneinparticular.biocircuitry.gui.screens.BroodBinScreen;
import me.someoneinparticular.biocircuitry.gui.screens.ChromatographScreen;
import me.someoneinparticular.biocircuitry.gui.screens.FeedBinScreen;
import me.someoneinparticular.biocircuitry.gui.screens.SequencerScreen;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;

@Environment(EnvType.CLIENT)
public class ModScreens {

    public static void initialize() {
        ScreenRegistry.register(ModScreenHandlers.BROOD_BIN_SCREEN_HANDLER, BroodBinScreen::new);
        ScreenRegistry.register(ModScreenHandlers.FEED_BIN_SCREEN_HANDLER, FeedBinScreen::new);
        ScreenRegistry.register(ModScreenHandlers.SEQUENCER_SCREEN_HANDLER, SequencerScreen::new);
        ScreenRegistry.register(ModScreenHandlers.CHROMATOGRAPH_SCREEN_HANDLER, ChromatographScreen::new);
    }
}
