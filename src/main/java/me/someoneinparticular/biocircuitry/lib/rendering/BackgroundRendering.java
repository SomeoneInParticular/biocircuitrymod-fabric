/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib.rendering;

import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import net.minecraft.util.Identifier;

import static io.github.cottonmc.cotton.gui.client.BackgroundPainter.createLightDarkVariants;
import static io.github.cottonmc.cotton.gui.client.BackgroundPainter.createNinePatch;

public abstract class BackgroundRendering {
    // Draws the background like normal, but has 2/4 the padding around the edges
    public static BackgroundPainter VANILLA_TIGHT = createLightDarkVariants(
            createNinePatch(new Identifier("libgui", "textures/widget/panel_light.png"), 4),
            createNinePatch(new Identifier("libgui", "textures/widget/panel_dark.png"), 4)
    );

    // Draws a transparent background (I.E. no background at all)
    public static BackgroundPainter TRANSPARENT = (left, top, panel) -> {};

    // Draws a green "retro" style background
    public static BackgroundPainter RETRO_GREEN = createNinePatch(BioCircuitry.id("textures/gui/panel_retro.png"), 4);
}
