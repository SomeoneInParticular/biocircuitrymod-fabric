/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib.rendering;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

public interface BackgroundPainting {
    @Environment(EnvType.CLIENT)
    void initBackgroundPainter();
}
