/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import com.google.common.collect.ImmutableSet;
import com.mojang.datafixers.types.Type;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.function.Supplier;

public class BioCircBlockEntityType<B extends BlockEntity> extends BlockEntityType<B> {

    protected Identifier registryID;

    public BioCircBlockEntityType(Supplier<B> supplier, Type type, String registryID, Block... blocks) {
        super(supplier, ImmutableSet.copyOf(blocks), type);
        this.registryID = BioCircuitry.id(registryID);
    }

    public void register() {
        Registry.register(Registry.BLOCK_ENTITY_TYPE, this.registryID, this);
    }
}
