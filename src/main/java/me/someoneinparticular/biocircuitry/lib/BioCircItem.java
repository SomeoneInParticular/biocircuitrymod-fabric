/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class BioCircItem extends Item {

    protected Identifier registryID;

    public BioCircItem(Settings settings, String registryID) {
        super(settings);
        this.registryID = BioCircuitry.id(registryID);
    }

    public Identifier getIdentifier() {
        return this.registryID;
    }

    public void register() {
        Registry.register(Registry.ITEM, this.registryID, this);
    }
}
