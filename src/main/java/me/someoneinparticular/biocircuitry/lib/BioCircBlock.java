/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.api.BioCircItemGroups;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class BioCircBlock extends Block {

    protected Identifier registryID;

    public BioCircBlock(FabricBlockSettings settings, String registryID) {
        super(settings);
        this.registryID = BioCircuitry.id(registryID);
    }

    public Identifier getIdentifier() {
        return this.registryID;
    }

    public void register() {
        // Register the block itself
        Registry.register(Registry.BLOCK, this.registryID, this);
        // Register the item version of the block
        Item.Settings settings = new Item.Settings().group(BioCircItemGroups.GENERAL);
        Registry.register(Registry.ITEM, this.registryID, new BlockItem(this, settings));
    }
}
