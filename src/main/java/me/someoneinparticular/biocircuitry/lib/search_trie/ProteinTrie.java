/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib.search_trie;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.proteomics.components.AminoAcid;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.minecraft.util.Pair;

import java.util.*;

/**
 * A search trie implementation to allow slight protein sequence variance.
 * Allows for amino acid sequences to be found which are "similar enough" to
 * a known protein to function as one.
 */
public class ProteinTrie {
    // The root of the Trie is always empty
    private final ProteinTrieNode root = new ProteinTrieNode();

    /**
     * Insert a new AminoAcid product sequence into the Trie
     * @param value The WormProtein to insert
     */
    public void insert(WormProtein value) {
        // Start at our root
        ProteinTrieNode current = this.root;

        // Build up the trie with any nodes that are needed to fit this new value
        for (AminoAcid a : value.getSequence()) {
            current = current.getChildren().computeIfAbsent(a, k -> new ProteinTrieNode());
        }

        // Update the leaf node's product to our new product, warning the
        // user if it already exists
        if (current.getProduct() != null) {
            BioCircuitry.LOGGER.warn(
                    "A value for sequence " + value.getSequence().toString() +
                    " already existed, and was replaced by protein " +
                    value.getLabel() + ". Are you sure this was intended?"
            );
        }
        current.setProduct(value);
    }

    /**
     * A data class to hold values in an easy to parse and interpret manner
     */
    private static class SearchDataTuple implements Comparable<SearchDataTuple>{
        int queryPos;
        int deviation;
        ProteinTrieNode node;

        SearchDataTuple(int queryPos, int deviation, ProteinTrieNode node) {
            this.queryPos = queryPos;
            this.deviation = deviation;
            this.node = node;
        }

        int getQueryPos() {
            return this.queryPos;
        }

        int getDeviation() {
            return this.deviation;
        }

        ProteinTrieNode getNode() {
            return this.node;
        }

        @Override
        public int compareTo(SearchDataTuple o) {
            if (this.deviation != o.deviation) {
                return this.deviation - o.deviation;
            } else {
                return o.queryPos - this.queryPos;
            }
        }
    }

    /**
     * Query the Trie and find the closest match for the query.
     * Uses a priority queue for optimization of partial matches
     * @param query The AminoAcid query to search for
     * @param threshold The maximum score deviation (BLOSSUM62) allowed before a result is rejected
     * @return The best matching valid product corresponding to the query. null if no valid product found
     */
    public WormProtein search(List<AminoAcid> query, int threshold) {
        // Initialization
        // Position, deviation score, associated node
        PriorityQueue<SearchDataTuple> queryQueue = new PriorityQueue<>();
        // Start with position 0 and a deviation corresponding to our allowed threshold
        queryQueue.add(new SearchDataTuple(0, threshold, this.root));
        // Result to return when found (if any)
        WormProtein result = null;
        // A map of checked Deviation/Node pairs to prevent repeating identical node querying
        HashSet<Pair<Integer, ProteinTrieNode>> checkedSet = new HashSet<>();
        // The size of the query, cached for use later to break out early if need be
        int querySize = query.size();

        // Query the Trie until (at worst) the queue of possible values is empty
        while (!queryQueue.isEmpty()) {
            // Initialization
            SearchDataTuple currentData = queryQueue.poll();
            int currentPosition = currentData.getQueryPos();
            int currentDeviation = currentData.getDeviation();
            ProteinTrieNode currentNode = currentData.getNode();
            EnumMap<AminoAcid, ProteinTrieNode> nodeChildren = currentNode.getChildren();

            // Check to see if we have deviated too much; if so, finish early
            if (currentDeviation > threshold) {
                break;
            }

            // Update our "best result" if we find a node which has a product
            if (currentNode.getProduct() != null) {
                // Add in the mismatch penalty that would result given this match
                int delta = query.size() - currentPosition;
                int productDeviation = currentDeviation - (AminoAcid.BLOSSUM_GAP_PENALTY * delta);
                // Update the result and threshold if the product is valid
                if (productDeviation <= threshold) {
                    threshold = productDeviation;
                    result = currentNode.getProduct();
                }
                // If the delta is 0, we are done! There is no possible better alignment
                if (delta == 0) {
                    break;
                }
            }

            // Record that the results of this score and position have now been tested
            checkedSet.add(new Pair<>(currentPosition, currentNode));

            // Discover valid next steps and add them to the queue
            Set<AminoAcid> nodeKeys = nodeChildren.keySet();
            // If we haven't already iterated through the query in full yet...
            if (currentPosition < querySize) {
                AminoAcid queryAcid = query.get(currentPosition);
                // Add all possible next non-delete/insert steps to the search queue
                for (AminoAcid acid : nodeKeys) {
                    int tempDev = currentDeviation + AminoAcid.getBlossumDeviation(acid, queryAcid);
                    // Filter out any that would obviously be invalid
                    if (tempDev <= threshold) {
                        SearchDataTuple newVal = new SearchDataTuple(currentPosition+1, tempDev, nodeChildren.get(acid));
                        this.queueWithoutRedundancy(queryQueue, checkedSet, newVal);
                    }
                }
                // Add our insertion/deletion options, being always present
                int gapScore = currentDeviation - AminoAcid.BLOSSUM_GAP_PENALTY;
                if (gapScore <= threshold) {
                    // Insertion option (our position increases, but the node does not change)
                    SearchDataTuple newVal = new SearchDataTuple(currentPosition+1, gapScore, currentNode);
                    this.queueWithoutRedundancy(queryQueue, checkedSet, newVal);
                    // Deletion options (our node changes, but our position stays the same)
                    this.addDeletions(currentDeviation, currentPosition, currentNode, queryQueue, checkedSet);
                }
            }
            // When we have run through the queue, only add deletion options
            else {
                this.addDeletions(currentDeviation, currentPosition, currentNode, queryQueue, checkedSet);
            }
        }
        // Once the search is complete, return the best match (if any was found)
        return result;
    }

    // Helper function for adding delete-shifted queries to the search queue
    private void addDeletions(int deviation, int pos, ProteinTrieNode node,
                              PriorityQueue<SearchDataTuple> queue,
                              HashSet<Pair<Integer, ProteinTrieNode>> checkSet) {
        int newDev = deviation - AminoAcid.BLOSSUM_GAP_PENALTY;
        for (ProteinTrieNode child : node.getChildren().values()) {
            SearchDataTuple newVal = new SearchDataTuple(pos, newDev, child);
            this.queueWithoutRedundancy(queue, checkSet, newVal);
        }
    }

    // Helper function to reduce redundancy in above querying
    private void queueWithoutRedundancy(PriorityQueue<SearchDataTuple> queue,
                                        HashSet<Pair<Integer, ProteinTrieNode>> set,
                                        SearchDataTuple val) {
        Pair<Integer, ProteinTrieNode> test_pair = new Pair<>(val.getDeviation(), val.getNode());
        if (!set.contains(test_pair)) {
            queue.add(val);
        }
    }

    public void clear() {
        root.getChildren().clear();
    }

    @Override
    public String toString() {
        return this.root.prettyPrint();
    }
}
