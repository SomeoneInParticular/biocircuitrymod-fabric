/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib.search_trie;

import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import me.someoneinparticular.biocircuitry.proteomics.components.AminoAcid;

import java.util.Collection;
import java.util.EnumMap;
import java.util.Iterator;

/**
 * A Trie node used in the {@link ProteinTrie} class
 */
public class ProteinTrieNode {
    private final EnumMap<AminoAcid, ProteinTrieNode> children = new EnumMap<>(AminoAcid.class);
    private WormProtein product = null;

    EnumMap<AminoAcid, ProteinTrieNode> getChildren() {
        return children;
    }

    public WormProtein getProduct() {
        return product;
    }

    void setProduct(WormProtein product) {
        this.product = product;
    }

    /**
     * Print a tree representation of all children nodes, as if this were the root
     * @return The String representing the structure branching from this node
     */
    public String prettyPrint() {
        StringBuilder buffer = new StringBuilder();
        prettyPrintBuild(buffer, "", "");
        return buffer.toString();
    }

    // The actual "meat" of the code above, properly displaying the proteins in their relative positions
    private void prettyPrintBuild(StringBuilder buffer, String prefix, String suffix) {
        // Build the node's details, if any
        buffer.append(prefix);
        buffer.append(this.product == null ? "┐" : this.product.getLabel());
        buffer.append("\n");
        // Build the next set of branches for the tree, if this node has any children
        Collection<ProteinTrieNode> nodes = this.children.values();
        for (Iterator<ProteinTrieNode> iter = nodes.iterator(); iter.hasNext();) {
            ProteinTrieNode next = iter.next();
            if (iter.hasNext()) {
                next.prettyPrintBuild(buffer, suffix + "├── ", suffix + "│   ");
            } else {
                next.prettyPrintBuild(buffer, suffix + "└── ", suffix + "    ");
            }
        }
    }
}
