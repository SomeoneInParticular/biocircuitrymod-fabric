/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib.inventory;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.collection.DefaultedList;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Helper class which implements "allow only x" behaviour
 */
public class WhitelistInventory implements Inventory {

    private final DefaultedList<ItemStack> items;
    private final HashSet<Item> whitelist;

    public WhitelistInventory(int size, Item... whitelist) {
        // Initialize our list of tracked items
        this.items = DefaultedList.ofSize(size, ItemStack.EMPTY);
        // Initialize our list of allowed item types
        this.whitelist = new HashSet<>(Arrays.asList(whitelist));
    }

    @Override
    public int size() {
        return items.size();
    }

    public DefaultedList<ItemStack> getFullInv() {
        return this.items;
    }

    public HashSet<Item> getWhitelist() {
        return this.whitelist;
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack stack : items) {
            if (!stack.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return items.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        ItemStack stack = this.items.get(slot);
        ItemStack result = stack.split(amount);
        if (!result.isEmpty()) {
            markDirty();
        } else {
            return ItemStack.EMPTY;
        }
        return result;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack stack = this.items.get(slot);
        this.items.set(slot, ItemStack.EMPTY);
        markDirty();
        return stack;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        this.items.set(slot, stack.copy());
        markDirty();
    }

    @Override
    public void markDirty() {
        // Do Nothing Special
    }

    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        for (int i = 0; i < items.size(); i++) {
            this.items.set(i, ItemStack.EMPTY);
        }
    }

    public void fromTag(String key, CompoundTag tag) {
        CompoundTag relevantTag = tag.getCompound(key);
        ListTag listTag = relevantTag.getList("Items", 10);
        for(Tag stackTag : listTag) {
            int i = ((CompoundTag)stackTag).getByte("Slot") & 255;
            if (i < this.items.size()) {
                this.items.set(i, ItemStack.fromTag((CompoundTag)stackTag));
            }
        }
    }

    public CompoundTag toTag(String key, CompoundTag tag) {
        CompoundTag invTag = new CompoundTag();
        ListTag listTag = new ListTag();
        for (int i = 0; i < this.items.size(); i++) {
            CompoundTag compoundTag = new CompoundTag();
            compoundTag.putByte("Slot", (byte)i);
            ItemStack stack = this.items.get(i);
            stack.toTag(compoundTag);
            listTag.add(compoundTag);
        }
        if (!listTag.isEmpty()) {
            invTag.put("Items", listTag);
        }
        tag.put(key, invTag);
        return tag;
    }
}
