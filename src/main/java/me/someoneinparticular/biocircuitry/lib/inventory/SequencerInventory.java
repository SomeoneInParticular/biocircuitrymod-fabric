/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib.inventory;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.genomics.WormGenome;
import me.someoneinparticular.biocircuitry.items.ModItems;
import me.someoneinparticular.biocircuitry.items.SequencerItem;
import me.someoneinparticular.biocircuitry.proteomics.WormProteome;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.IntTag;
import net.minecraft.util.Identifier;

import java.util.ArrayList;

/**
 * Allows a genome (currently only for worms) to be sequenced after a short
 * delay (required to prevent lighting servers on fire).
 *
 * This inventory is intened to be used for sequencer ITEMS. Should a Sequencer
 * block be made in the future, it can use this inventory but should not
 * delegate its ticking functionality to FApi. Instead, it should be called
 * within the Block's BlockEntity::Tick function.
 */
public class SequencerInventory extends TickingInventory {
    // The progress of the sequencing task
    private int progress = 0;
    // The number of ticks required to complete a sequencing task
    public static final int MAX_PROGRESS = 40;

    // The inventory index for the genomic entity (for now, only a worm qualifies)
    public static final int WORM_IDX = 0;

    // The ItemStack this sequencer inventory corresponds too
    public final ItemStack sequencerStack;

    // A list of things to do when the sequencing process completes
    protected final ArrayList<Runnable> sequencingCompletionTasks;

    // Open an inventory with a specified sequencer item.
    public SequencerInventory(ItemStack sequencerStack) {
        // Initiate the 1-slot inventory (to-be-sequenced item)
        super(1);
        // Initiate the post-sequence task list
        sequencingCompletionTasks = new ArrayList<>();
        // Set the sequencer stack, if its valid
        if (sequencerStack == null || sequencerStack.isEmpty() || sequencerStack.getItem() != ModItems.SEQUENCER) {
            this.sequencerStack = null;
        } else {
            this.sequencerStack = sequencerStack;
        }
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public void addSequencingCompletionTask(Runnable task) {
        if (task != null) {
            sequencingCompletionTasks.add(task);
        }
    }

    // Update the sequencer's protein map with a new color value
    public void updateProteinColor(WormProtein protein, int color) {
        // Confirm the protein and color are both valid, terminating early otherwise
        if (protein == null) {
            BioCircuitry.LOGGER.warn("Tried to map a color to a null protein");
            return;
        }
        if (color > 0xFFFFFF || color < 0) {
            BioCircuitry.LOGGER.warn("Invalid color value. Must be between 0xFFFFFF and 0x0, inclusive");
            return;
        }

        // Confirm this inventory has a valid sequencer item to update
        if (sequencerStack != null && sequencerStack.getItem() == ModItems.SEQUENCER) {
            CompoundTag seqTag = sequencerStack.getOrCreateSubTag(SequencerItem.COLOR_MAP_KEY);
            Identifier id = WormProteinHandler.getId(protein);
            if (id == null) {
                BioCircuitry.LOGGER.warn("The protein we tried to color code is not registered!");
                return;
            }
            // Special case; the default color should just be deleted outright
            if (color == BioCircuitry.GENERIC_TOOLTIP_COLOR) {
                seqTag.remove(id.toString());
            }
            // Otherwise, just save the color as is
            else {
                seqTag.put(id.toString(), IntTag.of(color));
            }
            // Reset the sequencer's progress to account for the change
            progress = 0;
        }
    }

    @Override
    public void onOpen(PlayerEntity player) {
        // Begin the inventory ticking server side only
        if (!player.world.isClient) {
            enableTicking();
        }
        // Do any other opening operations
        super.onOpen(player);
    }

    @Override
    public void onClose(PlayerEntity player) {
        // Prevent this instance from ticking forever
        if (!player.world.isClient) {
            disableTicking();
        }
        // Do any other closing operations
        super.onClose(player);
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        // Reset the sequencer's progress when the stack is changed, period
        this.progress = 0;
        super.setStack(slot, stack);
    }

    protected void onSequencingComplete(ItemStack targetStack) {
        // Identify the proteome
        WormProteome proteome;
        if (WormProteome.hasProteome(targetStack.getTag())) {
            proteome = new WormProteome(targetStack);
        } else {
            WormGenome genome = new WormGenome(targetStack);
            proteome = new WormProteome(genome);
            proteome.toEntityNBT(targetStack);
        }
        // Update the proteome with a sequenced tooltip
        proteome.markSequenced(targetStack.getTag());
        proteome.applyColorMap(sequencerStack);
        // Save the data to NBT
        proteome.toEntityNBT(targetStack);

        // Run any custom actions passed to the inventory externally
        for (Runnable sequencingCompletionTask : sequencingCompletionTasks) {
            sequencingCompletionTask.run();
        }
    }

    // This inventory has an internal progress 'buffer', as the regex required
    // for parsing the proteome can be expensive is spammed. Thus, to prevent
    // trolling, a 1-2 second timer is in place
    @Override
    public void tick() {
        // Worm sequencing
        ItemStack targetStack = getStack(WORM_IDX);
        if (targetStack != null && targetStack.getItem() != Items.AIR) {
            // If we can still sequence the current stack, try to do so
            if (progress <= MAX_PROGRESS) {
                // If we've finished the cycle, do the sequencing operation
                if (progress == MAX_PROGRESS) {
                    onSequencingComplete(targetStack);
                }
                // If the entity has already been sequenced, it gets re-sequenced twice as fast
                if (WormProteome.isSequenced(targetStack.getTag())) {
                    progress++;
                }
                progress++;
            }
            // Otherwise, do nothing
        }
        // If the inventory is empty, reset the progress
        else {
            progress = 0;
        }
    }

    @Override
    public String toString() {
        return "SequencerInventory{" +
                "sequencerStack=" + sequencerStack +
                ", progress=" + progress +
                '}';
    }
}
