/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.lib.inventory;

import net.minecraft.inventory.SimpleInventory;
import net.minecraft.util.Tickable;

import java.util.HashSet;
import java.util.Set;

import static net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents.END_SERVER_TICK;

/**
 * Literally just an inventory that can tick alongside the server
 */
public abstract class TickingInventory extends SimpleInventory implements Tickable {

    public static Set<TickingInventory> TICKING_INVENTORIES = new HashSet<>();

    // Required to allow pass-through of the constructor, for convenience
    public TickingInventory(int size) {
        super(size);
    }

    static {
        END_SERVER_TICK.register(server -> TICKING_INVENTORIES.forEach(TickingInventory::tick));
    }

    public void enableTicking() {
        TICKING_INVENTORIES.add(this);
    }

    public void disableTicking() {
        TICKING_INVENTORIES.remove(this);
    }

    public abstract void tick();
}
