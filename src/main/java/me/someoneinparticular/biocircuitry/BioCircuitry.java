/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry;

import me.someoneinparticular.biocircuitry.block_entities.ModBlockEntityTypes;
import me.someoneinparticular.biocircuitry.blocks.ModBlocks;
import me.someoneinparticular.biocircuitry.data_handlers.WormEcologyHandler;
import me.someoneinparticular.biocircuitry.data_handlers.WormFoodNutrientHandler;
import me.someoneinparticular.biocircuitry.data_handlers.WormMetaboliteHandler;
import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.gui.ModScreenHandlers;
import me.someoneinparticular.biocircuitry.items.ModItems;
import me.someoneinparticular.biocircuitry.networking.ModNetworking;
import net.fabricmc.api.ModInitializer;
import net.minecraft.sound.SoundEvent;
import net.minecraft.text.Style;
import net.minecraft.text.TextColor;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ojalgo.OjAlgoUtils;
import org.ojalgo.matrix.Primitive64Matrix;
import org.ojalgo.random.Normal;
import org.ojalgo.random.Uniform;

import java.util.Random;

public class BioCircuitry implements ModInitializer {
    // -- Shared Resources -- //
    // The Mod's id label, used during translation
    public static final String MOD_ID = "biocircuitry";
    // Common shared logger
    public static final Logger LOGGER = LogManager.getLogger(MOD_ID);
    // Shared random instance, to reduce overhead of generating new ones on-the-fly
    public static final Random RANDOM = new Random();

    // The "default" styles and color for proteome tooltips
    public static final int GENERIC_TOOLTIP_COLOR = 0x999999;
    public static final Style GENERIC_TOOLTIP_STYLE = Style.EMPTY.withColor(TextColor.fromRgb(GENERIC_TOOLTIP_COLOR));

    @Override
    public void onInitialize() {
        LOGGER.info("Beginning BioCircuitry Initialization");
        // Initialize our metabolite registry first, as everything depends on it
        WormMetaboliteHandler.init();
        // Initialize the protein registry second, as it is most prone to failure
        WormProteinHandler.init();
        // Initialize the food nutrients next, being required for certain later initialization
        WormFoodNutrientHandler.init();
        // Finally, initialize worm populations, which relies on everything else
        WormEcologyHandler.init();
        // Initialize core structures of our mod
        ModItems.registerItems();
        ModBlocks.registerBlocks();
        // Initialize "Connecting Tissue" for our mods logic
        ModBlockEntityTypes.registerEntityTypes();
        ModScreenHandlers.initialize();
        // Initialize networking stuff
        ModNetworking.init();

        // Init whatever else needs to be initialized
        initMisc();
    }

    /**
     * Generate an Identifier for this specific mod
     */
    public static Identifier id(String... path) {
        return new Identifier(MOD_ID, String.join(".", path));
    }

    // Miscellaneous stuff that doesn't justify its own file (though it may in future)
    public static final Identifier STOB_GRUNT_ID = id("stob_grunt");
    public static final SoundEvent STOB_GRUNT_EVENT = new SoundEvent(STOB_GRUNT_ID);

    /**
     * Initialize any miscellaneous stuff required for the mod that doesn't fit
     * somewhere else. Currently includes OJAlgo initialization and testing, as
     * well as the initialization of our sole unique sound effect. If more sound
     * effects are a added, they will likely be moved to their own class instead.
     */
    private void initMisc() {
        // Force OJAlgo to initialize now, rather than hanging the game upon first use
        LOGGER.info("Initializing OJAlgo! This can take a bit...");
        Primitive64Matrix mtrxA = Primitive64Matrix.FACTORY.makeFilled(100, 100, Normal.standard());
        Primitive64Matrix mtrxB = Primitive64Matrix.FACTORY.makeFilled(100, 100, Uniform.standard());
        Runnable task = () -> mtrxA.multiply(mtrxB);
        task.run(); // Just some warm-up
        LOGGER.info("OJAlgo Initialized!");
        LOGGER.info("CPU cores used: " + OjAlgoUtils.ENVIRONMENT.cores);
        LOGGER.info("CPU threads used: " + OjAlgoUtils.ENVIRONMENT.threads);
        LOGGER.info("Overview of usable Resources: " + OjAlgoUtils.ENVIRONMENT);

        // Register the stob grunt, the only sound added by our mod
        Registry.register(Registry.SOUND_EVENT, STOB_GRUNT_ID, STOB_GRUNT_EVENT);
    }
}
