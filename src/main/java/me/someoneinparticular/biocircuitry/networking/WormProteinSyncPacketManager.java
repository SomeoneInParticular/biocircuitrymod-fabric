/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.networking;

import com.google.common.collect.ImmutableMap;
import io.netty.buffer.Unpooled;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.proteomics.components.AminoAcid;
import me.someoneinparticular.biocircuitry.proteomics.components.WormMetabolite;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.registry.Registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class WormProteinSyncPacketManager {
    public static final Identifier WORM_PROTEIN_RESET_ID = BioCircuitry.id("protein_reset");
    public static final Identifier WORM_PROTEIN_SYNC_ID = BioCircuitry.id("protein_sync");

    /**
     * Send protein synchronization packets to a specific player
     * @param player The player who needs to be updated
     */
    public static void sendTargetedWormProteinSyncS2CPackets(ServerPlayerEntity player) {
        for (WormProtein protein : WormProteinHandler.getAll()) {
            PacketByteBuf packedData = new PacketByteBuf(Unpooled.buffer());
            packedData.writeIdentifier(WormProteinHandler.getId(protein));
            writeProteinToBuffer(protein, packedData);
            ServerPlayNetworking.send(player, WORM_PROTEIN_SYNC_ID, packedData);
        }
    }

    /**
     * Receive an handle a ProteinSyncS2CPacket
     * @param client The client receiving the packet
     * @param handler The network handler for the client
     * @param buffer The packet's contents
     * @param responseSender Not used
     */
    @Environment(EnvType.CLIENT)
    public static void receiveWormProteinSyncS2CPacket(MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        Identifier id = buffer.readIdentifier();
        List<Object> unpackedContents = readProteinContentsFromBuffer(buffer);
        client.execute(() -> {
            String label = (String) unpackedContents.get(0);
            //noinspection unchecked
            ArrayList<AminoAcid> sequence = (ArrayList<AminoAcid>) unpackedContents.get(1);
            //noinspection unchecked
            HashMap<Identifier, Integer> reaction = (HashMap<Identifier, Integer>) unpackedContents.get(2);
            int biomass = (int) unpackedContents.get(3);
            int motility = (int) unpackedContents.get(4);
            //noinspection unchecked
            List<Pair<Item, Float>> products = (List<Pair<Item, Float>>) unpackedContents.get(5);
            WormProtein prot = WormProtein.build(label, sequence, reaction, products, biomass, motility);
            WormProteinHandler.register(id, prot);
        });
    }

    /**
     * Write the contents of a specific protein to the byte buffer
     * @param protein The protein to decompile
     * @param buffer The buffer to write too
     */
    protected static void writeProteinToBuffer(WormProtein protein, PacketByteBuf buffer) {
        // The protein's label
        buffer.writeString(protein.getLabel());
        // The protein's sequence
        List<AminoAcid> sequence = protein.getSequence();
        byte[] seqArr = new byte[sequence.size()];
        for (int i = 0; i < sequence.size(); i++) {
            seqArr[i] = (byte) sequence.get(i).ordinal();
        }
        buffer.writeByteArray(seqArr);
        // The protein's reactions
        ImmutableMap<WormMetabolite, Integer> reaction = protein.getReaction();
        buffer.writeInt(reaction.size());
        for (Map.Entry<WormMetabolite, Integer> reactant : reaction.entrySet()) {
            buffer.writeIdentifier(reactant.getKey().getId());
            buffer.writeInt(reactant.getValue());
        }
        // The protein's biomass and motility
        buffer.writeInt(protein.getBiomass());
        buffer.writeInt(protein.getMotility());
        // The proteins products (if any)
        List<Pair<Item, Float>> products = protein.getProducts();
        if (products == null) {
            buffer.writeInt(0);
        } else {
            buffer.writeInt(products.size());
            for (Pair<Item, Float> product : products) {
                buffer.writeIdentifier(Registry.ITEM.getId(product.getLeft()));
                buffer.writeFloat(product.getRight());
            }
        }
    }

    /**
     * Write the contents of a specific protein to the byte buffer
     * This needs to be done in the form it is to allow it to be passed from the
     * network stream to the client stream without issue.
     * @param buffer The buffer to read from
     */
    public static List<Object> readProteinContentsFromBuffer(PacketByteBuf buffer) {
        // Initialize the collection
        ArrayList<Object> decompiledContents = new ArrayList<>(6);
        // The protein's label
        decompiledContents.add(buffer.readString());
        // The protein's sequence
        byte[] seqArr = buffer.readByteArray();
        ArrayList<AminoAcid> sequence = new ArrayList<>(seqArr.length);
        for (byte b : seqArr) {
            sequence.add(AminoAcid.values()[b]);
        }
        decompiledContents.add(sequence);
        // The protein's reactions
        int reactantCount = buffer.readInt();
        HashMap<Identifier, Integer> reaction = new HashMap<>();
        for (int i = 0; i < reactantCount; i++) {
            Identifier metabId = buffer.readIdentifier();
            int count = buffer.readInt();
            reaction.put(metabId, count);
        }
        decompiledContents.add(reaction);
        // The protein's biomass and motility
        decompiledContents.add(buffer.readInt());
        decompiledContents.add(buffer.readInt());
        // The proteins products (if any)
        int productCount = buffer.readInt();
        List<Pair<Item, Float>> products = null;
        if (productCount != 0) {
            products = new ArrayList<>(productCount);
            for (int i = 0; i < productCount; i++) {
                Identifier itemId = buffer.readIdentifier();
                float amount = buffer.readFloat();
                products.add(new Pair<>(Registry.ITEM.get(itemId), amount));
            }
        }
        decompiledContents.add(products);
        return decompiledContents;
    }

    /**
     * Send a packet requesting to reset the protein registry (I.E. synchronization init)
     */
    public static void sendTargetedWormProteinResetS2CPackets(ServerPlayerEntity player) {
        ServerPlayNetworking.send(player, WORM_PROTEIN_RESET_ID, new PacketByteBuf(Unpooled.EMPTY_BUFFER));
    }

    /**
     * Receive a packet requesting to reset the protein registry (requesting the client to reset it)
     */
    @Environment(EnvType.CLIENT)
    public static void receiveWormProteinResetS2CPacket(MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        client.execute(WormProteinHandler::reset);
    }

    // TODO: Add other synchronization code

    /**
     * Helper function to send all packets for a full protein registry
     * synchronization, in their correct order.
     */
    public static void synchronizeProteins(ServerPlayerEntity player) {
        BioCircuitry.LOGGER.info("Sending protein synchronization packets...");
        sendTargetedWormProteinResetS2CPackets(player);
        sendTargetedWormProteinSyncS2CPackets(player);
        BioCircuitry.LOGGER.info("Protein synchronization packets sent!");
    }

    public static void registerClient() {
        // Receiver for worm protein reset packets
        ClientPlayNetworking.registerGlobalReceiver(
                WORM_PROTEIN_RESET_ID, WormProteinSyncPacketManager::receiveWormProteinResetS2CPacket
        );
        // Receiver for worm protein sync packets
        ClientPlayNetworking.registerGlobalReceiver(
                WORM_PROTEIN_SYNC_ID, WormProteinSyncPacketManager::receiveWormProteinSyncS2CPacket
        );
    }
}
