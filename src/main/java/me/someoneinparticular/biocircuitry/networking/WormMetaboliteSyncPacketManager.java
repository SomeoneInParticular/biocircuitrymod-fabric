/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.networking;

import io.netty.buffer.Unpooled;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormMetaboliteHandler;
import me.someoneinparticular.biocircuitry.proteomics.components.WormMetabolite;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public abstract class WormMetaboliteSyncPacketManager {
    public static final Identifier WORM_METABOLITE_RESET_ID = BioCircuitry.id("metabolite_reset");
    public static final Identifier WORM_METABOLITE_SYNC_ID = BioCircuitry.id("metabolite_sync");

    /**
     * Send a metabolite synchronization packets to a specific player
     * @param player The player who needs to be updated
     */
    public static void sendTargetedWormMetaboliteSyncS2CPackets(ServerPlayerEntity player) {
        for (WormMetabolite metab : WormMetaboliteHandler.getAll()) {
            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
            buf.writeIdentifier(metab.getId());
            ServerPlayNetworking.send(player, WORM_METABOLITE_SYNC_ID, buf);
        }
    }

    /**
     * Receive an handle a MetaboliteSyncS2CPacket
     * @param client The client receiving the packet
     * @param handler The network handler for the client
     * @param buffer The packet's contents
     * @param responseSender Not used
     */
    @Environment(EnvType.CLIENT)
    public static void receiveWormMetaboliteSyncS2CPacket(MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        Identifier metabId = buffer.readIdentifier();
        client.execute(() -> WormMetaboliteHandler.register(metabId));
    }

    /**
     * Send a packet requesting to reset the metabolite registry (I.E. synchronization init)
     */
    public static void sendTargetedWormMetaboliteResetS2CPacket(ServerPlayerEntity player) {
        ServerPlayNetworking.send(player, WORM_METABOLITE_RESET_ID, new PacketByteBuf(Unpooled.EMPTY_BUFFER));
    }

    /**
     * Receive a packet requesting to reset the protein registry (requesting the client to reset it)
     */
    @Environment(EnvType.CLIENT)
    public static void receiveWormMetaboliteResetS2CPacket(MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        client.execute(WormMetaboliteHandler::reset);
    }

    /**
     * Helper function to send all packets for a full metabolite registry
     * synchronization, in their correct order.
     */
    public static void synchronizeMetabolites(ServerPlayerEntity player) {
        BioCircuitry.LOGGER.info("Sending metabolite synchronization packets...");
        sendTargetedWormMetaboliteResetS2CPacket(player);
        sendTargetedWormMetaboliteSyncS2CPackets(player);
        BioCircuitry.LOGGER.info("Metabolite synchronization packets sent!");
    }

    public static void registerClient() {
        // Receiver for worm metabolite reset packets
        ClientPlayNetworking.registerGlobalReceiver(
                WORM_METABOLITE_RESET_ID, WormMetaboliteSyncPacketManager::receiveWormMetaboliteResetS2CPacket
        );
        // Receiver for worm metabolite sync packets
        ClientPlayNetworking.registerGlobalReceiver(
                WORM_METABOLITE_SYNC_ID, WormMetaboliteSyncPacketManager::receiveWormMetaboliteSyncS2CPacket
        );
    }
}
