/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.networking;

import io.netty.buffer.Unpooled;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.gui.descriptions.SequencerGuiDescription;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public abstract class SequencerPacketManager {
    public static final Identifier COLOR_UPDATE_ID = BioCircuitry.id("color_update");
    public static final Identifier SEQUENCING_COMPLETE_ID = BioCircuitry.id("sequencing_complete");

    /**
     * Send a color update packet for when a protein's color code is changed
     * @param protID The protein ID being modified
     * @param color The new color being set
     * @param syncID The synchronization ID keeping client and server in-line
     */
    public static void sendColorUpdateC2SPacket(Identifier protID, int color, int syncID) {
        PacketByteBuf packedData = new PacketByteBuf(Unpooled.buffer());
        packedData.writeInt(syncID);
        packedData.writeIdentifier(protID);
        packedData.writeInt(color);
        ClientPlayNetworking.send(COLOR_UPDATE_ID, packedData);
    }

    /**
     * Receive and process a color update packet, used when the client had
     * requested that a protein's color map be changed
     * @param server The server which should run the code
     * @param player The player who sent the packet
     * @param handler The network handler for the server side
     * @param buffer The contents of the packet, in byte-buffered form
     * @param responseSender The packet sender to send responses to if need be
     */
    public static void receiveColorUpdateC2SPacket(MinecraftServer server, ServerPlayerEntity player, ServerPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        // Unpack the data
        int syncID = buffer.readInt();
        Identifier protID = buffer.readIdentifier();
        int color = buffer.readInt();
        // Run the associated task
        server.execute(() -> {
            // Get the targeted inventory
            SequencerGuiDescription activeGui = SequencerGuiDescription.activeInstances.get(syncID);
            if (activeGui != null) {
                // Get the information for the targeted protein
                WormProtein protein = WormProteinHandler.get(protID);
                if (protein != null) {
                    // Confirm the color is valid
                    if (0xFFFFFF >= color && 0 <= color) {
                        // Finally, save the color
                        activeGui.updateProteinMappedColor(protein, color);
                    }
                }
            }
        });
    }

    /**
     * Send a sequencing completion packet when a sequencer item completes its
     * process
     * @param playerEntity The player using the GUI
     * @param newStack The now-sequenced worm item that should appear to the client
     */
    public static void sendSequencingCompleteS2CPacket(ServerPlayerEntity playerEntity, ItemStack newStack) {
        PacketByteBuf packedData = new PacketByteBuf(Unpooled.buffer());
        packedData.writeItemStack(newStack);
        ServerPlayNetworking.send(playerEntity, SEQUENCING_COMPLETE_ID, packedData);
    }

    /**
     * Receive and process a sequence completion packet, used when a sequencer
     * server-side has finished running post-sequencing processes
     * @param client The client instance which should run the code
     * @param handler The network handler for the server side
     * @param buffer The contents of the packet, in byte-buffered form
     * @param responseSender The packet sender to send responses to if need be
     */
    @Environment(EnvType.CLIENT)
    public static void receiveSequencingCompleteS2CPacket(MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        ClientPlayerEntity player = client.player;
        if (player == null) {
            BioCircuitry.LOGGER.warn("Useless SequencingComplete packet received; no player exists to receive it!");
            return;
        }
        ScreenHandler activeGui = player.currentScreenHandler;
        // If the player is somehow in a different screen type than this packet is for, terminate
        if (!(activeGui instanceof SequencerGuiDescription)) {
            BioCircuitry.LOGGER.warn("Useless SequencingComplete packet received; GUI instance was not correct type (SequencerGuiDescription)!");
            return;
        }
        // Null/empty stack handling is done by the handler
        ItemStack stack = buffer.readItemStack();
        client.execute(() -> ((SequencerGuiDescription) activeGui).updateResults(stack));
    }

    public static void register() {
        // Server side packet management
        ServerPlayNetworking.registerGlobalReceiver(
                COLOR_UPDATE_ID, SequencerPacketManager::receiveColorUpdateC2SPacket
        );
    }

    public static void registerClient() {
        // Client side packet management
        ClientPlayNetworking.registerGlobalReceiver(
                SEQUENCING_COMPLETE_ID, SequencerPacketManager::receiveSequencingCompleteS2CPacket
        );
    }

}
