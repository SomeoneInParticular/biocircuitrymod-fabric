/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.networking;

import io.netty.buffer.Unpooled;
import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormFoodNutrientHandler;
import me.someoneinparticular.biocircuitry.gui.descriptions.ChromatographGuiDescription;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.Map;

public abstract class ChromatographPacketManager {
    public static final Identifier FOOD_VALUE_REQUEST_ID = BioCircuitry.id("food_value_request");
    public static final Identifier FOOD_VALUE_RESPONSE_ID = BioCircuitry.id("food_value_response");

    /**
     * Request the food nutrients for a given item
     * @param item The food item request
     * @param syncID The synchronization id for the GUI requesting it
     */
    @Environment(EnvType.CLIENT)
    public static void sendFoodValueRequestC2SPacket(Item item, int syncID) {
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        buf.writeInt(syncID);
        buf.writeIdentifier(Registry.ITEM.getId(item));
        ClientPlayNetworking.send(FOOD_VALUE_REQUEST_ID, buf);
    }

    /**
     * Receive a food value request from a client, and respond with the results
     * @param server The server receiving the request
     * @param player The server-side equivalent to the player sending the request
     * @param handler The handler that will manage our response
     * @param buffer The contents of the request
     * @param responseSender Not used
     */
    public static void receiveFoodValueRequestC2SPacket(MinecraftServer server, ServerPlayerEntity player, ServerPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        int syncID = buffer.readInt();
        Identifier id = buffer.readIdentifier();
        server.execute(() -> {
            WormFoodNutrientHandler.FoodValue value = WormFoodNutrientHandler.get(id);
            sendFoodValueResponseS2CPacket(player, value, syncID);
        });
    }

    /**
     * Send the food nutrients for a given item back to the user
     * @param value The food values corresponding to the requested item
     * @param syncID The synchronization id for the GUI requesting it
     */
    public static void sendFoodValueResponseS2CPacket(ServerPlayerEntity player, WormFoodNutrientHandler.FoodValue value, int syncID) {
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        // Pull out the sync id
        buf.writeInt(syncID);
        // Write the nutrients into the packet
        if (value != null && value.getNutrients() != null) {
            HashMap<Identifier, Float> nutrients = value.getNutrients();
            buf.writeInt(nutrients.size());
            for (Map.Entry<Identifier, Float> entry : nutrients.entrySet()) {
                buf.writeIdentifier(entry.getKey());
                buf.writeFloat(entry.getValue());
            }
        } else {
            buf.writeInt(0);
        }
        ServerPlayNetworking.send(player, FOOD_VALUE_RESPONSE_ID, buf);
    }

    /**
     * Receive a packet containing the contents of the food value request
     * @param client The client who originally requested the packet
     * @param handler The client's network handler
     * @param buffer The packet's contents
     * @param responseSender Not used
     */
    @Environment(EnvType.CLIENT)
    public static void receiveFoodValueResponseS2CPacket(MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {
        // Unpack the packet's contents
        int syncID = buffer.readInt();
        int size = buffer.readInt();
        final HashMap<Identifier, Float> nutrients = new HashMap<>(size);
        if (size != 0) {
            for (int i = 0; i < size; i++) {
                Identifier id = buffer.readIdentifier();
                float amount = buffer.readFloat();
                nutrients.put(id, amount);
            }
        }
        client.execute(() -> {
            // Check that the GUI we want to update still exists and is valid
            ClientPlayerEntity player = client.player;
            if (player == null) {
                return;
            }
            ScreenHandler gui = player.currentScreenHandler;
            if (!(gui instanceof ChromatographGuiDescription)) {
                BioCircuitry.LOGGER.warn("Tried to receive Food Value packet, but the player's GUI is not the correct type: " + player.currentScreenHandler.getClass());
                BioCircuitry.LOGGER.warn("(This could be the result of the player closing the GUI before the packet was recieved)");
                return;
            }
            if (gui.syncId != syncID) {
                BioCircuitry.LOGGER.warn("Tried to receive Food Value packet, but the GUI it was for isn't present for player " + player.getName());
                BioCircuitry.LOGGER.warn("(This could be because the player opened a new GUI before receiving a packet requested by an old one)");
                return;
            }
            // Update the GUI
            ((ChromatographGuiDescription) gui).receiveNutrientInfo(nutrients);
        });
    }

    public static void register() {
        ServerPlayNetworking.registerGlobalReceiver(
                FOOD_VALUE_REQUEST_ID, ChromatographPacketManager::receiveFoodValueRequestC2SPacket
        );
    }

    @Environment(EnvType.CLIENT)
    public static void registerClient() {
        // Receiver for food value responses
        ClientPlayNetworking.registerGlobalReceiver(
                FOOD_VALUE_RESPONSE_ID, ChromatographPacketManager::receiveFoodValueResponseS2CPacket
        );
    }
}
