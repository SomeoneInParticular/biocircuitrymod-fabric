/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.networking;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

public class ModNetworking {
    public static void init() {
        SequencerPacketManager.register();
        ChromatographPacketManager.register();
    }

    @Environment(EnvType.CLIENT)
    public static void initClient() {
        SequencerPacketManager.registerClient();
        ChromatographPacketManager.registerClient();
        WormMetaboliteSyncPacketManager.registerClient();
        WormProteinSyncPacketManager.registerClient();
    }
}
