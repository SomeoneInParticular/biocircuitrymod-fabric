/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.blocks;

import me.someoneinparticular.biocircuitry.BioCircuitry;

public class ModBlocks {
    public static final BroodBinBlock BROOD_BIN = new BroodBinBlock();
    public static final FeedBinBlock FEED_BIN = new FeedBinBlock();
    public static final StobBlock STOB = new StobBlock();

    public static void registerBlocks() {
        BioCircuitry.LOGGER.info("Initializing blocks");
        BROOD_BIN.register();
        FEED_BIN.register();
        STOB.register();
    }
}
