/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.blocks;

import me.someoneinparticular.biocircuitry.block_entities.FeedBinEntity;
import me.someoneinparticular.biocircuitry.lib.BioCircBlock;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class FeedBinBlock extends BioCircBlock implements BlockEntityProvider {
    public FeedBinBlock() {
        super(FabricBlockSettings
                .of(Material.WOOD, MaterialColor.BROWN)
                .strength(2.0F, 3.0F)
                .sounds(BlockSoundGroup.WOOD)
                .breakByHand(true),
                "feed_bin");
    }

    @Override
    public BlockEntity createBlockEntity(BlockView view) {
        return new FeedBinEntity();
    }

    @SuppressWarnings("deprecation")
    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        NamedScreenHandlerFactory factory = state.createScreenHandlerFactory(world, pos);

        if (factory != null) {
            player.openHandledScreen(factory);
        }

        return ActionResult.SUCCESS;
    }

    @Override
    @SuppressWarnings("deprecation")
    public ExtendedScreenHandlerFactory createScreenHandlerFactory(BlockState state, World world, BlockPos pos) {
        return new ExtendedScreenHandlerFactory() {
            @Override
            public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
                buf.writeBlockPos(pos);
            }

            @Override
            public Text getDisplayName() {
                BlockEntity entity = world.getBlockEntity(pos);
                if (entity == null) return null;
                return new TranslatableText(entity.getCachedState().getBlock().getTranslationKey());
            }

            @Override
            public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
                BlockEntity entity = world.getBlockEntity(pos);
                if (entity != null) {
                    return ((FeedBinEntity)entity).createMenu(syncId, inv, player);
                } else {
                    return null;
                }
            }
        };
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        // Run what needs to be run
        super.onBreak(world, pos, state, player);
        // Drop the items contained within this block's attached entity
        BlockEntity entity = world.getBlockEntity(pos);
        if (entity instanceof Inventory) {
            ItemScatterer.spawn(world, pos, (Inventory)entity);
        }
    }
}
