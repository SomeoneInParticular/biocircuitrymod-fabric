/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.api.proteomics;

import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

/**
 * Simple helper to keep track of which metabolites exist
 */
public interface IMetabolite {
    /**
     * Get a raw identifier directly from the metabolite for easy access
     */
    Identifier getId();

    /**
     * Get a translatable text component for use in the various GUIs
     */
    TranslatableText getTranslatableText();
}
