/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.api.proteomics;

import net.minecraft.item.Item;
import net.minecraft.util.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * A protein which mediates some reaction within metabolic processes
 * @param <T> The sequence type of the protein; recommended to be an Enum
 * @param <M> The metabolite type this protein's reaction's tranform between
 */
public interface IProtein<
        T extends Comparable<T>,
        M extends IMetabolite> {
    /**
     * Get the sequence of this protein, for referencing purpose
     */

    Collection<T> getSequence();

    /**
     * Get the reaction this protein mediates, in {'metab': 'delta'} form,
     * (negative delta implies a cost, positive a surplus)
     */
    Map<M, Integer> getReaction();

    /**
     * Get the biomass this protein contributes while fully active, if any.
     * Acts to control dominance hierarchies as well (higher biomass -> greater
     * dominance)
     */
    Integer getBiomass();

    /**
     * Get the motility the activity of this protein contributes to its entity.
     * Generally representative of how quickly an entity can move/eat/digest
     * foods fed too it, though different mods may treat this differently.
     */
    int getMotility();

    /**
     * Get the ItemStacks created while this protein is active at full power,
     * if any exist
     */
    List<Pair<Item, Float>> getProducts();
}
