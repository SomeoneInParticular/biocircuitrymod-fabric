/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.api.proteomics;

import me.someoneinparticular.biocircuitry.genomics.WormGenome;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Pair;

import java.util.List;

/**
 * Manages the proteomes for a given in-game item object type. You should
 * subclass this if you want to have new types of objects contain proteomes
 * <br>
 * Mediates the determination of what proteins (and thus, what functions) exist
 * within a given genome. Also the entry point for determining the results of
 * an entity with a genome being fed under certain circumstances, namely what
 * said entity can eat and what it produces as a result of eating it.
 *
 * Refer to {@linkplain WormGenome WormGenome} for an
 * example of this.
 *
 * @param <T> The object type this Proteome should be applied too
 * @param <P> The protein type this Proteome uses to build metabolic pathways
 */
public interface IProteome<T, P extends IProtein<?, ?>> {
    /**
     * Read the proteome from a targeted object's NBT
     * @param target Target who's NBT should be read and used
     */
    void fromEntityNBT(T target);

    /**
     * Save the proteome onto the targeted object, saved for later access
     * @param target Target to update with this Genome's attributes
     */
    void toEntityNBT(T target);

    /**
     * Get the proteins associated with this proteome
     */
    List<P> getAllProteins();
}
