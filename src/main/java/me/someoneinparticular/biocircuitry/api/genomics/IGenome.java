/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.api.genomics;

import me.someoneinparticular.biocircuitry.genomics.WormGenome;

/**
 * Manages the genomes for a given in-game item object type. You should
 * subclass this if you want to have new types of objects contain
 * genomes
 * <br>
 * Mediates the creation of new genomes during breeding/cloning as well as
 * the reading/writing of said genome to NBT format.
 *
 * Refer to {@linkplain WormGenome WormGenome} for an
 * example of this.
 *
 * @param <T> The object type this Genome should be applied too
 */
public interface IGenome<T> {
    // -- Required Functions --- //
    /**
     * Generate a Gamete's Genome, to be combined with another during breeding!
     * In asexual reproduction, this could be the genome itself
     * In sexual reproduction, this should be only a part of the genome
     *
     * @return The Gamete Genome, representing this genome's contribution to the child
     */
    IGenome<T> buildGamete();

    /**
     * Read the genome from a targeted object's NBT
     * @param target Target who's NBT should be read and used
     */
    void fromEntityNBT(T target);

    /**
     * Save the genome onto the targeted object, saved for later access
     * @param target Target to update with this Genome's attributes
     */
    void toEntityNBT(T target);
}
