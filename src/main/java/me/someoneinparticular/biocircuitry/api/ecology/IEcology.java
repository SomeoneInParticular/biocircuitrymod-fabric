/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.api.ecology;

import me.someoneinparticular.biocircuitry.api.genomics.IGenome;

/**
 * Represents a given ecologies's meta-genome. Used to determine the genome of
 * individuals that can be found within a given area, i.e. forest worms,
 * wild cows, desert rabbits etc.
 *
 * @param <G> The genome type this ecology is associated with
 */
public interface IEcology<G extends IGenome<?>> {
    /**
     * Build a genome representing a random individual from this population
     *
     * @return A genome which can be attached to an individual to represent
     * said individual being from this population.
     */
    G generateGenome();
}
