/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.items;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.lib.BioCircItem;
import net.minecraft.item.Item;

public class ModItems {
    public static final BioCircItem WORM = new WormItem(new Item.Settings());
    public static final BioCircItem ROOPING_IRON = new RoopingIronItem(new Item.Settings());
    public static final BioCircItem SEQUENCER = new SequencerItem(new Item.Settings());
    public static final BioCircItem CHROMATOGRAPH = new ChromatographItem(new Item.Settings());

    public static void registerItems() {
        BioCircuitry.LOGGER.info("Initializing items");
        WORM.register();
        ROOPING_IRON.register();
        SEQUENCER.register();
        CHROMATOGRAPH.register();
    }
}
