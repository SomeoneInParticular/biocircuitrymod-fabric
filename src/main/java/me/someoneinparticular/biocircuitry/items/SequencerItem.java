/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.items;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.api.BioCircItemGroups;
import me.someoneinparticular.biocircuitry.gui.descriptions.SequencerGuiDescription;
import me.someoneinparticular.biocircuitry.lib.BioCircItem;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

import java.util.List;

public class SequencerItem extends BioCircItem {
    // The Sequencer compound tag key
    public final static String COLOR_MAP_KEY = "color_map";

    public SequencerItem(Settings settings) {
        super(settings.group(BioCircItemGroups.GENERAL).maxCount(1), "sequencer");
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        // Get the NBT tag of the sequencer stack
        CompoundTag tag = stack.getTag();
        // If it lacks an NBT tag, or lacks a color map key tag, add the tooltip stating it is not configured
        if (tag == null || !tag.contains(COLOR_MAP_KEY)) {
            tooltip.add(new TranslatableText(String.format("tooltip.%s.not_configured", BioCircuitry.MOD_ID)).
                    setStyle(BioCircuitry.GENERIC_TOOLTIP_STYLE));
        }
        // Otherwise, mark it as configured
        else {
            tooltip.add(new TranslatableText(String.format("tooltip.%s.configured", BioCircuitry.MOD_ID)).
                    setStyle(BioCircuitry.GENERIC_TOOLTIP_STYLE));
        }
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        user.openHandledScreen(createHandlerFactory(stack));
        return TypedActionResult.success(stack);
    }

    private ExtendedScreenHandlerFactory createHandlerFactory(ItemStack stack) {
        return new ExtendedScreenHandlerFactory() {
            @Override
            public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
                buf.writeItemStack(stack);
            }

            @Override
            public Text getDisplayName() {
                return stack.getName();
            }

            @Override
            public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
                return new SequencerGuiDescription(syncId, inv, stack);
            }
        };
    }
}
