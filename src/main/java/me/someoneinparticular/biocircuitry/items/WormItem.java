/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.items;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.lib.BioCircItem;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.text.*;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;

import java.util.List;

import static me.someoneinparticular.biocircuitry.BioCircuitry.GENERIC_TOOLTIP_STYLE;
import static me.someoneinparticular.biocircuitry.proteomics.WormProteome.*;

public class WormItem extends BioCircItem {
    public WormItem(Settings settings) {
        // Create the worm object itself
        super(settings.maxCount(1), "worm");
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        BioCircuitry.LOGGER.info("Worm's Genome:" + user.getStackInHand(hand).getTag());
        return super.use(world, user, hand);
    }

    @Override
    public void appendStacks(ItemGroup group, DefaultedList<ItemStack> stacks) {
        // Call the superclass in case something needs to be done there
        super.appendStacks(group, stacks);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        // Get the NBT tag of the worm stack
        CompoundTag tag = stack.getTag();
        // If it has no tag, somethings broke, report as such
        if (tag == null) {
            tooltip.add(new TranslatableText(
                    String.format("tooltip.%s.no_nbt", BioCircuitry.MOD_ID))
                    .setStyle(GENERIC_TOOLTIP_STYLE));
        }
        // If the sequenced tag reports false (or does not exist), report the "not sequenced" tooltip
        else if (!tag.getBoolean(SEQUENCED_KEY)) {
            tooltip.add(new TranslatableText(
                    String.format("tooltip.%s.not_sequenced", BioCircuitry.MOD_ID))
                    .setStyle(GENERIC_TOOLTIP_STYLE));
        }
        // Begin formatting the tooltip for each chromosome
        else {
            ListTag chromListTag = tag.getList(PROTEINS_KEY, 9);
            CompoundTag colorMapTag = tag.getCompound(COLOR_MAP_KEY);
            for (Tag chromTag : chromListTag) {
                // Initialize this strings text component
                MutableText chromTip = new LiteralText("{").setStyle(GENERIC_TOOLTIP_STYLE);
                // Color code each protein one by one, if they have been registered
                for (Tag protTag : ((ListTag)chromTag)) {
                    WormProtein prot = WormProteinHandler.get(new Identifier(protTag.asString()));
                    if (prot != null) {
                        // Build the styled text, using the color map if it exists
                        String label = prot.getLabel();
                        Style colorStyle = GENERIC_TOOLTIP_STYLE;
                        if (colorMapTag != null) {
                            int color = colorMapTag.getInt(protTag.asString());
                            if (color != 0) {
                                colorStyle = Style.EMPTY.withColor(TextColor.fromRgb(color));
                            }
                        }
                        chromTip.append(new LiteralText(label).setStyle(colorStyle));
                        chromTip.append(new LiteralText(", "));
                    }
                }
                // Finalize the tooltip, replacing the last comma with a "}"
                if (chromTip.getSiblings().size() > 0) {
                    chromTip.getSiblings().set(chromTip.getSiblings().size()-1, new LiteralText("}"));
                }
                // Rarely a chromosome with 0 genes will appear, in which case the tooltip is just "{}"
                else {
                    chromTip.append(new LiteralText("}"));
                }

                tooltip.add(chromTip);
            }
            super.appendTooltip(stack, world, tooltip, context);
        }
    }
}
