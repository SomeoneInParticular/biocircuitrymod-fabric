/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.items;

import me.someoneinparticular.biocircuitry.api.BioCircItemGroups;
import me.someoneinparticular.biocircuitry.gui.descriptions.ChromatographGuiDescription;
import me.someoneinparticular.biocircuitry.lib.BioCircItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.SimpleNamedScreenHandlerFactory;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class ChromatographItem extends BioCircItem {
    public ChromatographItem(Settings settings) {
        super(settings.group(BioCircItemGroups.GENERAL).maxCount(1), "chromatograph");
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        user.openHandledScreen(createHandlerFactory(user.getStackInHand(hand)));
        return TypedActionResult.success(stack);
    }

    private NamedScreenHandlerFactory createHandlerFactory(ItemStack stack) {
        return new SimpleNamedScreenHandlerFactory((syncId, inv, player) ->
                new ChromatographGuiDescription(syncId, inv),
                stack.getName());
    }
}
