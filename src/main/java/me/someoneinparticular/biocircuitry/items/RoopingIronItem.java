/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.items;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.api.BioCircItemGroups;
import me.someoneinparticular.biocircuitry.blocks.ModBlocks;
import me.someoneinparticular.biocircuitry.data_handlers.WormEcologyHandler;
import me.someoneinparticular.biocircuitry.ecology.WormEcology;
import me.someoneinparticular.biocircuitry.genomics.WormGenome;
import me.someoneinparticular.biocircuitry.lib.BioCircItem;
import me.someoneinparticular.biocircuitry.proteomics.WormProteome;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import java.util.Collection;
import java.util.List;

public class RoopingIronItem extends BioCircItem {
    public RoopingIronItem(Settings settings) {
        super(settings.maxDamage(128).group(BioCircItemGroups.GENERAL), "rooping_iron");
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        // Only run this server-side
        World world = context.getWorld();
        if (!world.isClient()) {
            BlockPos pos = context.getBlockPos();
            // Only attempt to spawn worms when a Stob is selected
            if (world.getBlockState(pos).getBlock() == ModBlocks.STOB) {
                // Play the stob-grunt sound
                world.playSound(null, pos, BioCircuitry.STOB_GRUNT_EVENT, SoundCategory.AMBIENT, 0.7f, BioCircuitry.RANDOM.nextFloat()*0.5f+0.75f);

                // Initialize the result tracker
                Pair<Block, BlockPos> results;

                // Check if bait items were provided
                List<ItemEntity> baitItems = world.getEntitiesByClass(
                        ItemEntity.class, new Box(pos.add(2, 2, 2), pos.add(-2, -2, -2)), null);

                // If none were provided, select a random x and y position
                Item bait = null;
                if (baitItems.isEmpty()) {
                    // Anywhere within a 3x3 range of the Stob
                    pos = pos.add(RANDOM.nextInt(5)-2, 0, RANDOM.nextInt(5)-2);
                    results = findValidBlock(world, pos);
                    // If the choice was invalid, return having done nothing
                    if (results == null) {
                        BioCircuitry.LOGGER.error("FAILED ROOPING IRON USE DUE TO NULL RESULT");
                        return ActionResult.SUCCESS;
                    }
                }
                // Otherwise, spawn the worm at the position of the bait
                else {
                    ItemEntity baitEntity = baitItems.get(RANDOM.nextInt(baitItems.size()));
                    bait = baitEntity.getStack().getItem();
                    Vec3d baitPos = baitEntity.getPos();
                    results = findValidBlock(world, new BlockPos(baitPos));
                    // Update our query values
                    if (results == null)  {
                        return ActionResult.SUCCESS;
                    }
                }
                // Initialize our search query parameters
                Biome biome = world.getBiome(results.getRight());
                int height = results.getRight().getY();
                float temp = biome.getTemperature();
                Block spawnBlock = results.getLeft();
                // Get the worm population to generate a genome for
                Collection<WormEcology> ecoOptions = WormEcologyHandler.query(temp, height, biome, bait, spawnBlock);
                // If no valid populations exist for this scenario, return early
                if (ecoOptions.isEmpty()) {
                    return ActionResult.SUCCESS;
                }
                // Initialize the worm population that will generate a new worm
                WormEcology ecology = (WormEcology) ecoOptions.toArray()[RANDOM.nextInt(ecoOptions.size())];
                // Generate a worm and its genome
                ItemStack wormStack = new ItemStack(ModItems.WORM);
                WormGenome genome = ecology.generateGenome();
                WormProteome proteome = new WormProteome(genome);
                genome.toEntityNBT(wormStack);
                proteome.toEntityNBT(wormStack);
                // Spawn the worm
                ItemEntity wormEntity = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), wormStack);
                wormEntity.setToDefaultPickupDelay();
                world.spawnEntity(wormEntity);
                world.playSound(null, pos, SoundEvents.BLOCK_WET_GRASS_STEP, SoundCategory.AMBIENT, 0.7f, 0.7f);
                // Decrement this item's durability if not in creative mode
                if (context.getPlayer() != null && !context.getPlayer().isCreative()) {
                    context.getStack().damage(1, RANDOM, (ServerPlayerEntity) context.getPlayer());
                    context.getPlayer().sendToolBreakStatus(context.getHand());
                }
                // Initiate a short cooldown to prevent spam
                context.getPlayer().getItemCooldownManager().set(this, 10);
                // Return a successful use
                return ActionResult.SUCCESS;
            }
        }
        return ActionResult.PASS;
    }

    /**
     * Helper function to find the top-most valid position for a worm spawn (if any)
     * @param world World to search in
     * @param initPos Initial position from which to begin the search
     * @return A pair of the root block for the valid position, and said valid position.
     *  Null if no valid position was found.
     */
    private Pair<Block, BlockPos> findValidBlock(World world, BlockPos initPos) {
        // Search from top down along the column
        for (int i = 2; i > -3; i--) {
            BlockView view = world.getExistingChunk(initPos.getX(), initPos.getZ());
            BlockPos spawnPos = initPos.add(0, i, 0);
            BlockState spawnState = world.getBlockState(spawnPos);
            BlockPos rootPos = spawnPos.add(0, -1, 0);
            BlockState groundState = world.getBlockState(rootPos);
            if (spawnState.getCollisionShape(view, spawnPos).isEmpty() &&
                !groundState.getCollisionShape(view, rootPos).isEmpty()) {
                return new Pair<>(groundState.getBlock(), spawnPos);
            }
        }
        return null;
    }
}
