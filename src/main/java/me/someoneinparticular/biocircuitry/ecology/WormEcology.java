/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.ecology;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.api.ecology.IEcology;
import me.someoneinparticular.biocircuitry.ecology.sequence_generators.AbstractSequenceGenerator;
import me.someoneinparticular.biocircuitry.genomics.WormGenome;
import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;
import me.someoneinparticular.biocircuitry.genomics.components.WormChromosome;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WormEcology implements IEcology<WormGenome> {

    // The sequence generators for each chromosome
    protected Collection<AbstractSequenceGenerator<NucleicBase>> chromGens;
    // The number of chromosome replicates that should be present in each genome
    // Diploid (2) is most common in nature, with tetraploid (4)
    // occasionally appearing in plants
    protected int ploidy;
    // The rate of random mutation induced in the sequences
    protected float mutationRate = 0.01f;

    public WormEcology(Collection<AbstractSequenceGenerator<NucleicBase>> chromGens, int ploidy) {
        this.chromGens = chromGens;
        this.ploidy = ploidy;
    }

    public WormEcology(Collection<AbstractSequenceGenerator<NucleicBase>> chromGens, int ploidy, float mutationRate) {
        this.chromGens = chromGens;
        this.ploidy = ploidy;
        this.mutationRate = mutationRate;
    }

    @Override
    public WormGenome generateGenome() {
        ArrayList<WormChromosome> chroms = new ArrayList<>(chromGens.size()*2);
        for (AbstractSequenceGenerator<NucleicBase> generator : chromGens) {
            // Generate each chromosome twice, as worms are presumed diploid
            for (int i = 0; i < ploidy; i++) {
                List<NucleicBase> newSeq = generator.generateSequence();
                newSeq = NucleicBase.mutateSequence(newSeq, mutationRate, BioCircuitry.RANDOM);
                chroms.add(new WormChromosome(newSeq));
            }

        }
        return new WormGenome(chroms);
    }
}
