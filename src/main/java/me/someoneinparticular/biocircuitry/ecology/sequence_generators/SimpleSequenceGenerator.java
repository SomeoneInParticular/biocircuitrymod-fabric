/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.ecology.sequence_generators;

import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;

import java.util.List;

/**
 * A simple, generic sequence generator which just returns a stored sequence
 */
public class SimpleSequenceGenerator extends AbstractSequenceGenerator<NucleicBase> {

    protected List<NucleicBase> sequence;

    public SimpleSequenceGenerator(List<NucleicBase> sequence) {
        this.sequence = sequence;
    }

    @Override
    public List<NucleicBase> generateSequence() {
        return sequence;
    }
}
