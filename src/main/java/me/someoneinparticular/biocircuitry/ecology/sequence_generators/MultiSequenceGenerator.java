/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.ecology.sequence_generators;

import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;

import java.util.ArrayList;
import java.util.List;

/**
 * A sequence generator which chains other sequence generators together in
 * a set order. Contents can be be of any other sequence generator type.
 */
public class MultiSequenceGenerator extends AbstractSequenceGenerator<NucleicBase> {

    protected List<AbstractSequenceGenerator<NucleicBase>> generators;

    public MultiSequenceGenerator(List<AbstractSequenceGenerator<NucleicBase>> generators) {
        this.generators = generators;
    }

    @Override
    public List<NucleicBase> generateSequence() {
        ArrayList<NucleicBase> sequence = new ArrayList<>();
        for (AbstractSequenceGenerator<NucleicBase> generator : generators) {
            sequence.addAll(generator.generateSequence());
        }
        return sequence;
    }
}
