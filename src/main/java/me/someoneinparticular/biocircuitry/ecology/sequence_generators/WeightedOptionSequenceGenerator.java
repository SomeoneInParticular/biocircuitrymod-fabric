/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.ecology.sequence_generators;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;

import java.util.List;

/**
 * A sequence generator which randomly selects from a set of weighted sequence
 * generators. Assumes the list of weights provided is the same length as the
 * list of sequence generators; will throw an error otherwise!
 */
public class WeightedOptionSequenceGenerator extends MultiSequenceGenerator {

    protected List<Integer> weights;
    protected int bound = 0;

    public WeightedOptionSequenceGenerator(List<AbstractSequenceGenerator<NucleicBase>> sequences, List<Integer> weights) {
        super(sequences);
        if (weights.size() != sequences.size()) {
            throw new IllegalArgumentException(String.format(
                    "Length of weights (%d) did not match length of generators (%d).", weights.size(), sequences.size()
            ));
        }
        this.weights = weights;
        weights.forEach((i) -> bound += i);
    }

    @Override
    public List<NucleicBase> generateSequence() {
        int target = BioCircuitry.RANDOM.nextInt(bound);
        int accum = 0;
        for (int i = 0; i < weights.size(); i++) {
            accum += weights.get(i);
            if (target <= accum) {
                return generators.get(i).generateSequence();
            }
        }
        // Should never reach this, but better safe than sorry
        throw new IndexOutOfBoundsException("Ran out of valid entries in " + this.toString());
    }
}
