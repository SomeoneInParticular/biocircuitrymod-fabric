/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.ecology.sequence_generators;

import me.someoneinparticular.biocircuitry.data_handlers.WormProteinHandler;
import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;
import me.someoneinparticular.biocircuitry.proteomics.components.AminoAcid;
import me.someoneinparticular.biocircuitry.proteomics.components.WormProtein;
import net.minecraft.util.Identifier;

import java.util.ArrayList;

/**
 * A sequence generator which reverse translates a protein sequence to generate
 * the result. These reverse translated sequences are random, containing any
 * genetic sequence which will translate into the targeted protein.
 */
public class ProteinBasedSequenceGenerator extends AbstractSequenceGenerator<NucleicBase> {

    protected Identifier id;

    public ProteinBasedSequenceGenerator(Identifier id) {
        this.id = id;
    }

    @Override
    public ArrayList<NucleicBase> generateSequence() {
        WormProtein prot = WormProteinHandler.get(id);
        if (prot == null) return new ArrayList<>();
        return AminoAcid.reverseTranslate(prot.getSequence());
    }
}
