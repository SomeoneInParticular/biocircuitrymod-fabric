/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.ecology.sequence_generators;

import java.util.List;

/**
 * Abstract class type for the generation of dynamic sequences
 * @param <S> The type of elements contained in the sequence
 */
public abstract class AbstractSequenceGenerator<S extends Comparable<S>> {
    public abstract List<S> generateSequence();
}
