/*
 * Copyright (c) 2019-2021 SomeoneInParticular.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v3
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Various Contributors including, but not limited to:
 * SomeoneInParticular (original work)
 */

package me.someoneinparticular.biocircuitry.ecology.sequence_generators;

import me.someoneinparticular.biocircuitry.BioCircuitry;
import me.someoneinparticular.biocircuitry.genomics.components.NucleicBase;

import java.util.List;

/**
 * A sequence generator which just randomly modifies the products of another
 * sequence generator. Useful for simulating completely random mutation.
 */
public class RandomVariantGenerator extends AbstractSequenceGenerator<NucleicBase> {

    protected AbstractSequenceGenerator<NucleicBase> generator;
    protected float rate;

    public RandomVariantGenerator(AbstractSequenceGenerator<NucleicBase> generator, float rate) {
        assert rate > 0; assert rate < 1;
        this.generator = generator;
        this.rate = rate;
    }

    @Override
    public List<NucleicBase> generateSequence() {
        return NucleicBase.mutateSequence(generator.generateSequence(), rate, BioCircuitry.RANDOM);
    }
}
